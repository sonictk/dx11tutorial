/**
 * @file   dx11_tutorial_test_data.h
 * @brief  Test data for demonstrating D3D API usage.
 */
#ifndef DX11_TUTORIAL_TEST_DATA_H
#define DX11_TUTORIAL_TEST_DATA_H


#include <Windows.h>
#include <DirectXMath.h>


static const char globalCheckerTextureFilename[] = "d_checker.png";
static const char globalCursorImageFilename[] = "cursor.bmp";
static const char globalTestBGMusicWAVFilename[] = "piano_jingle.wav";
static const char globalTestSFXFilename[] = "drip.wav";


/// Per-instance attributes that will be passed to the vertex shader.
__declspec(align(16)) // NOTE: (sonictk) Must be 16 byte aligned because XMMATRIX type must be 16-byte aligned. See https://docs.microsoft.com/en-us/windows/desktop/dxmath/pg-xnamath-getting-started#type_usage_guidelines_
struct Plane3DInstanceData
{
	DirectX::XMMATRIX worldMatrix;
	DirectX::XMMATRIX inverseTransposeWorldMatrix;
};


/// Holds the data for a per-frame constant buffer defined in the vertex shader.
struct PerFrameConstantBufferData
{
	DirectX::XMMATRIX viewProjectionMatrix;
};


/// Holds the data for a per-object constant buffer defined in the vertex shader.
struct PerObjectConstantBufferData
{
	DirectX::XMMATRIX worldMatrix;
	DirectX::XMMATRIX inverseTransposeWorldMatrix;
	DirectX::XMMATRIX worldViewProjectionMatrix;
};


#endif /* DX11_TUTORIAL_TEST_DATA_H */
