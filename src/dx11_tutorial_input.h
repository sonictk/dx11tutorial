/**
 * @file   dx11_tutorial_input.h
 * @brief  Constants and funcions for HID-related usage.
 */
#ifndef DX11_TUTORIAL_INPUT_H
#define DX11_TUTORIAL_INPUT_H

#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif

#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

#ifndef HID_USAGE_GENERIC_JOYSTICK
#define HID_USAGE_GENERIC_JOYSTICK        ((USHORT) 0x04)
#endif

#ifndef HID_USAGE_GENERIC_KEYBOARD
#define HID_USAGE_GENERIC_KEYBOARD     ((USHORT) 0x06)
#endif

#include <stdint.h>


/// Bitmasks for use in determining what keys are being held.
/// Memory layout is as such:
/// 0000 0000 ZCEQ DASW
globalVar uint16_t globalKeysDown = 0;
globalVar const uint16_t globalKeyMask_Fw = 0x1;
globalVar const uint16_t globalKeyMask_Bk = 0x2;
globalVar const uint16_t globalKeyMask_Lf = 0x4;
globalVar const uint16_t globalKeyMask_Rt = 0x8;
globalVar const uint16_t globalKeyMask_Up = 0x10;
globalVar const uint16_t globalKeyMask_Dn = 0x20;
globalVar const uint16_t globalKeyMask_RollLf = 0x40;
globalVar const uint16_t globalKeyMask_RollRt = 0x80;
globalVar const uint16_t globalKeyMask_UIModifier = 0x100;

globalVar uint8_t globalModifiersDown = 0;
globalVar uint8_t globalModifiersMask_Shift = 0x1;
globalVar uint8_t globalModifiersMask_Control = 0x2;


BOOL RegisterRawInputForMouse(HWND hWnd);


#endif /* DX11_TUTORIAL_INPUT_H */
