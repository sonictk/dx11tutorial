/**
 * @file   dx11_tutorial_lights.h
 * @brief  Functions and data structures related to lighting.
 */
#ifndef DX11_TUTORIAL_LIGHTS_H
#define DX11_TUTORIAL_LIGHTS_H

#include <stdint.h>
#include <DirectXMath.h>

/// Maximum number of lights in the application.
#define MAX_LIGHTS 4

/// Various enums for types of lights.
#define DIRECTIONAL_LIGHT 0
#define POINT_LIGHT 1
#define SPOT_LIGHT 2


/// This defines the properties for a given light. It matches the memory layout as
/// defined in the HLSL shader.
struct Light
{
	DirectX::XMFLOAT4 position;
	DirectX::XMFLOAT4 direction;
	DirectX::XMFLOAT4 color;

	float spotAngle;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;

	int lightType; // NOTE: (sonictk) This is an int in the HLSL shader.

	int isEnabled;

	double _padding;  // NOTE: (sonictk) This is to satisfy 16-byte alignment boundaries.
};


/// Matches the layout in the pixel shader.
struct LightProperties
{
	DirectX::XMFLOAT4 eyePosition;
	DirectX::XMFLOAT4 globalAmbient;

	Light sceneLights[MAX_LIGHTS];
};


/// Matches the layout in the pixel shader.
struct LightingResult
{
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT4 specular;
};


void CreateTestLights();


void UpdateLights(const float timeElapsed);


#endif /* DX11_TUTORIAL_LIGHTS_H */
