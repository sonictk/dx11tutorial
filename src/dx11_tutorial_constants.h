/**
 * @file   dx11_tutorial_constants.h
 * @brief  Contains variables that should not change throughout the lifetime of
 *         the entire application.
 */
#ifndef DX11_TUTORIAL_CONSTANTS_H
#define DX11_TUTORIAL_CONSTANTS_H

#define D3D11_MAX_MATERIAL_PROPERTIES_COUNT 4

globalVar const char D3D11_TUTORIAL_WINDOW_CLASS_NAME[] = "D3D11WndClass";
globalVar const char D3D11_TUTORIAL_WINDOW_TITLE[] = "Direct3D 11 Tutorial Window";
globalVar const char APPLICATION_TEXTURES_DIRECTORY_NAME[] = "textures";
globalVar const char APPLICATION_IMAGES_DIRECTORY_NAME[] = "images";
globalVar const char APPLICATION_AUDIO_DIRECTORY_NAME[] = "audio";


globalVar const int globalMinimumWindowWidth = 640;
globalVar const int globalMinimumWindowHeight = 480;

globalVar const int globalNumOfPlanes = 6;


enum D3DConstantBuffer
{
	D3DConstantBuffer_PerApp,
	D3DConstantBuffer_PerFrame,
	D3DConstantBuffer_PerObject,

	D3DConstantBufferCount
};

globalVar const int globalMouseDeltaThreshold = 1;

globalVar const int globalDefaultWindowWidth = 1280;
globalVar const int globalDefaultWindowHeight = 800;

globalVar const float globalDefaultTargetFPS = 60.0f;

globalVar const float globalDefaultNearClip = 0.1f;
globalVar const float globalDefaultFarClip = 100.0f;

globalVar const float globalDefaultCameraSpeed = 0.1f;
globalVar const float globalDefaultMouseSensitivity = 0.5f;
globalVar const float globalDefaultCameraZoomSpeed = 2.0f;
globalVar const float globalDefaultFieldOfViewAngle = 45.0f;


#endif /* DX11_TUTORIAL_CONSTANTS_H */
