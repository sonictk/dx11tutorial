/**
 * @file   dx11_tutorial_config.h
 * @brief  Variables that are intended to act as user-configurable options should
 * 		be defined here.
 */
#ifndef DX11_TUTORIAL_CONFIG_H
#define DX11_TUTORIAL_CONFIG_H

#include "dx11_tutorial_constants.h"

// NOTE: (sonictk) Default width/height.
globalVar int globalWindowWidth = 1280;
globalVar int globalWindowHeight = 800;

globalVar float globalTargetFPS = 60.0f;
globalVar float globalCurrentApplicationFPS = 0.0f;

globalVar BOOL globalVSyncEnabled = TRUE;
globalVar UINT globalD3DMSAASampleCount = 4; // NOTE: (sonictk) Set to 1 if you want to disable MSAA
globalVar UINT globalD3DMSAASampleQuality = 0; // NOTE: (sonictk) Used to store D3D11 supported quality levels.

globalVar float globalNearClip = 0.1f;
globalVar float globalFarClip = 100.0f;

globalVar float globalCameraSpeed = globalDefaultCameraSpeed;
globalVar float globalCameraRollSpeed = 0.02f;
globalVar float globalMouseSensitivity = 0.005f;
globalVar float globalCameraZoomSpeed = 2.0f;

globalVar float globalFieldOfViewAngle = 45.0f;

globalVar float globalMasterVolume = 1.0f;

#endif /* DX11_TUTORIAL_CONFIG_H */
