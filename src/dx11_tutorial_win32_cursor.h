#ifndef DX11_TUTORIAL_WIN32_CURSOR_H
#define DX11_TUTORIAL_WIN32_CURSOR_H


/**
 * Creates a Windows cursor from the given image data.
 *
 * @param hwnd		The handle to the window that will have the new cursor resource.
 * @param width	The stride of the image data.
 * @param height	The pitch of the image data.
 * @param agbr		The image data in AGBR format, 32bpp.
 * @param xHotspot	The x-coordinate hotspot of the cursor.
 * @param yHotspot	The y-coordinate hotspot of the cursor.
 *
 * @return			A handle to the newly-created cursor. On error, returns ``NULL``.
 */
HCURSOR CreateCursorFromImage(HWND hwnd,
							  int width,
							  int height,
							  uint32_t *rgba,
							  int xHotspot,
							  int yHotspot);


#endif /* DX11_TUTORIAL_WIN32_CURSOR_H */
