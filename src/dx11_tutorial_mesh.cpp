#include <math.h>
#include <stdlib.h>
#include "dx11_tutorial_mesh.h"
#include "dx11_tutorial_math.h"

#include <Windows.h>
#include <DirectXMath.h> // NOTE: (sonictk) Must be included after windows header according to MSDN


namespace dx = DirectX;
using namespace DirectX; // NOTE: (sonictk) Need to do this for operator overloads to work.


VertexTexture midPoint(const VertexTexture &v1, const VertexTexture &v2)
{
	// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 code sample
	dx::XMVECTOR pt1 = dx::XMLoadFloat3(&v1.position);
	dx::XMVECTOR pt2 = dx::XMLoadFloat3(&v2.position);

	dx::XMVECTOR normal1 = dx::XMLoadFloat3(&v1.normal);
	dx::XMVECTOR normal2 = dx::XMLoadFloat3(&v2.normal);

	dx::XMVECTOR texCoord1 = dx::XMLoadFloat2(&v1.texCoord0);
	dx::XMVECTOR texCoord2 = dx::XMLoadFloat2(&v2.texCoord0);

	// Compute the midpoints of all the attributes. Normals need to be re-normalized
	// since lerp can make them non-normalized.
	VertexTexture result;

	dx::XMVECTOR midPt = 0.5f * (pt1 + pt2);
	dx::XMVECTOR midNormal = dx::XMVector3Normalize(0.5f * (normal1 + normal2));
	dx::XMVECTOR midTexCoords = 0.5f * (texCoord1 + texCoord2);

	dx::XMStoreFloat3(&result.position, midPt);
	dx::XMStoreFloat3(&result.normal, midNormal);
	dx::XMStoreFloat2(&result.texCoord0, midTexCoords);

	return result;
}


// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 code sample
void subdivideMesh(VertexTexture *vertices,
				   const int numVertices,
				   VertexTexture *outVertices,
				   const int numOfSubdivs,
				   int *outVerticesCount,
				   short *outIndices,
				   int *outIndicesCount)
{
	if (numOfSubdivs <= 0) {
		return;
	}

	// NOTE: (sonictk) We should only subdivide manifold meshes.
	// (i.e. no stray points, fully formed triangles.)
	assert(numVertices % 3 == 0);

	const int numTris = numVertices / 3;

	if (outVerticesCount != NULL) {
		*outVerticesCount = numTris * 6 * numOfSubdivs;
	}
	if (outIndicesCount != NULL) {
		*outIndicesCount = numTris * 12 * numOfSubdivs;
	}

	// NOTE: (sonictk) If the user hasn't passed in a buffer for vertices at this
	// point, there's nothing left to do.
	if (outVertices == NULL) {
		return;
	}

	int vtxCounter = 0;
	int idxCounter = 0;
	for (int s=0; s < numOfSubdivs; ++s) {
		if (outVertices != NULL) {
			for (int i=0; i < numTris; ++i) {
				VertexTexture v0 = vertices[(i * 3)];
				VertexTexture v1 = vertices[(i * 3) + 1];
				VertexTexture v2 = vertices[(i * 3) + 2];

				// Generate the midpoints.
				VertexTexture midPt0 = midPoint(v0, v1);
				VertexTexture midPt1 = midPoint(v1, v2);
				VertexTexture midPt2 = midPoint(v0, v2);

				// Add new geometry.
				outVertices[vtxCounter] = v0;
				outVertices[vtxCounter + 1] = v1;
				outVertices[vtxCounter + 2] = v2;
				outVertices[vtxCounter + 3] = midPt0;
				outVertices[vtxCounter + 4] = midPt1;
				outVertices[vtxCounter + 5] = midPt2;

				vtxCounter += 6;
			}
		}

		if (outIndices != NULL) {
			for (short i=0; i < numTris; ++i) {
				outIndices[(short)idxCounter] = (i * 6) + 0;
				outIndices[(short)idxCounter + 1] = (i * 6) + 3;
				outIndices[(short)idxCounter + 2] = (i * 6) + 5;

				outIndices[(short)idxCounter + 3] = (i * 6) + 3;
				outIndices[(short)idxCounter + 4] = (i * 6) + 4;
				outIndices[(short)idxCounter + 5] = (i * 6) + 5;

				outIndices[(short)idxCounter + 6] = (i * 6) + 5;
				outIndices[(short)idxCounter + 7] = (i * 6) + 4;
				outIndices[(short)idxCounter + 8] = (i * 6) + 2;

				outIndices[(short)idxCounter + 9] = (i * 6) + 3;
				outIndices[(short)idxCounter + 10] = (i * 6) + 1;
				outIndices[(short)idxCounter + 11] = (i * 6) + 4;

				idxCounter += 12;
			}
		}
	}

	return;
}


void createQuad(const float width,
				const float height,
				const float x,
				const float y,
				const float z,
				VertexTexture *vertices,
				short *indices)
{
	// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 code sample
	// Position coordinates defined in NDC space.
	VertexTexture v0;
	v0.position = dx::XMFLOAT3(x, y - height, z);
	v0.normal = dx::XMFLOAT3(0.0f, 0.0f, -1.0f);
	v0.texCoord0 = dx::XMFLOAT2(0.0f, 1.0f);
	vertices[0] = v0;

	VertexTexture v1;
	v1.position = dx::XMFLOAT3(x, y, z);
	v1.normal = dx::XMFLOAT3(0.0f, 0.0f, -1.0f);
	v1.texCoord0 = dx::XMFLOAT2(0.0f, 0.0f);
	vertices[1] = v1;

	VertexTexture v2;
	v2.position = dx::XMFLOAT3(x + width, y, z);
	v2.normal = dx::XMFLOAT3(0.0f, 0.0f, -1.0f);
	v2.texCoord0 = dx::XMFLOAT2(1.0f, 0.0f);
	vertices[2] = v2;

	VertexTexture v3;
	v3.position = dx::XMFLOAT3(x + width, y - height, z);
	v3.normal = dx::XMFLOAT3(0.0f, 0.0f, -1.0f);
	v3.texCoord0 = dx::XMFLOAT2(1.0f, 1.0f);
	vertices[3] = v3;

	if (indices != NULL) {
		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;

		indices[3] = 0;
		indices[4] = 2;
		indices[5] = 3;
	}

	return;
}


void createGridMesh(const float width,
					const float depth,
					const int rows,
					const int columns,
					VertexTexture *vertices,
					short *indices)
{
	// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 (6.11.1)
	// Formula for the coordinates of the i x j-th grid vertex in the XZ-plane can be expressed as:
	// (where w is width and d is depth)
	// pos = [-0.5w + j . dx, 0.0, 0.5d - i . dz]
	const int numOfVertices = rows * columns;
	const int numOfFaces = (rows - 1) * (columns - 1) * 2;

	const float halfWidth = 0.5f * width;
	const float halfDepth = 0.5f * depth;

	const float dx = width / (rows - 1);
	const float dz = width / (columns - 1);

	const float du = 1.0f / (rows - 1);
	const float dv = 1.0f / (columns - 1);

	for (int r = 0; r < rows; ++r) {

		float z = halfDepth - (r * dz);

		for (int c = 0; c < columns; ++c) {
			float x = -halfWidth + (c * dx);

			int curVtxIdx = (r * columns) + c;
			// NOTE: (sonictk) Assuming plane mesh is created in the XZ plane.
			vertices[curVtxIdx].position = dx::XMFLOAT3(x, 0.0f, z);
			vertices[curVtxIdx].normal = dx::XMFLOAT3(0.0f, 1.0f, 0.0f);

			// Stretch texture over the new grid mesh.
			vertices[curVtxIdx].texCoord0.x = c * du;
			vertices[curVtxIdx].texCoord0.y = r * dv;
		}
	}

	if (indices != NULL) {
		// Generate indices per-quad.
		int indicesCounter = 0;
		for (short r = 0; r < rows - 1; ++r) {
			for (short c = 0; c < columns - 1; ++c) {
				indices[(short)indicesCounter] = (r * (short)columns) + c;
				indices[(short)indicesCounter + 1] = (r * (short)columns) + c + 1;
				indices[(short)indicesCounter + 2] = ((r + 1) * (short)columns) + c;

				indices[(short)indicesCounter + 3] = ((r + 1) * (short)columns) + c;
				indices[(short)indicesCounter + 4] = (r * (short)columns) + c + 1;
				indices[(short)indicesCounter + 5] = ((r + 1) * (short)columns) + c + 1;

				indicesCounter += 6;
			}
		}
	}

	return;
}


void createCylinderMesh(const float radiusBottom,
						const float radiusTop,
						const float height,
						const int numSlices,
						const int numStacks,
						VertexTexture *vertices)
{
	// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 (6.11.1)
	float stackHeight = height / (float)numStacks;

	// NOTE: (sonictk) Amount to increment radius as we move up each stack level from btm to top.
	float radiusStep = (radiusTop - radiusBottom) / (float)numStacks;

	int numRings = numStacks + 1;

	// NOTE: (sonictk) Compute vertices for each stack ring starting from btm to top.
	// radius of ring is ``h(i) = -h/2 + stackHeight`` for the ith ring
	float dTheta = 2.0f * PI / numSlices;
	for (int i=0; i < numRings; ++i) {
		float y = (-0.5f * height) + (i * stackHeight);
		float r = radiusBottom + (i * radiusStep);

		for (int j=0; j <= numSlices; ++j) {
			VertexTexture vtx;

			float c = cosf(j * dTheta);
			float s = sinf(j * dTheta);

			vtx.position.x = r * c;
			vtx.position.y = y;
			vtx.position.z = r * s;

			vtx.texCoord0.x = (float)j / (float)numSlices;
			vtx.texCoord0.y = 1.0f - ((float)i / (float)numStacks);

			// Tangent U as unit length.
			dx::XMFLOAT3 tangentU = dx::XMFLOAT3(-s, 0.0f, c);
			float dr = radiusBottom - radiusTop;
			dx::XMFLOAT3 biTangent(dr * c, -height, dr * s);
			dx::XMVECTOR t = dx::XMLoadFloat3(&tangentU);
			dx::XMVECTOR b = dx::XMLoadFloat3(&biTangent);
			dx::XMVECTOR n = dx::XMVector3Normalize(dx::XMVector3Cross(t, b));
			dx::XMStoreFloat3(&vtx.normal, n);

			vertices[(i * numSlices) + j] = vtx;
		}
	}

	// TODO: (sonictk) Build the cylinder cap on top/bottom and their indices.
	// int baseIndex = numRings * numSlices;
	// float y = 0.5f * height;

	// // Duplicate cap ring vertices because the texture coordinates and normals differ.
	// for (int i=0; i < numSlices; ++i) {
	// 	float x = topRadius * cosf(i * dTheta);
	// 	float z = topRadius * sinf(i * dTheta);

	// 	// Scale down by the height to try and make top cap texture coordinates area
	// 	// proportional to the base.
	// 	float u = x / height + 0.5f;
	// 	float v = z / height + 0.5f;

	// 	VertexTexture vtx;

	// }

	return;
}


// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 (6.11.1)
void generateIndicesForCylinder(const int numSlices, const int numStacks, short *indices)
{
	// NOTE: (sonictk) Add one because we duplicate the first and last vertices per ring
	// since the tex coords are different.
	int ringVertexCount = numSlices + 1;

	// Compute indices for each stack. Formula is based on the one described in the book.
	for (short i=0; i < (short)numStacks; ++i) {
		for (short j=0; j < (short)numSlices; ++j) {
			short k = (i * (short)numSlices) + j;

			// Indices for first triangle of the quad of the current slice.
			indices[k] = (i * (short)ringVertexCount) + j;
			indices[k + 1] = ((i + 1) * (short)ringVertexCount) + j;
			indices[k + 2] = ((i + 1) * (short)ringVertexCount) + j + 1;

			// Second triangle.
			indices[k + 3] = (i * (short)ringVertexCount) + j;
			indices[k + 4] = ((i + 1) * (short)ringVertexCount) + j + 1;
			indices[k + 5] = (i * (short)ringVertexCount) + j + 1;
		}
	}
}


// NOTE: (sonictk) Implementation adapted from Frank Luna's Introduction to 3D Game Programming With DirectX11 (6.11.1)
// TL;DR Start with a icosahedron, subdivide the tris, and then project the new
// vertices onto the sphere with the given radius. Repeat as necessary.
void createGeosphere(const float radius,
					 const int numSubdivs,
					 VertexTexture *vertices,
					 int *outVerticesCount,
					 short *indices,
					 int *outIndicesCount)
{
	// Approximate a sphere by tessellating a icosahedron.
	const float x = 0.525731f;
	const float z = 0.850651f;

	dx::XMFLOAT3 defaultVertexPositions[12] = {
		dx::XMFLOAT3(-x, 0.0f, z),    dx::XMFLOAT3(x, 0.0f, z),
		dx::XMFLOAT3(-x, 0.0f, -z),   dx::XMFLOAT3(x, 0.0f, -z),
		dx::XMFLOAT3(0.0f, z, x),     dx::XMFLOAT3(0.0f, z, -x),
		dx::XMFLOAT3(0.0f, -z, x),    dx::XMFLOAT3(0.0f, -z, -x),
		dx::XMFLOAT3(z, x, 0.0f),     dx::XMFLOAT3(-z, x, 0.0f),
		dx::XMFLOAT3(z, -x, 0.0f),    dx::XMFLOAT3(-z, -x, 0.0f)
	};

	int defaultVertexIndices[60] = {
		1,4,0,   4,9,0,   4,5,9,   8,5,4,   1,8,4,
		1,10,8,  10,3,8,  8,3,5,   3,2,5,   3,7,2,
		3,10,7,  10,6,7,  6,11,7,  6,0,11,  6,1,0,
		10,1,6,  11,0,9,  2,11,9,  5,2,9,   11,2,7
	};

	VertexTexture defaultVertices[12];
	for (int i=0; i < 12; ++i) {
		defaultVertices[i].position = defaultVertexPositions[i];
	}

	if (vertices == NULL && indices == NULL) {
		subdivideMesh(defaultVertices,
					  _countof(defaultVertexPositions),
					  NULL,
					  numSubdivs,
					  outVerticesCount,
					  NULL,
					  outIndicesCount);

		return;
	}

	for (int i=0; i < 12; ++i) {
		vertices[i].position = defaultVertexPositions[i];
	}

	if (indices != NULL) {
		for (int i=0; i < 60; ++i) {
			indices[i] = (short)defaultVertexIndices[i];
		}
	}

	subdivideMesh(defaultVertices,
				  _countof(defaultVertexPositions),
				  vertices,
				  numSubdivs,
				  outVerticesCount,
				  indices,
				  outIndicesCount);

	// TODO: (sonictk) Finish implementation

}
