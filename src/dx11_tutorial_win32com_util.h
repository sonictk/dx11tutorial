#ifndef DX11_TUTORIAL_WIN32COM_UTIL_H
#define DX11_TUTORIAL_WIN32COM_UTIL_H

/// Convenience macro for releasing COM objects.
#define ReleaseCOM(x) { if(x) { x->Release(); x = NULL; } }

#endif /* DX11_TUTORIAL_WIN32COM_UTIL_H */
