#include "dx11_tutorial_win32_audio.h"


XAudio2Status InitializeXAudio2(IXAudio2 **engine, IXAudio2MasteringVoice **masterVoice)
{
	// NOTE: (sonictk) This is according to MSDN's own example, even though the
	// documentation says the second parameter should be ``0``!
	IXAudio2 *engineResult;
	HRESULT hr = XAudio2Create(&engineResult, 0, XAUDIO2_DEFAULT_PROCESSOR);
	if (FAILED(hr)) {
		return XAudio2Status_InterfaceCreationFailure;
	}

	*engine = engineResult;

	assert(engine != NULL);

	IXAudio2MasteringVoice *masterVoiceResult;
	hr = engineResult->CreateMasteringVoice(&masterVoiceResult);
	switch (hr) {
	case ERROR_NOT_FOUND:
		return XAudio2Status_NoDefaultAudioDevice;
	case S_OK:
		break;
	default:
		return XAudio2Status_MasteringVoiceCreationFailure;
	}

	*masterVoice = masterVoiceResult;

	return XAudio2Status_Success;
}


// NOTE: (sonictk) Implementation from: https://docs.microsoft.com/en-us/windows/desktop/xaudio2/how-to--load-audio-data-files-in-xaudio2
XAudio2Status FindChunk(HANDLE hFile, DWORD fourcc, DWORD &chunkSize, DWORD &chunkDataPosition)
{
	DWORD filePtr = SetFilePointer(hFile, 0, NULL, FILE_BEGIN);
	if (filePtr == INVALID_SET_FILE_POINTER) {

		return XAudio2Status_SetFilePointerFailed;
	}

	DWORD chunkType;
	DWORD chunkDataSize;
	DWORD RIFFDataSize = 0;
	DWORD fileType;
	DWORD totalBytesRead = 0;
	DWORD offset = 0;

	// NOTE: (sonictk) Keep reading the file in chunks, identify the chunk data type
	// by its FOURCC identifier and return it if it matches the input code.
	//                                   4 bytes    FOURCC   chunk(s)
	// RIFF chunk structure is: "RIFF", fileSize, fileType, data
	HRESULT hr = S_OK;
	while (hr == S_OK) {
		DWORD bytesRead;
		BOOL status = ReadFile(hFile, &chunkType, sizeof(DWORD), &bytesRead, NULL);
		if (status == FALSE) {
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		status = ReadFile(hFile, &chunkDataSize, sizeof(DWORD), &bytesRead, NULL);
		if (status == FALSE) {
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch (chunkType) {
		case fourccRIFF:
			RIFFDataSize = chunkDataSize;
			chunkDataSize = 4;

			status = ReadFile(hFile, &fileType, sizeof(DWORD), &bytesRead, NULL);
			if (status == FALSE) {
				hr = HRESULT_FROM_WIN32(GetLastError());
			}

			break;

		default:
			DWORD advanceFilePtr = SetFilePointer(hFile, chunkDataSize, NULL, FILE_CURRENT);
			if (advanceFilePtr == INVALID_SET_FILE_POINTER) {
				return XAudio2Status_SetFilePointerFailed;
			}
			break;
		}

		offset += sizeof(DWORD) * 2;

		if (chunkType == fourcc) {
			chunkSize = chunkDataSize;
			chunkDataPosition = offset;

			return XAudio2Status_Success;
		}

		offset += chunkDataSize;

		if (totalBytesRead >= RIFFDataSize) {
			return XAudio2Status_ChunkNotFound;
		}
	}

	return XAudio2Status_Success;
}


XAudio2Status ReadChunkData(HANDLE hFile, void *buffer, DWORD bufferSize, DWORD bufferOffset)
{
	DWORD filePtr = SetFilePointer(hFile, bufferOffset, NULL, FILE_BEGIN);
	if (filePtr == INVALID_SET_FILE_POINTER) {
		return XAudio2Status_SetFilePointerFailed;
	}

	DWORD bytesRead;
	BOOL status = ReadFile(hFile, buffer, bufferSize, &bytesRead, NULL);
	if (status = FALSE) {
		return XAudio2Status_FileReadFailed;
	}

	return XAudio2Status_Success;
}


XAudio2Status LoadWAVAudio(HANDLE hFile,
						   WAVEFORMATEXTENSIBLE &waveData,
						   XAUDIO2_BUFFER &buffer,
						   BYTE *data,
						   DWORD &size)
{

	// TODO: (sonictk) Should reset the file since it could have been seeked ahead earlier?
	// DWORD filePtr = SetFilePointer(hFile, 0, NULL, FILE_BEGIN);

	DWORD chunkSize;
	DWORD chunkPosition;
	XAudio2Status status = FindChunk(hFile, fourccRIFF, chunkSize, chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_InvalidRIFFFileFormat;
	}

	DWORD fileType;
	status = ReadChunkData(hFile, &fileType, sizeof(DWORD), chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_FileReadFailed;
	} else if (fileType != fourccWAVE) {
		return XAudio2Status_InvalidRIFFFileFormat;
	}

	// NOTE: (sonictk) Order of data stored in RIFF form should be:
	// "RIFF", fileSize, fileType, data <--- we need to find data and read it in
	// See: https://docs.microsoft.com/en-us/windows/desktop/xaudio2/resource-interchange-file-format--riff-
	status = FindChunk(hFile, fourccFMT, chunkSize, chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_ChunkNotFound;
	}

	// NOTE: (sonictk) According to MSDN the WAVEFORMATEXTENSIBLE data structure
	// should match this chunk 1:1.
	status = ReadChunkData(hFile, &waveData, chunkSize, chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_FileReadFailed;
	}

	// TODO: (sonictk) Move this first?
	status = FindChunk(hFile, fourccDATA, chunkSize, chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_ChunkNotFound;
	}

	if (size == 0) {
		size = chunkSize;

		return XAudio2Status_Success;
	}

	status = ReadChunkData(hFile, data, chunkSize, chunkPosition);
	if (status != XAudio2Status_Success) {
		return XAudio2Status_FileReadFailed;
	}

	buffer.Flags = XAUDIO2_END_OF_STREAM; // NOTE: (sonictk) According to MSDN, this tells the source voice not to expect any data after this buffer.
	buffer.AudioBytes = chunkSize;
	buffer.pAudioData = data;

	return XAudio2Status_Success;
}


void SetXAudio2FXDefaultReverbParams(XAUDIO2FX_REVERB_PARAMETERS &reverbParams)
{
	reverbParams.ReflectionsDelay = XAUDIO2FX_REVERB_DEFAULT_REFLECTIONS_DELAY;
	reverbParams.ReverbDelay = XAUDIO2FX_REVERB_DEFAULT_REVERB_DELAY;
	reverbParams.RearDelay = XAUDIO2FX_REVERB_DEFAULT_REAR_DELAY;
	reverbParams.PositionLeft = XAUDIO2FX_REVERB_DEFAULT_POSITION;
	reverbParams.PositionRight = XAUDIO2FX_REVERB_DEFAULT_POSITION;
	reverbParams.PositionMatrixLeft = XAUDIO2FX_REVERB_DEFAULT_POSITION_MATRIX;
	reverbParams.PositionMatrixRight = XAUDIO2FX_REVERB_DEFAULT_POSITION_MATRIX;
	reverbParams.EarlyDiffusion = XAUDIO2FX_REVERB_DEFAULT_EARLY_DIFFUSION;
	reverbParams.LateDiffusion = XAUDIO2FX_REVERB_DEFAULT_LATE_DIFFUSION;
	reverbParams.LowEQGain = XAUDIO2FX_REVERB_DEFAULT_LOW_EQ_GAIN;
	reverbParams.LowEQCutoff = XAUDIO2FX_REVERB_DEFAULT_LOW_EQ_CUTOFF;
	reverbParams.HighEQGain = XAUDIO2FX_REVERB_DEFAULT_HIGH_EQ_GAIN;
	reverbParams.HighEQCutoff = XAUDIO2FX_REVERB_DEFAULT_HIGH_EQ_CUTOFF;
	reverbParams.RoomFilterFreq = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_FREQ;
	reverbParams.RoomFilterMain = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_MAIN;
	reverbParams.RoomFilterHF = XAUDIO2FX_REVERB_DEFAULT_ROOM_FILTER_HF;
	reverbParams.ReflectionsGain = XAUDIO2FX_REVERB_DEFAULT_REFLECTIONS_GAIN;
	reverbParams.ReverbGain = XAUDIO2FX_REVERB_DEFAULT_REVERB_GAIN;
	reverbParams.DecayTime = XAUDIO2FX_REVERB_DEFAULT_DECAY_TIME;
	reverbParams.Density = XAUDIO2FX_REVERB_DEFAULT_DENSITY;
	reverbParams.RoomSize = XAUDIO2FX_REVERB_DEFAULT_ROOM_SIZE;
	reverbParams.WetDryMix = XAUDIO2FX_REVERB_DEFAULT_WET_DRY_MIX;

	return;
}


XAudio2Status CreateXAPOReverbForVoice(IXAudio2Voice *voice, bool initialState, XAUDIO2FX_REVERB_PARAMETERS reverbParams, IUnknown **XAPOReverb)
{
	// NOTE: (sonictk) Audio Processing Object has an APO interface for creating custom audio
	// processing effects. Reverb comes with XAudio2.
	HRESULT hr = XAudio2CreateReverb(XAPOReverb);
	if (FAILED(hr)) {
		return XAudio2Status_XAPOCreationFailed;
	}

	XAUDIO2_VOICE_DETAILS voiceDetails;
	voice->GetVoiceDetails(&voiceDetails);

	XAUDIO2_EFFECT_DESCRIPTOR XAPOReverbDesc;
	ZeroMemory(&XAPOReverbDesc, sizeof(XAUDIO2_EFFECT_DESCRIPTOR));
	XAPOReverbDesc.pEffect = *XAPOReverb;
	XAPOReverbDesc.InitialState = (BOOL)initialState;
	XAPOReverbDesc.OutputChannels = voiceDetails.InputChannels;

	XAUDIO2_EFFECT_CHAIN reverbFXChain;
	reverbFXChain.EffectCount = 1;
	reverbFXChain.pEffectDescriptors = &XAPOReverbDesc;

	hr = voice->SetEffectChain(&reverbFXChain);
	if (FAILED(hr)) {
		return XAudio2Status_ApplyXAPOFXChainFailed;
	}

	hr = voice->SetEffectParameters(0, &reverbParams, sizeof(XAUDIO2FX_REVERB_PARAMETERS));
	if (FAILED(hr)) {
		return XAudio2Status_SetFXParamsFailed;
	}

	return XAudio2Status_Success;
}


void InitializeX3DAudio(IXAudio2MasteringVoice *masterVoice, X3DAUDIO_HANDLE &X3DInstance)
{
	DWORD channelMask;
	masterVoice->GetChannelMask(&channelMask);

	X3DAudioInitialize(channelMask, X3DAUDIO_SPEED_OF_SOUND, X3DInstance);
}
