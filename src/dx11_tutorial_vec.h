/**
 * @file   dx11_tutorial_vec.h
 * @brief  Vector math data structures.
 */
#ifndef DX11_TUTORIAL_VEC_H
#define DX11_TUTORIAL_VEC_H

#include <DirectXMath.h>


struct Vertex
{
	DirectX::XMFLOAT3 position; // NOTE: (sonictk) 0 byte offset
	DirectX::XMFLOAT4 color; // NOTE: (sonictk) 12 byte offset
};


struct VertexTexture
{
	DirectX::XMFLOAT3 position; // NOTE: (sonictk) 0 byte offset
	DirectX::XMFLOAT3 normal; // NOTE: (sonictk) 12 byte offset
	DirectX::XMFLOAT2 texCoord0; // NOTE: (sonictk) 24 byte offset
};


#endif /* DX11_TUTORIAL_VEC_H */
