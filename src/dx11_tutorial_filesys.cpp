#include "dx11_tutorial_filesys.h"

#include <Windows.h>


int getAppPath(char *filename, const unsigned int len)
{
	DWORD pathLen = GetModuleFileName(NULL, (LPTSTR)filename, (DWORD)len);
	if (pathLen == 0) {
		perror("Failed to get executable path!\n");

		return -1;
	}

	return pathLen;
}
