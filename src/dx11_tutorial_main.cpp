/**
 * @file   dx11_tutorial_main.cpp
 * @brief  Main entry point for the Direct3D 11 test application.
 *
 * @todo   Mesh prims library.
 * @todo   positional sound cuts out after a while.
 * @todo   Modify test play audio to start audio with lower volume and control using the GUI.
 */
#define USE_PRECOMPILED_SHADERS // NOTE: (sonictk) Will use the precompiled .fxc shaders.
// #define USE_PRECOMPILED_SHADER_HEADERS	// NOTE: (sonictk) Will use precompiled shader headers instead.

#define globalVar static

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <Windows.h>
#include <Pathcch.h>
#include <Windowsx.h> // NOTE: (sonictk) For the GET_X_LPARAM macro
#include <d3d11.h>
#include <d3dcompiler.h>
#include <dxgi1_2.h>

#include <DirectXMath.h> // NOTE: (sonictk) This _must_ be included _after_ ``windows.h`` according to MSDN.
#include <DirectXColors.h>

#include <Xaudio2.h>
#include <xaudio2fx.h>

#include "WinuserEx.h"

#include "dx11_tutorial_constants.h"
#include "dx11_tutorial_config.h"

#include "dx11_tutorial_math.h"
#include "dx11_tutorial_statuses.h"
#include "dx11_tutorial_application_states.h"
#include "dx11_tutorial_input.cpp"
#include "dx11_tutorial_filesys.cpp"
#include "dx11_tutorial_vec.h"
#include "dx11_tutorial_colors.h"
#include "dx11_tutorial_test_data.cpp"
#include "dx11_tutorial_materials.cpp"
#include "dx11_tutorial_lights.cpp"
#include "dx11_tutorial_mesh.cpp"

#include "dx11_tutorial_win32com_util.h"
#include "dx11_tutorial_win32_cursor.cpp"
#include "dx11_tutorial_win32_audio.cpp"

// NOTE: (sonictk) We use STB libraries for loading image data.
#define STB_IMAGE_IMPLEMENTATION // NOTE: (sonictk) Required by stb_image.h.
#include <stb/stb_image.h>


// NOTE: (sonictk) We use ImGui for HUD.
#include <imgui.cpp> // NOTE: (sonictk) Must be included before ``imgui_internal.h``.
#include <imconfig.h>
#include <imgui_internal.h>
#include <imstb_rectpack.h>
#include <imstb_textedit.h>
#include <imstb_truetype.h>
#include <imgui_draw.cpp>
#include <imgui_widgets.cpp>

#include <imgui_impl_win32.cpp>
#include <imgui_impl_dx11.cpp>


// NOTE: (sonictk) We have to do this since the overloads are also in the DirectX namespace.
using namespace DirectX;
namespace dx = DirectX;

globalVar HANDLE globalProcessHeap;

/// Global handle to main window
globalVar HWND globalMainWindowHandle = NULL;

/// The frequency of the high-resolution performance counter of the system.
globalVar LARGE_INTEGER globalCPUTimerFreq = {0};

/// Global timer used for measuring short intervals with high-precision. This is
/// also the global timer for the application (i.e. it does not measure time when
/// the application is paused/suspended.)

globalVar LARGE_INTEGER globalHighResolutionTimer = {0};
globalVar double globalTimeElapsed = 0.0;

globalVar ID3D11Device *globalD3DDevice = NULL;
globalVar ID3D11DeviceContext *globalD3DDeviceContext = NULL;
globalVar IDXGISwapChain *globalD3DSwapChain = NULL;

globalVar IDXGIDevice *globalDXGIDevice = NULL;
globalVar IDXGIAdapter *globalDXGIAdapter = NULL;
globalVar IDXGIFactory *globalDXGIFactory = NULL;

/// Render target view for the back buffer of the swap chain.
globalVar ID3D11RenderTargetView *globalD3DRenderTargetView = NULL;

/// Depth/stencil view for use as a depth buffer.
globalVar ID3D11DepthStencilView *globalD3DDepthStencilView = NULL;

/// Texture to associate to the depth/stencil view.
globalVar ID3D11Texture2D *globalD3DDepthStencilBuffer = NULL;

/// Define the functionality of the depth/stencil stages.
globalVar ID3D11DepthStencilState *globalD3DDepthStencilState = NULL;

/// Define the functionality of the rasterizer stage. This rasterizer will be used
/// for solid fills.
globalVar ID3D11RasterizerState *globalD3DRasterizerState = NULL;

/// Defines the dimensions of the viewport.
globalVar D3D11_VIEWPORT globalD3DViewport = {0};

/// This is the feature level that the device/swap chain is created with.
globalVar D3D_FEATURE_LEVEL globalD3DFeatureLevel;

/// Global variable that indicates the state(s) of the application.
globalVar DX11ApplicationState globalApplicationState = DX11_TUTORIAL_APPLICATION_RUNNING;

globalVar int globalWindowState = DX11_TUTORIAL_WINDOW_STATE_WINDOWED;

/// Main vertex buffer to use for storing vertices' data.
globalVar ID3D11Buffer *globalD3DVertexBuffer = NULL;

/// Main index buffer.
globalVar ID3D11Buffer *globalD3DIndexBuffer = NULL;

globalVar ID3D11Buffer *globalD3DConstantBuffers[D3DConstantBufferCount];

globalVar ID3D11Buffer *globalD3DLightPropertiesConstantBuffer = NULL;

/// This will store the properties for one material at a time to be accessed by the pixel shader.
globalVar ID3D11Buffer *globalD3DMaterialPropertiesConstantBuffer = NULL;

/// This will store the variables for the multiple instance vertex shader.
globalVar ID3D11Buffer *globalD3DInstancesConstantBuffer = NULL;

/// Main description of how the vertex structure attached to the IA stage is laid
/// out in memory.
globalVar ID3D11InputLayout *globalD3DVertexInputLayout = NULL;

globalVar ID3D11InputLayout *globalD3DVertexTextureInputLayout = NULL;

/// Main description of how the vertex structure for instances attached to the IA
/// stage is laid out in memory.
globalVar ID3D11InputLayout *globalInstancedInputLayout = NULL;

globalVar dx::XMMATRIX globalWorldM44; // TODO: (sonictk) Currently this is used to rotate test cube
globalVar dx::XMMATRIX globalViewM44;
globalVar dx::XMMATRIX globalProjM44;

globalVar ID3DBlob *globalVertexShaderBlob;
globalVar ID3DBlob *globalPixelShaderBlob;
globalVar ID3DBlob *globalSingleInstanceVertexShaderBlob;
globalVar ID3DBlob *globalMultipleInstanceVertexShaderBlob;
globalVar ID3DBlob *globalTexturedLitPixelShaderBlob;
globalVar ID3DBlob *errorBlob;

globalVar ID3D11VertexShader *globalVertexShader = NULL;
globalVar ID3D11PixelShader *globalPixelShader = NULL;

globalVar ID3D11VertexShader *globalSingleInstanceVertexShader = NULL;
globalVar ID3D11VertexShader *globalMultipleInstanceVertexShader = NULL;
globalVar ID3D11PixelShader *globalTexturedLitPixelShader = NULL;

/// This will store all the material properties globally for everything.
globalVar PhongMaterialProperties globalPhongMaterials[D3D11_MAX_MATERIAL_PROPERTIES_COUNT];

globalVar ID3D11SamplerState *globalD3DSamplerState;

globalVar const dx::XMVECTOR globalDefaultCameraPosition = dx::XMVectorSet(0, 0, -10, 0);
globalVar const dx::XMVECTOR globalDefaultCameraTargetVector = dx::XMVectorSet(0, 0, 1, 0);
globalVar const dx::XMVECTOR globalDefaultCameraUpVector = dx::XMVectorSet(0, 1, 0, 0); // NOTE: (sonictk) Y-up

globalVar dx::XMVECTOR globalCameraPosition = globalDefaultCameraPosition;
globalVar dx::XMVECTOR globalCameraTargetVector = globalDefaultCameraTargetVector;
globalVar dx::XMVECTOR globalCameraUpVector = globalDefaultCameraUpVector;

globalVar float globalClientCenterPosX = 0.0f;
globalVar float globalClientCenterPosY = 0.0f;

/// NOTE: (sonictk) These should be specified in radians.
globalVar float globalCameraYaw = 0.0f;
globalVar float globalCameraPitch = 0.0f;
globalVar float globalCameraRoll = 0.0f;

globalVar LONG globalMouseDeltaX = 0;
globalVar LONG globalMouseDeltaY = 0;

globalVar HCURSOR globalMouseCursorHandle = NULL;

globalVar IXAudio2 *globalXAudio2Engine = NULL;
globalVar IXAudio2MasteringVoice *globalXAudio2MasteringVoice = NULL;

globalVar X3DAUDIO_HANDLE globalX3DInstance;

globalVar X3DAUDIO_LISTENER globalX3DListener;
globalVar X3DAUDIO_CONE globalListenerCone;

globalVar X3DAUDIO_EMITTER globalX3DEmitter;
globalVar X3DAUDIO_CONE globalEmitterCone;

globalVar dx::XMFLOAT3 globalX3DLastCameraPosition = {0.0f, 0.0f, 0.0f};

globalVar BYTE *globalAudioDataBuffer = NULL;
globalVar FLOAT32 *globalX3DChannelsMatrix = NULL;
globalVar FLOAT32 *globalX3DDelayTimes = NULL;

IXAudio2SubmixVoice *globalSubmixVoice = NULL;
IXAudio2SourceVoice *globalBGSourceVoice = NULL;
IXAudio2SourceVoice *globalSFXSourceVoice = NULL;


/// Just a test of loading a test audio file and playing it.
DX11TutorialStatus TestPlayAudio(const char *audioFile,
								 XAUDIO2_VOICE_SENDS *sendList,
								 UINT32 numLoops,
								 IXAudio2SourceVoice **sourceVoice,
								 UINT32 sourceVoiceFlags,
								 float volume)
{
	HANDLE hFile = CreateFileA((LPCTSTR)audioFile,
							   GENERIC_READ,
							   FILE_SHARE_READ, NULL,
							   OPEN_EXISTING,
							   FILE_ATTRIBUTE_NORMAL,
							   NULL);

	if (hFile == INVALID_HANDLE_VALUE) {
		return DX11_TUTORIAL_FILE_NOT_FOUND;
	}

	WAVEFORMATEXTENSIBLE waveFormat;
	ZeroMemory(&waveFormat, sizeof(WAVEFORMATEXTENSIBLE));

	XAUDIO2_BUFFER waveDataBuffer;
	ZeroMemory(&waveDataBuffer, sizeof(XAUDIO2_BUFFER));

	DWORD waveDataBufferSize = 0;
	XAudio2Status audioStatus = LoadWAVAudio(hFile, waveFormat, waveDataBuffer, NULL, waveDataBufferSize);
	if (audioStatus != XAudio2Status_Success) {
		return DX11_TUTORIAL_INVALID_FILE_READ;
	}

	// TODO: (sonictk) Allocate from arena for this instead of here.
	globalAudioDataBuffer = (BYTE *)HeapAlloc(globalProcessHeap, HEAP_ZERO_MEMORY, (SIZE_T)waveDataBufferSize);
	if (globalAudioDataBuffer == NULL) {
		return DX11_TUTORIAL_OUT_OF_MEMORY;
	}

	audioStatus = LoadWAVAudio(hFile, waveFormat, waveDataBuffer, globalAudioDataBuffer, waveDataBufferSize);
	if (audioStatus != XAudio2Status_Success) {
		return DX11_TUTORIAL_INVALID_FILE_READ;
	}

	waveDataBuffer.LoopCount = numLoops;

	HRESULT hr = globalXAudio2Engine->CreateSourceVoice(sourceVoice,
														&waveFormat.Format,
														sourceVoiceFlags,
														XAUDIO2_DEFAULT_FREQ_RATIO,
														NULL,
														sendList,
														NULL);
	if (FAILED(hr)) {
		return DX11_TUTORIAL_FAILED_TO_INIT_AUDIO_SOURCE;
	}

	hr = (*sourceVoice)->SubmitSourceBuffer(&waveDataBuffer);
	if (FAILED(hr)) {
		return DX11_TUTORIAL_AUDIO_PLAYBACK_FAILURE;
	}

	hr = (*sourceVoice)->SetVolume(volume);
	if (FAILED(hr)) {
		return DX11_TUTORIAL_AUDIO_PLAYBACK_FAILURE;
	}

	hr = (*sourceVoice)->Start();
	if (FAILED(hr)) {
		return DX11_TUTORIAL_AUDIO_PLAYBACK_FAILURE;
	}

	return DX11_TUTORIAL_SUCCESS;
}


void TestInitializeX3DAudio()
{
	// TODO: (sonictk) Decide if the sound cones are needed or not. Seemed to crash
	// last time without them!
	ZeroMemory(&globalListenerCone, sizeof(X3DAUDIO_CONE));
	globalListenerCone.InnerAngle = X3DAUDIO_PI;
	globalListenerCone.OuterAngle = X3DAUDIO_2PI;
	globalListenerCone.InnerVolume = 1.0f;
	globalListenerCone.OuterVolume = 1.0f;
	globalListenerCone.InnerLPF = 1.0f;
	globalListenerCone.OuterLPF = 1.0f;
	globalListenerCone.InnerReverb = 1.0f;
	globalListenerCone.OuterReverb = 1.0f;
	// globalX3DListener.pCone = &globalListenerCone;
	globalX3DListener.pCone = NULL;

	ZeroMemory(&globalEmitterCone, sizeof(X3DAUDIO_CONE));
	globalEmitterCone.InnerAngle = X3DAUDIO_2PI;
	globalEmitterCone.OuterAngle = X3DAUDIO_2PI;
	globalEmitterCone.InnerVolume = 1.0f;
	globalEmitterCone.OuterVolume = 1.0f;
	globalEmitterCone.InnerLPF = 1.0f;
	globalEmitterCone.OuterLPF = 1.0f;
	globalEmitterCone.InnerReverb = 1.0f;
	globalEmitterCone.OuterReverb = 1.0f;
	// globalX3DEmitter.pCone = &globalEmitterCone;
	globalX3DEmitter.pCone = NULL;

	globalX3DEmitter.InnerRadius = 10.0f;
	globalX3DEmitter.InnerRadiusAngle = X3DAUDIO_PI / 4.0f;
	globalX3DEmitter.ChannelCount = 2;
	globalX3DEmitter.ChannelRadius = 0.1f;

	FLOAT32 *testEmitterChannelAzimuths = (FLOAT32 *)HeapAlloc(globalProcessHeap, 0, sizeof(FLOAT32) * 2);
	testEmitterChannelAzimuths[0] = 0.0f;
	testEmitterChannelAzimuths[1] = 0.1f;
	globalX3DEmitter.pChannelAzimuths = testEmitterChannelAzimuths;

	globalX3DEmitter.pVolumeCurve = NULL;
	globalX3DEmitter.pLFECurve = NULL;
	globalX3DEmitter.pLPFDirectCurve = NULL;
	globalX3DEmitter.pLPFReverbCurve = NULL;
	globalX3DEmitter.pReverbCurve = NULL;
	globalX3DEmitter.CurveDistanceScaler = 1.0f;
	globalX3DEmitter.DopplerScaler = 0.0f;

	XAUDIO2_VOICE_DETAILS masteringVoiceDetails;
	globalXAudio2MasteringVoice->GetVoiceDetails(&masteringVoiceDetails);

	// NOTE: (sonictk) Since X3DAudioCalcuate does not allocate memory for any of its parameters,
	// we have to do so here.
	// "The array must have at least (SrcChannelCount � DstChannelCount) elements." - MSDN
	globalX3DChannelsMatrix = (FLOAT32 *)HeapAlloc(globalProcessHeap, HEAP_ZERO_MEMORY, masteringVoiceDetails.InputChannels * 2 * sizeof(FLOAT32));
	globalX3DDelayTimes = (FLOAT32 *)HeapAlloc(globalProcessHeap, HEAP_ZERO_MEMORY, masteringVoiceDetails.InputChannels * sizeof(FLOAT32));
}


void X3DAudioUpdate(const float deltaTime)
{
	dx::XMFLOAT3 cameraFront;
	dx::XMVECTOR globalCameraTargetVectorNormalized = dx::XMVector3Normalize(globalCameraTargetVector);
	cameraFront.x = dx::XMVectorGetX(globalCameraTargetVectorNormalized);
	cameraFront.y = dx::XMVectorGetY(globalCameraTargetVectorNormalized);
	cameraFront.z = dx::XMVectorGetZ(globalCameraTargetVectorNormalized);
	globalX3DListener.OrientFront = cameraFront;

	dx::XMFLOAT3 cameraTop;
	dx::XMVECTOR globalCameraUpVectorNormalized = dx::XMVector3Normalize(globalCameraUpVector);
	cameraTop.x = dx::XMVectorGetX(globalCameraUpVectorNormalized);
	cameraTop.y = dx::XMVectorGetY(globalCameraUpVectorNormalized);
	cameraTop.z = dx::XMVectorGetZ(globalCameraUpVectorNormalized);
	globalX3DListener.OrientTop = cameraTop;

	dx::XMFLOAT3 cameraPosition;
	cameraPosition.x = dx::XMVectorGetX(globalCameraPosition);
	cameraPosition.y = dx::XMVectorGetY(globalCameraPosition);
	cameraPosition.z = dx::XMVectorGetZ(globalCameraPosition);
	globalX3DListener.Position = cameraPosition;

	dx::XMFLOAT3 cameraVelocity;
	if (deltaTime > FLT_EPSILON) {
		cameraVelocity.x = (cameraPosition.x - globalX3DLastCameraPosition.x) / deltaTime;
		cameraVelocity.y = (cameraPosition.y - globalX3DLastCameraPosition.y) / deltaTime;
		cameraVelocity.z = (cameraPosition.z - globalX3DLastCameraPosition.z) / deltaTime;
	} else {
		cameraVelocity.x = 0.0f;
		cameraVelocity.y = 0.0f;
		cameraVelocity.z = 0.0f;
	}

	globalX3DListener.Velocity = cameraVelocity;

	// TODO: (sonictk) Set to identity for testing
	dx::XMFLOAT3 cubeFront = {0, 0, 1};
	dx::XMFLOAT3 cubeTop = {0, 1, 0};
	dx::XMFLOAT3 cubePosition = {0, 0, 0};
	dx::XMFLOAT3 cubeVelocity = {0, 0, 0};

	dx::XMVECTOR cubePositionRow = globalWorldM44.r[3];
	cubePosition.x = dx::XMVectorGetX(cubePositionRow);
	cubePosition.y = dx::XMVectorGetY(cubePositionRow);
	cubePosition.z = dx::XMVectorGetZ(cubePositionRow);

	globalX3DEmitter.OrientFront = cubeFront;
	globalX3DEmitter.OrientTop = cubeTop;
	globalX3DEmitter.Position = cubePosition;
	globalX3DEmitter.Velocity = cubeVelocity;

	X3DAUDIO_DSP_SETTINGS DSPSettings;
	ZeroMemory(&DSPSettings, sizeof(X3DAUDIO_DSP_SETTINGS));
	DSPSettings.pMatrixCoefficients = globalX3DChannelsMatrix;
	DSPSettings.pDelayTimes = globalX3DDelayTimes;
	DSPSettings.SrcChannelCount = 2;

	XAUDIO2_VOICE_DETAILS masteringVoiceDetails;
	globalXAudio2MasteringVoice->GetVoiceDetails(&masteringVoiceDetails);
	DSPSettings.DstChannelCount = masteringVoiceDetails.InputChannels;

	// TODO: (sonictk) This call causes a segfault if dspsettings is global; maybe due to the multithreaded nature of the call
	X3DAudioCalculate(globalX3DInstance,
					  &globalX3DListener,
					  &globalX3DEmitter,
					  X3DAUDIO_CALCULATE_MATRIX|X3DAUDIO_CALCULATE_DELAY|X3DAUDIO_CALCULATE_REVERB|X3DAUDIO_CALCULATE_DOPPLER|X3DAUDIO_CALCULATE_EMITTER_ANGLE,
					  &DSPSettings);

	globalSFXSourceVoice->SetOutputMatrix(NULL, 2, 2, DSPSettings.pMatrixCoefficients);

	globalSFXSourceVoice->SetFrequencyRatio(DSPSettings.DopplerFactor);
	// TODO: (sonictk) Apply reverb to one submix voice, and the volume to another (or to the master voice).
	// This is how they chain the effects together.
	// globalSFXSourceVoice->SetOutputMatrix(globalSubmixVoice, 2, 2, &DSPSettings.ReverbLevel);

	// TODO: (sonictk) filter settings not supported when creating the voices
	// XAUDIO2_FILTER_PARAMETERS filterParams;
	// filterParams.Type = LowPassFilter;
	// filterParams.Frequency = 2.0f * sinf(PI / 6.0f) * globalX3DDSPSettings.LPFDirectCoefficient;
	// filterParams.OneOverQ = 1.0f;

	// globalSFXSourceVoice->SetFilterParameters(&filterParams);

	globalXAudio2MasteringVoice->SetVolume(globalMasterVolume);

	globalX3DLastCameraPosition = cameraPosition;
}


/**
 * Clears the backbuffer and depth/stencil buffers.
 *
 * @param color	The colour to use to clear the backbuffer to.
 * @param depth	The depth value to clear the depth/stencil buffer to.
 * @param stencil	The stencil value to clear the depth/stencil buffer to.
 */
void D3DClearBuffers(const FLOAT color[4], FLOAT depth, UINT8 stencil)
{
	globalD3DDeviceContext->ClearRenderTargetView(globalD3DRenderTargetView, color);
	globalD3DDeviceContext->ClearDepthStencilView(globalD3DDepthStencilView,
												  D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL,
												  depth,
												  stencil);
}


/**
 * Presents the contents of the backbuffer to the screen.
 *
 * @param vSync 	Set to ``true`` if presentation should be synced to vblank timing.
 */
void D3DPresent(bool vSync)
{
	if (vSync == true) {
		globalD3DSwapChain->Present(1, 0); // NOTE: (sonictk) Sync presentation after the 1st vblank.
	} else {
		globalD3DSwapChain->Present(0, 0); // NOTE: (sonictk) Presentation occurs immediately.
	}
}


/**
 * Run every tick in order to update all test resources' states.
 *
 * @param deltaTime 	The amount of time that has passed between calls to this function.
 */
void TestD3DUpdate(const float deltaTime)
{
	// TODO: (sonictk) Since global time is updated only once a second, need a better
	// consistent time for this.
	UpdateLights(globalTimeElapsed);

	globalD3DDeviceContext->UpdateSubresource(globalD3DLightPropertiesConstantBuffer,
											  0,
											  NULL,
											  &globalLightsProperties,
											  0,
											  0);

	globalD3DDeviceContext->UpdateSubresource(globalD3DMaterialPropertiesConstantBuffer,
											  0,
											  NULL,
											  &globalPhongMaterials[0],
											  0,
											  0);

	dx::XMMATRIX viewProjM44 = globalViewM44 * globalProjM44;
	globalD3DDeviceContext->UpdateSubresource(globalD3DInstancesConstantBuffer, 0, NULL, &viewProjM44, 0, 0);
}


/**
 * This is run every tick in order to update all state necessary for the application.
 *
 * @param deltaTime 	The amount of time to integrate into updating the state.
 */
void D3DUpdate(const float deltaTime)
{
	dx::XMVECTOR currentCameraRotation = dx::XMQuaternionRotationRollPitchYaw(globalCameraPitch, globalCameraYaw, globalCameraRoll);

	if (globalKeysDown & globalKeyMask_Fw) {
		dx::XMVECTOR displacement = globalCameraTargetVector * globalCameraSpeed;
		globalCameraPosition += displacement;
	}
	if (globalKeysDown & globalKeyMask_Bk) {
		dx::XMVECTOR displacement = globalCameraTargetVector * globalCameraSpeed;
		globalCameraPosition -= displacement;
	}

	if (globalKeysDown & globalKeyMask_Lf) {
		globalCameraPosition += dx::XMVector4Normalize(dx::XMVector3Cross(globalCameraTargetVector, globalCameraUpVector)) * globalCameraSpeed;
	}
	if (globalKeysDown & globalKeyMask_Rt) {
		globalCameraPosition -= dx::XMVector4Normalize(dx::XMVector3Cross(globalCameraTargetVector, globalCameraUpVector)) * globalCameraSpeed;
	}

	if (globalKeysDown & globalKeyMask_Up) {
		globalCameraPosition += dx::XMVectorSet(0, 1, 0, 0) * globalCameraSpeed;
	}
	if (globalKeysDown & globalKeyMask_Dn) {
		globalCameraPosition -= dx::XMVectorSet(0, 1, 0, 0) * globalCameraSpeed;
	}

	float deltaCameraRoll = 0.0f;
	globalVar const float rollClamp = PI / 2.0f;
	if (globalKeysDown & globalKeyMask_RollLf) {
		deltaCameraRoll += globalCameraRollSpeed;
		globalCameraRoll += deltaCameraRoll;
	}
	if (globalKeysDown & globalKeyMask_RollRt) {
		deltaCameraRoll -= globalCameraRollSpeed;
		globalCameraRoll += deltaCameraRoll;
	}

	if (globalModifiersDown & globalModifiersMask_Shift) {
		globalCameraSpeed = 1.0f;
	} else {
		globalCameraSpeed = globalDefaultCameraSpeed;
	}

	// TODO: (sonictk) Need to figure out what to do for roll past 90 degrees on either side to opposite vertical.
	// For now, we just clamp.
	// TODO: (sonictk) There is camera jittering when looking back past negative Z.
	if (globalCameraRoll > FLT_EPSILON) {
		globalCameraRoll = clamp(globalCameraRoll, 0.0f, rollClamp);
	} else {
		globalCameraRoll = clamp(globalCameraRoll, -rollClamp, 0.0f);
	}

	// NOTE: (sonictk) We reset the deltas because raw input stops sending messages
	// once the mouse device has stopped moving.
	float deltaCameraPitch = 0.0f;
	float deltaCameraYaw = 0.0f;
	if (abs(globalMouseDeltaX) >= globalMouseDeltaThreshold || abs(globalMouseDeltaY) >= globalMouseDeltaThreshold) {
		deltaCameraYaw = (float)globalMouseDeltaX * globalMouseSensitivity;
		deltaCameraPitch = (float)globalMouseDeltaY * globalMouseSensitivity;

		dx::XMVECTOR cameraRollVector = dx::XMVectorSet(cosf(globalCameraRoll), sinf(globalCameraRoll), 0, 0);
		globalVar const dx::XMVECTOR cameraZeroRollVector = dx::XMVectorSet(1, 0, 0, 0);
		dx::XMVECTOR cameraRollAngleBetweenVerticalVector = dx::XMVector2AngleBetweenVectors(cameraZeroRollVector, cameraRollVector);
		float cameraRollAngleBetweenVertical = dx::XMVectorGetX(cameraRollAngleBetweenVerticalVector);
		float pitchYawRollLerpFactor = cameraRollAngleBetweenVertical / (PI / 2.0f);

		// NOTE: (sonictk) Map pitch/yaw differently when roll increases
		float origDeltaCameraPitch = deltaCameraPitch;
		float origDeltaCameraYaw = deltaCameraYaw;

		// NOTE: (sonictk) Negate pitch/yaw depending on roll vector quadrant
		float cameraRollVectorY = dx::XMVectorGetY(cameraRollVector);
		if (cameraRollVectorY > FLT_EPSILON) {
			deltaCameraPitch = lerp(origDeltaCameraPitch, -origDeltaCameraYaw, pitchYawRollLerpFactor);
			deltaCameraYaw = lerp(origDeltaCameraYaw, origDeltaCameraPitch, pitchYawRollLerpFactor);
		} else {
			deltaCameraPitch = lerp(origDeltaCameraPitch, origDeltaCameraYaw, pitchYawRollLerpFactor);
			deltaCameraYaw = lerp(origDeltaCameraYaw, -origDeltaCameraPitch, pitchYawRollLerpFactor);
		}

		globalCameraYaw += deltaCameraYaw;
		float newCameraPitch = globalCameraPitch + deltaCameraPitch;

		// NOTE: (sonictk) Clamp the pitch to looking up at sky and down at feet, but no further
		// this prevents the view from getting "reversed"
		globalVar const float absPitchClamp = 85.0f * DEG_TO_RAD;
		newCameraPitch = clamp(newCameraPitch, -absPitchClamp, absPitchClamp);
		deltaCameraPitch = newCameraPitch - globalCameraPitch;
		globalCameraPitch = newCameraPitch;
	}

	dx::XMVECTOR deltaCameraRotation = dx::XMQuaternionRotationRollPitchYaw(deltaCameraPitch, deltaCameraYaw, deltaCameraRoll);
	deltaCameraRotation = dx::XMQuaternionNormalize(deltaCameraRotation); // NOTE: (sonictk) Minimize chance of rounding error math-wise

	// NOTE: (sonictk) Since converting a quaternion's basis is too difficult for
	// me, I'm just applying the rotations onto the default vectors.
	globalCameraTargetVector = dx::XMVector3Rotate(globalDefaultCameraTargetVector, currentCameraRotation);
	globalCameraTargetVector = dx::XMVector3Normalize(globalCameraTargetVector);
	globalCameraTargetVector = dx::XMVector3Rotate(globalCameraTargetVector, deltaCameraRotation);
	globalCameraTargetVector = dx::XMVector3Normalize(globalCameraTargetVector);

	globalCameraUpVector = dx::XMVector3Rotate(globalDefaultCameraUpVector, currentCameraRotation);
	globalCameraUpVector = dx::XMVector3Normalize(globalCameraUpVector);
	globalCameraUpVector = dx::XMVector3Rotate(globalCameraUpVector, deltaCameraRotation);
	globalCameraUpVector = dx::XMVector3Normalize(globalCameraUpVector);

	globalViewM44 = dx::XMMatrixLookAtLH(globalCameraPosition, globalCameraPosition + globalCameraTargetVector, globalCameraUpVector);

	// NOTE: (sonictk) This is the method that copies memory from CPU/RAM to
	// GPU memory (non-mappable) across the PCI/PCI-E bus.
	// This updates the cbuffer for ``PerFrame``.
	globalD3DDeviceContext->UpdateSubresource(globalD3DConstantBuffers[D3DConstantBuffer_PerFrame],
											  0,
											  NULL,
											  &globalViewM44,
											  0,
											  0);

	globalMouseDeltaX = 0;
	globalMouseDeltaY = 0;

	// NOTE: (sonictk) Temp. update of global world matrix here to give a rotating cube
	globalWorldM44 = dx::XMMatrixIdentity();
	static float tmpRotationAngle = 0.0f;
	static const float tmpRotationSpeed = 60.0f;

	tmpRotationAngle += tmpRotationSpeed * deltaTime;

	dx::XMVECTOR tmpRotationAxis = dx::XMVectorSet(1, 1, 1, 0);

	globalWorldM44 = dx::XMMatrixRotationAxis(tmpRotationAxis, dx::XMConvertToRadians(tmpRotationAngle));

	globalD3DDeviceContext->UpdateSubresource(globalD3DConstantBuffers[D3DConstantBuffer_PerObject],
											  0,
											  NULL,
											  &globalWorldM44,
											  0,
											  0);

	// NOTE: (sonictk) Compute the exact dimensions at this time of the client area.
	// Need this for calculating the correct projection matrix.
	RECT clientRect;
	BOOL gStatus = GetClientRect(globalMainWindowHandle, &clientRect);
	if (gStatus == 0) {
		MessageBox(globalMainWindowHandle, "Unable to accquire client area dimensions!", 0, 0);

		return;
	}

	LONG clientWidth = clientRect.right - clientRect.left;
	LONG clientHeight = clientRect.bottom - clientRect.top;
	globalProjM44 = dx::XMMatrixPerspectiveFovLH(dx::XMConvertToRadians(globalFieldOfViewAngle),
												 (float)clientWidth / (float)clientHeight,
												 globalNearClip,
												 globalFarClip);

	// NOTE: (sonictk) Update the cbuffer that stores the value of the proj. matrix
	// in the shader. Side note: it is not possible to use this method to partially
	// update a shader-constant buffer.
	// TODO: (sonictk) Maybe the projection matrix should be per-object as well...since it's being updated all the time
	globalD3DDeviceContext->UpdateSubresource(globalD3DConstantBuffers[D3DConstantBuffer_PerApp],
											  0,
											  NULL,
											  &globalProjM44,
											  0,
											  0);

	TestD3DUpdate(deltaTime);
}


void TestD3DDraw()
{
	// NOTE: (sonictk) Now we can finally draw the actual vertices after they have
	// been binded to an input slot and are ready to be fed into the pipeline.

	// NOTE: (sonictk) Draw using the index buffer (instead of defining the vertices
	// repeatedly and then calling Draw()). Draw the test rotating cube
	globalD3DDeviceContext->DrawIndexed(_countof(testVtxIndices), 0, 0);

	// NOTE: (sonictk) Now draw scene
	globalVar const UINT vertexStrides[2] = {sizeof(VertexTexture), sizeof(Plane3DInstanceData)};
	globalVar const UINT offsets[2] = {0, 0};

	ID3D11Buffer *planeBuffers[2] = {globalPlanesVertexBuffer, globalPlanesInstanceBuffer};
	globalD3DDeviceContext->IASetVertexBuffers(0, _countof(planeBuffers), planeBuffers, vertexStrides, offsets);
	globalD3DDeviceContext->IASetInputLayout(globalInstancedInputLayout);
	globalD3DDeviceContext->IASetIndexBuffer(globalPlanesIndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	globalD3DDeviceContext->VSSetShader(globalMultipleInstanceVertexShader, NULL, 0);
	globalD3DDeviceContext->VSSetConstantBuffers(0, 1, &globalD3DInstancesConstantBuffer);

	globalD3DDeviceContext->PSSetShader(globalTexturedLitPixelShader, NULL, 0);

	ID3D11Buffer *pixelShaderConstantBuffers[2] = {globalD3DMaterialPropertiesConstantBuffer, globalD3DLightPropertiesConstantBuffer};
	globalD3DDeviceContext->PSSetConstantBuffers(0, _countof(pixelShaderConstantBuffers), pixelShaderConstantBuffers);

	globalD3DDeviceContext->DrawIndexedInstanced(_countof(testPlaneIndices), globalNumOfPlanes, 0, 0, 0);
}


/**
 * Draws everything required to the framebuffer and presents it to the screen.
 */
void D3DDraw()
{
	assert(globalD3DDevice);
	assert(globalD3DDeviceContext);

	D3DClearBuffers(DXColorMagenta, 1.0f, 0);

	// NOTE: (sonictk) After we've created the buffer, we need to bind it to an
	// input slot of the device in order to feed the vertices to the pipeline as input.
	UINT vtxBufferStride = sizeof(Vertex);
	UINT vtxBufferOffset = 0; // NOTE: (sonictk) We're not skipping over any vertex data at the front of the buffer.
	globalD3DDeviceContext->IASetVertexBuffers(0,
											   1,
											   &globalD3DVertexBuffer,
											   &vtxBufferStride,
											   &vtxBufferOffset);

	globalD3DDeviceContext->IASetInputLayout(globalD3DVertexInputLayout);

	// NOTE: (sonictk) Bind the index buffer to the pipeline as well.
	globalD3DDeviceContext->IASetIndexBuffer(globalD3DIndexBuffer,
											 DXGI_FORMAT_R32_UINT,
											 vtxBufferOffset);

	// NOTE: (sonictk) Tell D3D how to form geo. prims. from the vertex data by
	// specifying the prim. topog.
	globalD3DDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	globalD3DDeviceContext->VSSetShader(globalVertexShader, NULL, 0);
	globalD3DDeviceContext->VSSetConstantBuffers(0, D3DConstantBufferCount, globalD3DConstantBuffers);

	globalD3DDeviceContext->PSSetShader(globalPixelShader, NULL, 0);

	// globalD3DDeviceContext->IASetIndexBuffer(globalD3DIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	TestD3DDraw();
}


/**
 * Calculates the average FPS per second of the application. This function should
 * be called in a tight update loop (i.e. every frame) in order to update the result.
 *
 * @param fps	The storage for the FPS average result. If not enough time has elapsed,
 * 			the result will not be updated.
 *
 * @return 	Returns ``DX11_TUTORIAL_SUCCESS`` if the FPS has been updated, or
 * 			``DX11_TUTORIAL_FRAMERATE_INTERVAL_NOT_YET_MET`` if not enough time
 * 			has passed between calls to this function.
 */
DX11TutorialStatus CalculateApplicationFrameStats(float &fps)
{
	globalTimeElapsed = (double)globalHighResolutionTimer.QuadPart / (double)globalCPUTimerFreq.QuadPart;
	static double timeElapsedSinceLastCalc = globalTimeElapsed;

	static int frameCounter = 0;
	frameCounter++;

	if (globalTimeElapsed - timeElapsedSinceLastCalc >= 1.0f) {
		fps = (float)frameCounter;

		frameCounter = 0;
		timeElapsedSinceLastCalc += 1.0f;

		return DX11_TUTORIAL_SUCCESS;
	}

	return DX11_TUTORIAL_FRAMERATE_INTERVAL_NOT_YET_MET;
}


void D3DDrawGUI()
{
	static bool showControls = true;

	// NOTE: (sonictk) Draw HUD elements using ImGui; start the ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	{
		// NOTE: (sonictk) Set initial size of ImGui window.
		globalVar const ImVec2 globalImGuiWindowInitialSize = ImVec2(0, 0); // NOTE: (sonictk) Setting to 0, 0 size will auto-resize to contents from second frame onwards.
		ImGui::SetNextWindowSize(globalImGuiWindowInitialSize, ImGuiCond_Always);

		globalVar const ImVec2 globalImGuiWindowInitialPos = ImVec2(50, 50);
		ImGui::SetNextWindowPos(globalImGuiWindowInitialPos, ImGuiCond_Always);

		globalVar const ImGuiWindowFlags imGuiWindowFlags = ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoResize;

		ImGui::Begin("Hello, world!", NULL, imGuiWindowFlags);

		if (showControls) {
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::Text("Global time elapsed %.3f ", globalTimeElapsed);

			ImGui::SliderFloat("Master Volume", &globalMasterVolume, 0.0f, 1.0f, "%.1f");

			if (ImGui::Button("Hide")) {
				showControls = false;
			}
		} else {
			if (ImGui::Button("Show controls")) {
				showControls = true;
			}
		}

		ImGui::End();
	}
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}


/**
 * The main application loop.
 * Implementation from: https://docs.microsoft.com/en-us/windows/desktop/direct3dgetstarted/work-with-dxgi
 *
 * @return 	Additional information about the message that triggered the termination of the loop.
 */
int Run()
{
	bool bGotMsg;
	MSG msg = {0};
	msg.message = WM_NULL;

	// NOTE: (sonictk) According to MSDN: dispatches incoming sent messages, checks
	// the thread message queue for a posted message, and retrives the message (if any exist.)
	// Here, we say that we want no filtering of messages and to _not_ remove the
	// message from the queue after processing.
	// TODO: (sonictk) This first peek doesn't seem necessary...
	// PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE);

	// TODO: (sonictk) Maybe set the main app thread using SetThreadAffinityMask()
	// so that we avoid this giving weird results even if there are bugs in the
	// BIOS or HAL?
	BOOL status = QueryPerformanceCounter(&globalHighResolutionTimer);
	if (status == 0) {
		DWORD winError = GetLastError();
		MessageBox(0, "Failed to get high-resolution timing from the CPU! This hardware is not suppoorted.", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	globalVar float globalMaxTimeStep = 1.0f / globalTargetFPS;

	while (msg.message != WM_QUIT) {
		// NOTE: (sonictk) Process window events.
		// We use PeekMessage() so that we can use idle time to render the scene.
		// If we used GetMessage() instead, it would put the thread to sleep while
		// waiting for a message; we do not want this behavior. If there are no
		// Windows messages to be processed, we want to run our own rendering code instead.
		bGotMsg = (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE) != 0);
		if (bGotMsg) {
			TranslateMessage(&msg); // NOTE: (sonictk) Translates virtual key messages into character messages.
			DispatchMessage(&msg); // NOTE: (sonictk) Dispatch the translated message.

			continue;
		}

		// NOTE: (sonictk) No Windows messages remaining to be processed in the queue;
		// time to run our own rendering game code now!
		switch (globalApplicationState) {
		case DX11_TUTORIAL_APPLICATION_PAUSED:
			Sleep(100);

			break;

		case DX11_TUTORIAL_APPLICATION_RUNNING:
		default:
			CalculateApplicationFrameStats(globalCurrentApplicationFPS);

			// NOTE: (sonictk) If there are no messages to be processed, run render code.
			LARGE_INTEGER currentTime;
			QueryPerformanceCounter(&currentTime);
			float deltaTime = (float)(currentTime.QuadPart - globalHighResolutionTimer.QuadPart) / (float)globalCPUTimerFreq.QuadPart;

			globalHighResolutionTimer = currentTime;
			// NOTE: (sonictk) Cap delta time to the max time step
			deltaTime = deltaTime < globalMaxTimeStep ? deltaTime : globalMaxTimeStep;

			D3DUpdate(deltaTime);

			X3DAudioUpdate(deltaTime);

			D3DDraw();

			D3DDrawGUI();

			D3DPresent(globalVSyncEnabled);

			break;
		}
	}

	ShowCursor(TRUE);

	return (int)msg.wParam; // NOTE: (sonictk) As guidance from MSDN dictates.
}


/**
 * This callback is executed when the window is resized.
 */
DX11TutorialStatus OnResize()
{
	assert(globalD3DDeviceContext);
	assert(globalD3DDevice);
	assert(globalD3DSwapChain);

	// NOTE: (sonictk) Release the old views as they hold references to the buffers
	// that we are destroying along with the depth-stencil buffer.
	ReleaseCOM(globalD3DRenderTargetView);
	ReleaseCOM(globalD3DDepthStencilView);
	ReleaseCOM(globalD3DDepthStencilBuffer);

	// NOTE: (sonictk) Resize the swap chain and re-create the render target view.
	RECT clientRect;
	if (GetClientRect(globalMainWindowHandle, &clientRect) == 0) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; could not get window client area!", 0, 0);

		return DX11_TUTORIAL_GET_CLIENT_RECT_FAILED;
	}

	LONG clientWidth = clientRect.right - clientRect.left;
	LONG clientHeight = clientRect.bottom - clientRect.top;

	HRESULT status = globalD3DSwapChain->ResizeBuffers(1,
													   clientWidth,
													   clientHeight,
													   DXGI_FORMAT_R8G8B8A8_UNORM,
													   0);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to resize the swap chain buffers; an unhandled error occurred!", 0, 0);

		return DX11_TUTORIAL_D3D_RESIZE_BUFFERS_FAILED;
	}

	// TODO: (sonictk) Figure out if I can cleanup the device, adapter and factory
	// immediately after creating the swap chain? Or should I keep them around in
	// memory until application exit?
	// NOTE: (sonictk) In order to bind the backbuffer to the output-merger (OM)
	// stage of the rendering pipeline (so that D3D can render onto it), we need
	// to create a render target view to it.
	ID3D11Texture2D *backBuffer;

	// NOTE: (sonictk) This is according to guidance from MSDN: see example given.
	// The swap chain's backbuffer is automatically created based on the DXGI_SWAP_CHAIN_DESC
	// passed in earlier to CreateSwapChain() so we do not need to manually create
	// a texture; but we still need to associate the backbuffer to a render target
	// view in order to render to it.
	status = globalD3DSwapChain->GetBuffer(0,
										   __uuidof(ID3D11Texture2D),
										   reinterpret_cast<void **>(&backBuffer));
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to access the swap-chain's backbuffer!", 0, 0);

		return DX11_TUTORIAL_D3D_BACKBUFFER_ACCESS_FAILED;
	}

	status = globalD3DDevice->CreateRenderTargetView(backBuffer,
													 NULL, // NOTE: (sonictk) Creates view to the first mipmap level since the backbuffer has only 1 mipmap level
													 &globalD3DRenderTargetView);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to create a render target view for the back-buffer!", 0, 0);

		return DX11_TUTORIAL_D3D_CREATING_RENDER_TARGET_FAILED;
	}

	// NOTE: (sonictk) GetBuffer() increases the COM refcount to the backbuffer,
	// thus we release it here once we're done with it.
	backBuffer->Release();

	// NOTE: (sonictk) Now create the depth/stencil buffer. We cannot render 3D
	// graphics without it, since objects that are drawn far away need to be drawn
	// behind objects that are closer regardless of the order in which they are
	// rendered.
	D3D11_TEXTURE2D_DESC depthStencilBufferDesc;
	ZeroMemory(&depthStencilBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	depthStencilBufferDesc.Width = clientWidth;
	depthStencilBufferDesc.Height = clientHeight;
	depthStencilBufferDesc.MipLevels = 1; // NOTE: (sonictk) Multisample the texture
	depthStencilBufferDesc.ArraySize = 1;
	depthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; // NOTE: (sonictk) 24 bit depth buffer and 8 bits for stencil buffer.
	depthStencilBufferDesc.SampleDesc.Count = globalD3DMSAASampleCount;
	depthStencilBufferDesc.SampleDesc.Quality = globalD3DMSAASampleCount == 1 ? 0 : globalD3DMSAASampleQuality - 1; // NOTE: (sonictk) Must match values given when creating the swap chain.
	depthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilBufferDesc.CPUAccessFlags = 0;
	depthStencilBufferDesc.MiscFlags = 0;

	status = globalD3DDevice->CreateTexture2D(&depthStencilBufferDesc,
											  NULL, // NOTE: (sonictk) Since this is the depth/stencil buffer, we do not need to fill it with any initial data. D3D will write to it directly when performing depth buffering and stencil operations.
											  &globalD3DDepthStencilBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to create a texture for the depth/stencil buffer!", 0, 0);

		return DX11_TUTORIAL_D3D_CREATING_RENDER_TARGET_FAILED;
	}

	status = globalD3DDevice->CreateDepthStencilView(globalD3DDepthStencilBuffer,
													 NULL, // NOTE: (sonictk) The created view will access mipmap level 0 of the entire resource. Since we created the depth/stencil buffer as a typed buffer, this can be null.
													 &globalD3DDepthStencilView);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to create a depth/stencil view!", 0, 0);

		return DX11_TUTORIAL_D3D_CREATING_VIEW_FAILED;
	}

	// NOTE: (sonictk) We create a depth/stencil state object which controls how
	// depth-stencil testing is performed by the OM stage.
	D3D11_DEPTH_STENCIL_DESC depthStencilStateDesc;
	ZeroMemory(&depthStencilStateDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	depthStencilStateDesc.DepthEnable = TRUE;
	depthStencilStateDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL; // NOTE: (sonictk) Allow writing to the depth-stencil buffer.
	depthStencilStateDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilStateDesc.StencilEnable = FALSE; // TODO: (sonictk) We'll get back to this later

	status = globalD3DDevice->CreateDepthStencilState(&depthStencilStateDesc,
													  &globalD3DDepthStencilState);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to create a depth/stencil state!", 0, 0);

		return DX11_TUTORIAL_D3D_CREATING_DEPTH_STENCIL_STATE_FAILED;
	}

	// NOTE: (sonictk) We also set up a rasterizer state object which controls
	// how the rasterizer behaves.
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = FALSE; // NOTE: (sonictk) Tris whose verts are clockwise are considered front-facing.
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.MultisampleEnable = globalD3DMSAASampleCount == 1 ? FALSE : TRUE; // NOTE: (sonictk) Uses the quadrilateral line AA algorithm on MSAA targets. Recommended by MSDN.
	rasterizerDesc.AntialiasedLineEnable = FALSE;

	status = globalD3DDevice->CreateRasterizerState(&rasterizerDesc,
													&globalD3DRasterizerState);
	if (status != S_OK) {
		switch(status) {
		case E_OUTOFMEMORY:
			MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; insufficient memory available to create the compute shader for the rasterizer!", 0, MB_OK|MB_ICONERROR);

			return DX11_TUTORIAL_D3D_OUT_OF_MEMORY;

		default:
			MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while attempting to create a rasterizer state!", 0, MB_OK|MB_ICONERROR);

			return DX11_TUTORIAL_D3D_CREATING_RASTERIZER_STATE_FAILED;
		}
	}

	// NOTE: (sonictk) Update the device with the new rasterizer state block.
	globalD3DDeviceContext->RSSetState(globalD3DRasterizerState);

	// NOTE: (sonictk) Here we bind the views to the output-merger stage
	globalD3DDeviceContext->OMSetRenderTargets(1,
											   &globalD3DRenderTargetView, // NOTE: (sonictk) This points to the first element in an array of render target view pointers to bind to the pipeline. We only have 1 for now.
											   globalD3DDepthStencilView);

	// NOTE: (sonictk) The portion of the backbuffer that we draw into is called
	// the **viewport**.
	ZeroMemory(&globalD3DViewport, sizeof(D3D11_VIEWPORT));
	globalD3DViewport.TopLeftX = 0.0f;
	globalD3DViewport.TopLeftY = 0.0f;
	globalD3DViewport.Width = (FLOAT)clientWidth;
	globalD3DViewport.Height = (FLOAT)clientHeight;
	globalD3DViewport.MinDepth = 0.0f;
	globalD3DViewport.MaxDepth = 1.0f;

	// NOTE: (sonictk) Bind the viewport to the rasterizer.
	globalD3DDeviceContext->RSSetViewports(1, &globalD3DViewport);

	// NOTE: (sonictk) Since the window is resized, update the aspect ratio and
	// re-compute the projection matrix.
	globalProjM44 = dx::XMMatrixPerspectiveFovLH(dx::XMConvertToRadians(45.0f),
												 (float)clientWidth / (float)clientHeight,
												 globalNearClip,
												 globalFarClip);

	globalClientCenterPosX = (float)clientWidth / 2.0f;
	globalClientCenterPosY = (float)clientHeight / 2.0f;

	return DX11_TUTORIAL_SUCCESS;
}


DX11TutorialStatus CreateD3DTestMaterials()
{
	// NOTE: (sonictk) We'll let the default material be a "white plaster" sort of material.
	PhongMaterialProperties defaultMaterialProperties = createPhongMaterialProperties();
	defaultMaterialProperties.material.diffuse = dx::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	defaultMaterialProperties.material.specular = dx::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	defaultMaterialProperties.material.specularExponent = 10.0f;
	defaultMaterialProperties.material.useTexture = true;
	globalPhongMaterials[0] = defaultMaterialProperties;

	// NOTE: (sonictk) Material properties referenced from: http://devernay.free.fr/cours/opengl/materials.html
	PhongMaterialProperties testGreenMaterialProperties = createPhongMaterialProperties();
	testGreenMaterialProperties.material.ambient = dx::XMFLOAT4(0.07568f, 0.61424f, 0.07568f, 1.0f);
	testGreenMaterialProperties.material.diffuse = dx::XMFLOAT4(0.07568f, 0.61424f, 0.07568f, 1.0f);
	testGreenMaterialProperties.material.specular = dx::XMFLOAT4(0.07568f, 0.61424f, 0.07568f, 1.0f);
	testGreenMaterialProperties.material.specularExponent = 76.8f;
	globalPhongMaterials[1] = testGreenMaterialProperties;

	PhongMaterialProperties testRedMaterialProperties = createPhongMaterialProperties();
	testRedMaterialProperties.material.diffuse = dx::XMFLOAT4(0.6f, 0.1f, 0.1f, 1.0f);
	testRedMaterialProperties.material.specular = dx::XMFLOAT4(1.0f, 0.2f, 0.2f, 1.0f);
	testRedMaterialProperties.material.specularExponent = 32.0f;
	globalPhongMaterials[2] = testRedMaterialProperties;

	PhongMaterialProperties testWhiteMaterialProperties = createPhongMaterialProperties();
	testWhiteMaterialProperties.material.ambient = dx::XMFLOAT4(0.25f, 0.20725f, 0.20725f, 1.0f);
	testWhiteMaterialProperties.material.diffuse = dx::XMFLOAT4(1.0f, 0.829f, 0.829f, 1.0f);
	testWhiteMaterialProperties.material.specular = dx::XMFLOAT4(0.296648f, 0.296648f, 0.296648f, 1.0f);
	testWhiteMaterialProperties.material.specularExponent = 11.264f;
	globalPhongMaterials[3] = testWhiteMaterialProperties;

	return DX11_TUTORIAL_SUCCESS;
}


DX11TutorialStatus LoadD3DTestContent()
{
	static char appDir[MAX_PATH];

	getAppPath(appDir, MAX_PATH);

	// NOTE: (sonictk) Fudgery here to convert to wide char for Windows path functions.
	wchar_t appDirW[MAX_PATH];
	mbstowcs(appDirW, appDir, MAX_PATH);

	PathCchRemoveFileSpec((PWSTR)appDirW, MAX_PATH);

	memset(appDir, 0, sizeof(appDir));
	wcstombs(appDir, appDirW, MAX_PATH);

	globalVar char globalCheckerTexturePath[MAX_PATH];

	sprintf(globalCheckerTexturePath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, APPLICATION_TEXTURES_DIRECTORY_NAME, WIN32_OS_PATH_SEPARATOR, globalCheckerTextureFilename);
	DWORD texCheckerFileAttrs = GetFileAttributes((LPCTSTR)globalCheckerTexturePath);
	if (texCheckerFileAttrs & FILE_ATTRIBUTE_DIRECTORY || texCheckerFileAttrs == INVALID_FILE_ATTRIBUTES)
	{
		return DX11_TUTORIAL_TEXTURE_RESOURCE_NOT_FOUND_ON_DISK;
	}

	int imgWidth;
	int imgHeight;
	int imgChannels;
	uint8_t *imgData = (uint8_t *)stbi_load(globalCheckerTexturePath,
											&imgWidth,
											&imgHeight,
											&imgChannels,
											STBI_rgb_alpha);
	if (!imgData) {
		return DX11_TUTORIAL_FAILED_TO_LOAD_TEXTURE_DATA_FROM_FILE;
	}

	D3D11_TEXTURE2D_DESC texCheckerDesc;
	ZeroMemory(&texCheckerDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texCheckerDesc.Width = (UINT)imgWidth;
	texCheckerDesc.Height = (UINT)imgHeight;
	texCheckerDesc.MipLevels = 1; // TODO: (sonictk) setting to 0 for auto mipmap generation fails; maybe supposed to use context->GenerateMips instead? When generating mips here can't use initdata...
	texCheckerDesc.ArraySize = 1;
	texCheckerDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texCheckerDesc.SampleDesc.Count = 1;
	texCheckerDesc.SampleDesc.Quality = 0;
	texCheckerDesc.Usage = D3D11_USAGE_DEFAULT;
	texCheckerDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE|D3D11_BIND_RENDER_TARGET;
	texCheckerDesc.CPUAccessFlags = 0;
	texCheckerDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA texCheckerSubresourceData;
	ZeroMemory(&texCheckerSubresourceData, sizeof(D3D11_SUBRESOURCE_DATA));
	texCheckerSubresourceData.pSysMem = imgData;
	texCheckerSubresourceData.SysMemPitch = imgWidth * STBI_rgb_alpha;
	texCheckerSubresourceData.SysMemSlicePitch = 0;

	ID3D11Texture2D *d3dTexChecker = NULL;
	HRESULT hr = globalD3DDevice->CreateTexture2D(&texCheckerDesc,
												  &texCheckerSubresourceData,
												  &d3dTexChecker);
	if (hr != S_OK) {
		return DX11_TUTORIAL_FAILED_TO_CREATE_TEXTURE_SUBRESOURCE;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC texCheckerResourceViewDesc;
	ZeroMemory(&texCheckerResourceViewDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	texCheckerResourceViewDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texCheckerResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	texCheckerResourceViewDesc.Texture2D.MostDetailedMip = 0;
	texCheckerResourceViewDesc.Texture2D.MipLevels = 1;

	hr = globalD3DDevice->CreateShaderResourceView(d3dTexChecker,
												   &texCheckerResourceViewDesc,
												   &globalTxCheckerShaderResourceView);
	if (hr != S_OK) {
		return DX11_TUTORIAL_FAILED_TO_CREATE_SHADER_RESOURCE_VIEW;
	}

	globalD3DDeviceContext->PSSetShaderResources(0, 1, &globalTxCheckerShaderResourceView);

	// NOTE: (sonictk) Now that we have the texture resource view, need to create
	// the texture sampler in order to sample it.
	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 0.0f;
	samplerDesc.BorderColor[3] = 1.0f;
	samplerDesc.MinLOD = 0.0f;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	hr = globalD3DDevice->CreateSamplerState(&samplerDesc,  &globalD3DSamplerState);
	if (hr != S_OK) {
		return DX11_TUTORIAL_FAILED_TO_CREATE_SAMPLER;
	}

	globalD3DDeviceContext->PSSetSamplers(0, 1, &globalD3DSamplerState);

	stbi_image_free(imgData);

	return DX11_TUTORIAL_SUCCESS;
}


/**
 * Temporary code here for testing drawing in Direct3D. This loads the test shaders,
 * creates the vertex input layouts, creates the vertex buffers and updates the
 * constant buffers for the shaders to use for drawing.
 */
void TestD3DInitialize()
{
	static char appDir[MAX_PATH];

	getAppPath(appDir, MAX_PATH);

	// NOTE: (sonictk) Fudgery here to convert to wide char for Windows path functions.
	wchar_t appDirW[MAX_PATH];
	mbstowcs(appDirW, appDir, MAX_PATH);

	PathCchRemoveFileSpec((PWSTR)appDirW, MAX_PATH);

	memset(appDir, 0, sizeof(appDir));
	wcstombs(appDir, appDirW, MAX_PATH);

	DX11TutorialStatus lstatus = LoadD3DTestContent();
	if (lstatus != DX11_TUTORIAL_SUCCESS) {
		MessageBox(globalMainWindowHandle, "Unable to load test content! Does the resource(s) exist on disk?", 0, 0);
	}

	lstatus = CreateD3DTestMaterials();
	if (lstatus != DX11_TUTORIAL_SUCCESS) {
		MessageBox(globalMainWindowHandle, "Unable to create test materials!", "Error", MB_ICONERROR);
	}

	CreateTestLights();

#ifdef USE_PRECOMPILED_SHADERS
#pragma message("Using pre-compiled shader objects...")

	// NOTE: (sonictk) Method 1 of loading the shaders: load the precompiled objects.
	static const char globalVertexShaderFilename[] = "SimpleVertexShader.fxc";
	static const char globalPixelShaderFilename[] = "SimplePixelShader.fxc";

	static const char globalSingleInstanceVertexShaderFilename[] = "SingleInstanceVertexShader.fxc";
	static const char globalMultipleInstanceVertexShaderFilename[] = "MultipleInstanceVertexShader.fxc";
	static const char globalTexturedLitPixelShaderFilename[] = "TexturedLitPixelShader.fxc";

	static char globalVertexShaderPath[MAX_PATH];
	static char globalPixelShaderPath[MAX_PATH];

	static char globalSingleInstanceVertexShaderPath[MAX_PATH];
	static char globalMultipleInstanceVertexShaderPath[MAX_PATH];
	static char globalTexturedLitPixelShaderPath[MAX_PATH];

	sprintf(globalVertexShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalVertexShaderFilename);
	sprintf(globalPixelShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalPixelShaderFilename);

	sprintf(globalSingleInstanceVertexShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalSingleInstanceVertexShaderFilename);
	sprintf(globalMultipleInstanceVertexShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalMultipleInstanceVertexShaderFilename);
	sprintf(globalTexturedLitPixelShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalTexturedLitPixelShaderFilename);

	wchar_t globalVertexShaderPathW[MAX_PATH];
	wchar_t globalPixelShaderPathW[MAX_PATH];

	wchar_t globalSingleInstanceVertexShaderPathW[MAX_PATH];
	wchar_t globalMultipleInstanceVertexShaderPathW[MAX_PATH];
	wchar_t globalTexturedLitPixelShaderPathW[MAX_PATH];

	mbstowcs(globalVertexShaderPathW, globalVertexShaderPath, MAX_PATH);
	mbstowcs(globalPixelShaderPathW, globalPixelShaderPath, MAX_PATH);

	mbstowcs(globalSingleInstanceVertexShaderPathW, globalSingleInstanceVertexShaderPath, MAX_PATH);
	mbstowcs(globalMultipleInstanceVertexShaderPathW, globalMultipleInstanceVertexShaderPath, MAX_PATH);
	mbstowcs(globalTexturedLitPixelShaderPathW, globalTexturedLitPixelShaderPath, MAX_PATH);

	HRESULT status = D3DReadFileToBlob((LPCWSTR)globalVertexShaderPathW, &globalVertexShaderBlob);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to read the vertex shader! Does the resource exist on disk?", 0, 0);

		return;
	}

	status = D3DReadFileToBlob((LPCWSTR)globalPixelShaderPathW, &globalPixelShaderBlob);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to read the pixel shader! Does the resource exist on disk?", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalVertexShaderBlob->GetBufferPointer(),
												 globalVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(globalPixelShaderBlob->GetBufferPointer(),
												globalPixelShaderBlob->GetBufferSize(),
												NULL,
												&globalPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the pixel shader!", 0, 0);

		return;
	}

	ReleaseCOM(globalPixelShaderBlob);

	// NOTE: (sonictk) Read other more complex pre-compiled shaders to blobs
	status = D3DReadFileToBlob((LPCWSTR)globalSingleInstanceVertexShaderPathW, &globalSingleInstanceVertexShaderBlob);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to read the single instance vertex shader! Does the resource exist on disk?", 0, 0);

		return;
	}

	status = D3DReadFileToBlob((LPCWSTR)globalMultipleInstanceVertexShaderPathW, &globalMultipleInstanceVertexShaderBlob);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to read the multiple instance vertex shader! Does the resource exist on disk?", 0, 0);

		return;
	}

	status = D3DReadFileToBlob((LPCWSTR)globalTexturedLitPixelShaderPathW, &globalTexturedLitPixelShaderBlob);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to read the textured lit pixel shader! Does the resource exist on disk?", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalSingleInstanceVertexShaderBlob->GetBufferPointer(),
												 globalSingleInstanceVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalSingleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the single instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalMultipleInstanceVertexShaderBlob->GetBufferPointer(),
												 globalMultipleInstanceVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalMultipleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the multiple instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(globalTexturedLitPixelShaderBlob->GetBufferPointer(),
												globalTexturedLitPixelShaderBlob->GetBufferSize(),
												NULL,
												&globalTexturedLitPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the textured-lit pixel shader!", 0, 0);

		return;
	}

#elif USE_PRECOMPILED_SHADER_HEADERS
#pragma message("Using pre-compiled shader headers...")

	// NOTE: (sonictk) Method 2: Load the precompiled header bytecode. This requires
	// that the headers have been built *first*.

#include "../msbuild/inc/SimpleVertexShader.h"
#include "../msbuild/inc/SimplePixelShader.h"

#include "../msbuild/inc/SingleInstanceVertexShader.h"
#include "../msbuild/inc/MultipleInstanceVertexShader.h"
#include "../msbuild/inc/TexturedLitPixelShader.h"

	// NOTE: (sonictk) These entry points were already defined in the ``build.bat``.
	status = globalD3DDevice->CreateVertexShader(g_SimpleVertexShader_main,
												 sizeof(g_SimpleVertexShader_main),
												 NULL,
												 &globalVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(g_SimplePixelShader_main,
												sizeof(g_SimplePixelShader_main),
												NULL,
												&globalPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the pixel shader!", 0, 0);

		return;
	}

	// NOTE: (sonictk) More complex shaders here
	status = globalD3DDevice->CreateVertexShader(g_SingleInstanceVertexShader_main,
												 sizeof(g_SingleInstanceVertexShader_main),
												 NULL,
												 &globalSingleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the single instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(g_MultipleInstanceVertexShader_main,
												 sizeof(g_MultipleInstanceVertexShader_main),
												 NULL,
												 &globalMultipleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the multiple instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(g_TexturedLitPixelShader_main,
												sizeof(g_TexturedLitPixelShader_main),
												NULL,
												&globalTexturedLitPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the textured lit pixel shader!", 0, 0);

		return;
	}

#else
	// NOTE: (sonictk) Load the shaders dynamically at run-time and compile them.
	// This is the slowest of the options available and is also not allowed for
	// UWP (Windows Store) applications.
	static const char globalVertexShaderFilename[] = "SimpleVertexShader.hlsl";
	static const char globalPixelShaderFilename[] = "SimplePixelShader.hlsl";

	static const char globalSingleInstanceVertexShaderFilename[] = "SingleInstanceVertexShader.hlsl";
	static const char globalMultipleInstanceVertexShaderFilename[] = "MultipleInstanceVertexShader.hlsl";
	static const char globalTexturedLitPixelShaderFilename[] = "TexturedLitPixelShader.hlsl";

	static char globalVertexShaderPath[MAX_PATH];
	static char globalPixelShaderPath[MAX_PATH];

	static char globalSingleInstanceVertexShaderPath[MAX_PATH];
	static char globalMultipleInstanceVertexShaderPath[MAX_PATH];
	static char globalTexturedLitPixelShaderPath[MAX_PATH];

	sprintf(globalVertexShaderPath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, "shaders", WIN32_OS_PATH_SEPARATOR, globalVertexShaderFilename);
	sprintf(globalPixelShaderPath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, "shaders", WIN32_OS_PATH_SEPARATOR, globalPixelShaderFilename);

	sprintf(globalSingleInstanceVertexShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalSingleInstanceVertexShaderFilename);
	sprintf(globalMultipleInstanceVertexShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalMultipleInstanceVertexShaderFilename);
	sprintf(globalTexturedLitPixelShaderPath, "%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, globalTexturedLitPixelShaderFilename);

	wchar_t globalVertexShaderPathW[MAX_PATH];
	wchar_t globalPixelShaderPathW[MAX_PATH];

	wchar_t globalSingleInstanceVertexShaderPathW[MAX_PATH];
	wchar_t globalMultipleInstanceVertexShaderPathW[MAX_PATH];
	wchar_t globalTexturedLitPixelShaderPathW[MAX_PATH];

	mbstowcs(globalVertexShaderPathW, globalVertexShaderPath, MAX_PATH);
	mbstowcs(globalPixelShaderPathW, globalPixelShaderPath, MAX_PATH);

	mbstowcs(globalSingleInstanceVertexShaderPathW, globalSingleInstanceVertexShaderPath, MAX_PATH);
	mbstowcs(globalMultipleInstanceVertexShaderPathW, globalMultipleInstanceVertexShaderPath, MAX_PATH);
	mbstowcs(globalTexturedLitPixelShaderPathW, globalTexturedLitPixelShaderPath, MAX_PATH);

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if _DEBUG
	flags |= D3DCOMPILE_DEBUG;
#endif // _DEBUG

	status = D3DCompileFromFile(globalVertexShaderPathW,
								NULL,
								D3D_COMPILE_STANDARD_FILE_INCLUDE,
								"main", // NOTE: (sonictk) We're just using ``main`` as the entry point for the shader by convention.
								"vs_5_0",
								flags,
								0,
								&globalVertexShaderBlob,
								&errorBlob);
	if (status != S_OK) {
		const char *errMsg = (const char *)errorBlob->GetBufferPointer();
		OutputDebugString(errMsg);
		MessageBox(globalMainWindowHandle, "Unable to compile the vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalVertexShaderBlob->GetBufferPointer(),
												 globalVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the vertex shader!", 0, 0);

		return;
	}

	status = D3DCompileFromFile(globalPixelShaderPathW,
								NULL,
								D3D_COMPILE_STANDARD_FILE_INCLUDE,
								"main",
								"ps_5_0",
								flags,
								0,
								&globalPixelShaderBlob,
								&errorBlob);
	if (status != S_OK) {
		const char *errMsg = (const char *)errorBlob->GetBufferPointer();
		OutputDebugString(errMsg);
		MessageBox(globalMainWindowHandle, "Unable to compile the pixel shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(globalPixelShaderBlob->GetBufferPointer(),
												globalPixelShaderBlob->GetBufferSize(),
												NULL,
												&globalPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the textured-lit pixel shader!", 0, 0);

		return;
	}

	// NOTE: (sonictk) More complex shaders here
	status = D3DCompileFromFile(globalSingleInstanceVertexShaderPathW,
								NULL,
								D3D_COMPILE_STANDARD_FILE_INCLUDE,
								"main",
								"vs_5_0",
								flags,
								0,
								&globalSingleInstanceVertexShaderBlob,
								&errorBlob);
	if (status != S_OK) {
		const char *errMsg = (const char *)errorBlob->GetBufferPointer();
		OutputDebugString(errMsg);
		MessageBox(globalMainWindowHandle, "Unable to compile the single instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalSingleInstanceVertexShaderBlob->GetBufferPointer(),
												 globalSingleInstanceVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalSingleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the single instance vertex shader!", 0, 0);

		return;
	}

	status = D3DCompileFromFile(globalMultipleInstanceVertexShaderPathW,
								NULL,
								D3D_COMPILE_STANDARD_FILE_INCLUDE,
								"main",
								"vs_5_0",
								flags,
								0,
								&globalMultipleInstanceVertexShaderBlob,
								&errorBlob);
	if (status != S_OK) {
		const char *errMsg = (const char *)errorBlob->GetBufferPointer();
		OutputDebugString(errMsg);
		MessageBox(globalMainWindowHandle, "Unable to compile the multiple instance vertex shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreateVertexShader(globalMultipleInstanceVertexShaderBlob->GetBufferPointer(),
												 globalMultipleInstanceVertexShaderBlob->GetBufferSize(),
												 NULL,
												 &globalMultipleInstanceVertexShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the multiple instance vertex shader!", 0, 0);

		return;
	}

	status = D3DCompileFromFile(globalTexturedLitPixelShaderPathW,
								NULL,
								D3D_COMPILE_STANDARD_FILE_INCLUDE,
								"main",
								"ps_5_0",
								flags,
								0,
								&globalTexturedLitPixelShaderBlob,
								&errorBlob);
	if (status != S_OK) {
		const char *errMsg = (const char *)errorBlob->GetBufferPointer();
		OutputDebugString(errMsg);
		MessageBox(globalMainWindowHandle, "Unable to compile the textured lit pixel shader!", 0, 0);

		return;
	}

	status = globalD3DDevice->CreatePixelShader(globalTexturedLitPixelShaderBlob->GetBufferPointer(),
												globalTexturedLitPixelShaderBlob->GetBufferSize(),
												NULL,
												&globalTexturedLitPixelShader);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the textured-lit pixel shader!", 0, 0);

		return;
	}

#endif // Precompiled/headers/runtime-compilation of shaders

	// NOTE: (sonictk) We need to provide D3D with a description of our vertex
	// structures so that it knows how to interpret each component.
	D3D11_INPUT_ELEMENT_DESC vertexInputLayoutDesc[] = {
		// NOTE: (sonictk) These will end up being the user-defined semantics names.
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	D3D11_INPUT_ELEMENT_DESC vertexTextureInputLayoutDesc[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0}
	};

	status = globalD3DDevice->CreateInputLayout(vertexInputLayoutDesc,
												2,
												globalVertexShaderBlob->GetBufferPointer(),
												globalVertexShaderBlob->GetBufferSize(),
												&globalD3DVertexInputLayout);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the input layout!", "Error", MB_ICONERROR);
	}

	// NOTE: (sonictk) Once we've created the input layout object, the shader blob
	// is no longer required.
	ReleaseCOM(globalVertexShaderBlob);

	status = globalD3DDevice->CreateInputLayout(vertexTextureInputLayoutDesc,
												_countof(vertexTextureInputLayoutDesc),
												globalSingleInstanceVertexShaderBlob->GetBufferPointer(),
												globalSingleInstanceVertexShaderBlob->GetBufferSize(),
												&globalD3DVertexTextureInputLayout);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the input layout!", "Error", MB_ICONERROR);
	}

	ReleaseCOM(globalSingleInstanceVertexShaderBlob);

	// NOTE: (sonictk) Describe the vertex buffer for the test cube object.
	D3D11_BUFFER_DESC vtxBufferDesc;
	ZeroMemory(&vtxBufferDesc, sizeof(D3D11_BUFFER_DESC));
	vtxBufferDesc.ByteWidth = sizeof(Vertex) * _countof(testCubeVertices); // NOTE: (sonictk) Num of vertices
	vtxBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vtxBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vtxBufferDesc.CPUAccessFlags = 0; // NOTE: (sonictk) No CPU access necessary
	vtxBufferDesc.MiscFlags = 0;
	vtxBufferDesc.StructureByteStride = 0; // NOTE: (sonictk) This isn't a structured buffer.

	// NOTE: (sonictk) Specifies the data we want to initialize the vertex buffer contents with.
	D3D11_SUBRESOURCE_DATA vtxInitData;
	ZeroMemory(&vtxInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	vtxInitData.pSysMem = testCubeVertices;

	status = globalD3DDevice->CreateBuffer(&vtxBufferDesc, &vtxInitData, &globalD3DVertexBuffer);
	switch (status) {
	case E_OUTOFMEMORY:
		MessageBox(globalMainWindowHandle, "Insufficient memory to create the vertex buffer!", 0, 0);

		return;
	case S_OK:
		break;
	default:
		MessageBox(globalMainWindowHandle, "Error occurred while attempting to create a vertex buffer!", 0, MB_ICONERROR);
		break;
	}

	// NOTE: (sonictk) Describe the index buffer.
	D3D11_BUFFER_DESC idxBufferDesc;
	ZeroMemory(&idxBufferDesc, sizeof(D3D11_BUFFER_DESC));
	idxBufferDesc.ByteWidth = sizeof(UINT) * _countof(testVtxIndices);
	idxBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	idxBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	// NOTE: (sonictk) Specify default data for the index buffer.
	D3D11_SUBRESOURCE_DATA idxInitData;
	ZeroMemory(&idxInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	idxInitData.pSysMem = testVtxIndices;

	status = globalD3DDevice->CreateBuffer(&idxBufferDesc, &idxInitData, &globalD3DIndexBuffer);
	switch (status) {
	case E_OUTOFMEMORY:
		MessageBox(globalMainWindowHandle, "Insufficient memory to create the index buffer!", 0, 0);
	case S_OK:
	default:
		break;
	}

	// NOTE: (sonictk) Also create the necessary buffers for the test cylinder object.
	createCylinderMesh(1.0f, 1.0f, 3.0f, TEST_CYLINDER_NUM_SEGMENTS, TEST_CYLINDER_NUM_STACKS, globalTestCylinderVertices);

	D3D11_BUFFER_DESC cylinderVtxBufferDesc;
	ZeroMemory(&cylinderVtxBufferDesc, sizeof(D3D11_BUFFER_DESC));
	cylinderVtxBufferDesc.ByteWidth = sizeof(UINT) * _countof(globalTestCylinderVertices);
	cylinderVtxBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	cylinderVtxBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA cylinderVtxInitData;
	ZeroMemory(&cylinderVtxInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	cylinderVtxInitData.pSysMem = globalTestCylinderVertices;

	status = globalD3DDevice->CreateBuffer(&cylinderVtxBufferDesc, &cylinderVtxInitData, &globalCylinderVertexBuffer);
	switch (status) {
	case E_OUTOFMEMORY:
		MessageBox(globalMainWindowHandle, "Insufficient memory to create the vertex buffer!", 0, 0);

		return;
	case S_OK:
		break;
	default:
		MessageBox(globalMainWindowHandle, "Error occurred while attempting to create a vertex buffer!", 0, MB_ICONERROR);
		break;
	}

	D3D11_BUFFER_DESC cylinderIdxBufferDesc;
	ZeroMemory(&cylinderIdxBufferDesc, sizeof(D3D11_BUFFER_DESC));
	cylinderIdxBufferDesc.ByteWidth = sizeof(UINT) * _countof(globalTestCylinderVertIndices);
	cylinderIdxBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	cylinderIdxBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA cylinderIdxInitData;
	ZeroMemory(&cylinderIdxInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	cylinderIdxInitData.pSysMem = globalTestCylinderVertIndices;

	status = globalD3DDevice->CreateBuffer(&cylinderIdxBufferDesc, &cylinderIdxInitData, &globalCylinderVertIndicesBuffer);
	switch (status) {
	case E_OUTOFMEMORY:
		MessageBox(globalMainWindowHandle, "Insufficient memory to create the index buffer!", 0, 0);

		return;
	case S_OK:
		break;
	default:
		MessageBox(globalMainWindowHandle, "Error occurred while attempting to create an index buffer!", 0, MB_ICONERROR);
		break;
	}

	// NOTE: (sonictk) Create the constant buffers for the variables defined in
	// the vertex shader.
	D3D11_BUFFER_DESC constantBufferDesc;
	ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));
	constantBufferDesc.ByteWidth = sizeof(dx::XMMATRIX);
	// NOTE: (sonictk) This isn't set to ``D3D11_USAGE_DYNAMIC`` because we will
	// use ID3D11DeviceContext::UpdateSubresource to handle the update and that
	// expects constant buffers to be initialized with ``D3D11_USAGE_DEFAULT``.``
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	status = globalD3DDevice->CreateBuffer(&constantBufferDesc,
										   NULL,
										   &globalD3DConstantBuffers[D3DConstantBuffer_PerApp]);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to update the constant buffer for the application!", 0, 0);
	}

	status = globalD3DDevice->CreateBuffer(&constantBufferDesc,
										   NULL,
										   &globalD3DConstantBuffers[D3DConstantBuffer_PerFrame]);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to update the constant buffer for the frame!", 0, 0);
	}

	status = globalD3DDevice->CreateBuffer(&constantBufferDesc,
										   NULL,
										   &globalD3DConstantBuffers[D3DConstantBuffer_PerObject]);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to update the constant buffer for the object!", 0, 0);
	}

	// NOTE: (sonictk) Create the constant buffer for light properties.
	D3D11_BUFFER_DESC lightPropertiesConstantBufferDesc;
	ZeroMemory(&lightPropertiesConstantBufferDesc, sizeof(D3D11_BUFFER_DESC));
	lightPropertiesConstantBufferDesc.ByteWidth = sizeof(LightProperties);
	lightPropertiesConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	lightPropertiesConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightPropertiesConstantBufferDesc.CPUAccessFlags = 0;
	lightPropertiesConstantBufferDesc.MiscFlags = 0;
	lightPropertiesConstantBufferDesc.StructureByteStride = 0;

	status = globalD3DDevice->CreateBuffer(&lightPropertiesConstantBufferDesc,
										   NULL,
										   &globalD3DLightPropertiesConstantBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the constant buffer for the light properties!", "Error", MB_ICONERROR);
	}

	// NOTE: (sonictk) Create the constant buffer for materials' properties.
	D3D11_BUFFER_DESC phongMaterialsConstantBufferDesc;
	ZeroMemory(&phongMaterialsConstantBufferDesc, sizeof(D3D11_BUFFER_DESC));
	phongMaterialsConstantBufferDesc.ByteWidth = sizeof(PhongMaterialProperties);
	phongMaterialsConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	phongMaterialsConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	status = globalD3DDevice->CreateBuffer(&phongMaterialsConstantBufferDesc,
										   NULL,
										   &globalD3DMaterialPropertiesConstantBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the constant buffer for the material properties!", "Error", MB_ICONERROR);
	}

	// NOTE: (sonictk) Create a bunch of planes to render.

	// NOTE: (sonictk) Create the constant buffer for the combined view-projection matrices.
	D3D11_BUFFER_DESC instancesConstantBufferDesc;
	ZeroMemory(&instancesConstantBufferDesc, sizeof(D3D11_BUFFER_DESC));
	instancesConstantBufferDesc.ByteWidth = sizeof(dx::XMMATRIX);
	instancesConstantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	instancesConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	status = globalD3DDevice->CreateBuffer(&instancesConstantBufferDesc,
										   NULL,
										   &globalD3DInstancesConstantBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the constant buffer for the instances!", "Error", MB_ICONERROR);
	}

	// NOTE: (sonictk) Specify the data we want to initialize the 3D planes with.
	D3D11_SUBRESOURCE_DATA planesVerticesResourceData;
	ZeroMemory(&planesVerticesResourceData, sizeof(D3D11_SUBRESOURCE_DATA));
	planesVerticesResourceData.pSysMem = testPlaneVertices;

	D3D11_BUFFER_DESC planesVertexBufferDesc;
	ZeroMemory(&planesVertexBufferDesc, sizeof(D3D11_BUFFER_DESC));
	planesVertexBufferDesc.ByteWidth = sizeof(testPlaneVertices);
	planesVertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	planesVertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	status = globalD3DDevice->CreateBuffer(&planesVertexBufferDesc,
										   &planesVerticesResourceData,
										   &globalPlanesVertexBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the vertex buffer for the 3D planes!", "Error!", MB_ICONERROR);

		return;
	}

	D3D11_BUFFER_DESC planesIndicesBufferDesc;
	ZeroMemory(&planesIndicesBufferDesc, sizeof(D3D11_BUFFER_DESC));
	planesIndicesBufferDesc.ByteWidth = sizeof(testPlaneIndices);
	planesIndicesBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	planesIndicesBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA planesIndicesInitData;
	ZeroMemory(&planesIndicesInitData, sizeof(D3D11_SUBRESOURCE_DATA));
	planesIndicesInitData.pSysMem = testPlaneIndices;

	status = globalD3DDevice->CreateBuffer(&planesIndicesBufferDesc, &planesIndicesInitData, &globalPlanesIndexBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the index buffer for the 3D planes!", "Error!", MB_ICONERROR);

		return;
	}

	// TODO: (sonictk) If the Plane3DInstanceData is already aligned to 16 byte boundaries, does malloc need to be aligned as well?
	Plane3DInstanceData *planeInstanceData = (Plane3DInstanceData *)_aligned_malloc(sizeof(Plane3DInstanceData) * globalNumOfPlanes, 16);

	float scalePlane = 20.0f;
	float translateOffset = scalePlane / 2.0f;
	dx::XMMATRIX planeScaleM44 = dx::XMMatrixScaling(scalePlane, 1.0f, scalePlane);
	dx::XMMATRIX planeTranslationM44 = dx::XMMatrixTranslation(0, 0, 0);
	dx::XMMATRIX planeRotationM44 = dx::XMMatrixRotationX(0.0f);

	// NOTE: (sonictk) Because the plane world matrix is scaled non-uniformly in
	// order to appear bigger in the scene, we must compute the inverse
	// transpose of the world matrix and use it to transform the normals of the
	// plane in order to avoid any skew on them.

	// NOTE: (sonictk) Floor plane.
	dx::XMMATRIX planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[0].worldMatrix = planeWorldM44;
	planeInstanceData[0].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Back plane.
	planeTranslationM44 = dx::XMMatrixTranslation(0, translateOffset, translateOffset);
	planeRotationM44 = dx::XMMatrixRotationX(dx::XMConvertToRadians(-90.0f));
	planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[1].worldMatrix = planeWorldM44;
	planeInstanceData[1].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Top plane.
	planeTranslationM44 = dx::XMMatrixTranslation(0, translateOffset * 2.0f, 0);
	planeRotationM44 = dx::XMMatrixRotationX(dx::XMConvertToRadians(180.0f));
	planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[2].worldMatrix = planeWorldM44;
	planeInstanceData[2].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Front plane.
	planeTranslationM44 = dx::XMMatrixTranslation(0, translateOffset, -translateOffset);
	planeRotationM44 = dx::XMMatrixRotationX(dx::XMConvertToRadians(90.0f));
	planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[3].worldMatrix = planeWorldM44;
	planeInstanceData[3].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Left plane.
	planeTranslationM44 = dx::XMMatrixTranslation(-translateOffset, translateOffset, 0);
	planeRotationM44 = dx::XMMatrixRotationX(dx::XMConvertToRadians(-90.0f));
	planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[4].worldMatrix = planeWorldM44;
	planeInstanceData[4].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Right plane.
	planeTranslationM44 = dx::XMMatrixTranslation(translateOffset, translateOffset, 0);
	planeRotationM44 = dx::XMMatrixRotationX(dx::XMConvertToRadians(90.0f));
	planeWorldM44 = planeScaleM44 * planeRotationM44 * planeTranslationM44;

	planeInstanceData[5].worldMatrix = planeWorldM44;
	planeInstanceData[5].inverseTransposeWorldMatrix = dx::XMMatrixTranspose(dx::XMMatrixInverse(NULL, planeWorldM44));

	// NOTE: (sonictk) Create the vertex buffer for binding the per-instance attrs
	// of the six walls to the IA stage.
	D3D11_SUBRESOURCE_DATA planesInstanceResourceData;
	ZeroMemory(&planesInstanceResourceData, sizeof(D3D11_SUBRESOURCE_DATA));
	planesInstanceResourceData.pSysMem = planeInstanceData;

	D3D11_BUFFER_DESC planesInstanceBufferDesc;
	ZeroMemory(&planesInstanceBufferDesc, sizeof(D3D11_BUFFER_DESC));
	planesInstanceBufferDesc.ByteWidth = sizeof(Plane3DInstanceData) * globalNumOfPlanes;
	planesInstanceBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	planesInstanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	planesInstanceBufferDesc.CPUAccessFlags = 0;
	planesInstanceBufferDesc.MiscFlags = 0;
	planesInstanceBufferDesc.StructureByteStride = 0;

	status = globalD3DDevice->CreateBuffer(&planesInstanceBufferDesc,
										   &planesInstanceResourceData,
										   &globalPlanesInstanceBuffer);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the instance buffer for the 3D planes!", "Error!", MB_ICONERROR);

		return;
	}

	// NOTE: (sonictk) Define the input layout for rendering instanced vertex data.
	D3D11_INPUT_ELEMENT_DESC vtxInstancesLayoutDesc[] = {

		// NOTE: (sonictk) Input slot 0 here for per-vertex data; indicates that
		// the data will come from the 1st vertex buffer bound to the IA stage.
		// NOTE: (sonictk) These are semantics for per-vertex data.
		{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0},

		// NOTE: (sonictk) Input slot 1 here for per-instance data; indicates that
		// the data will come from the 2nd vertex buffer bound to the IA stage.
		// NOTE: (sonictk) These are semantics for per-instance data. Cause it's
		// a 4x4 float matrix, so we define it 4 times.
		{"WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},

		{"INVERSETRANSPOSEWORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"INVERSETRANSPOSEWORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"INVERSETRANSPOSEWORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1},
		{"INVERSETRANSPOSEWORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1}
	};

	status = globalD3DDevice->CreateInputLayout(vtxInstancesLayoutDesc,
												_countof(vtxInstancesLayoutDesc),
												globalMultipleInstanceVertexShaderBlob->GetBufferPointer(),
												globalMultipleInstanceVertexShaderBlob->GetBufferSize(),
												&globalInstancedInputLayout);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Unable to create the instanced input layout!", "Error", MB_ICONERROR);
	}

	ReleaseCOM(globalMultipleInstanceVertexShaderBlob);
}


/**
 * Initializes the DirectX device and swap chain.
 *
 * @param hInstance 		The handle to the application instance.
 * @param enableVSync		Determines if vertical sync should be enabled.
 *
 * @return					``DX11_TUTORIAL_SUCCESS`` on success, or a non-zero
 * 						value if an error occurred.
 */
int InitializeD3D(HINSTANCE hInstance, BOOL enableVSync)
{
	assert(globalMainWindowHandle != 0);

	UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT; // NOTE: (sonictk) This is for Direct2D interoperability

#if _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	// NOTE: (sonictk) Accepted Direct3D feature levels for support.
	globalVar const D3D_FEATURE_LEVEL d3dAcceptedFeatureLevels[] = {
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1
	};

	// NOTE: (sonictk) We create the device and swap chain separately (instead
	// of using D3D11CreateDeviceAndSwapChain) so that we can query the device
	// for MSAA quality level support. Even though all D3D11-capable devices
	// must be able to support 4xMSAA with all render target formats, the
	// supported quality levels themselves could be different.
	HRESULT status = D3D11CreateDevice(NULL, // NOTE: (sonictk) Use the default adapter
									   D3D_DRIVER_TYPE_HARDWARE, // NOTE: (sonictk) Use the GPU (Hardware Abstraction Layer) to actually implement D3D features, not CPU
									   NULL, // NOTE: (sonictk) We're using the GPU, so no software rasterizer
									   createDeviceFlags,
									   d3dAcceptedFeatureLevels,
									   _countof(d3dAcceptedFeatureLevels),
									   D3D11_SDK_VERSION,
									   &globalD3DDevice,
									   &globalD3DFeatureLevel,
									   &globalD3DDeviceContext);
	if (status != S_OK) {
		switch (status) {
		case E_FAIL:
			MessageBox(globalMainWindowHandle, "Failed to create Direct3D device; debug layer not installed!", 0, 0);
			break;
		case E_OUTOFMEMORY:
			MessageBox(globalMainWindowHandle, "Failed to create Direct3D device; out of memory!", 0, 0);
			break;
		default:
			MessageBox(globalMainWindowHandle, "Failed to create Direct3D device; unknown error!", 0, 0);
			break;
		}

		return DX11_TUTORIAL_D3D_DEVICE_CREATION_FAILED;
	}

	status = globalD3DDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, globalD3DMSAASampleCount, &globalD3DMSAASampleQuality);
	if (status != S_OK) {
		MessageBox(globalMainWindowHandle, "Failed to get D3D MSAA quality levels! The graphics hardware is not supported!", 0, 0);

		return DX11_TUTORIAL_D3D_DEVICE_UNSUPPORTED_MSAA;
	}

	// NOTE: (sonictk) 4X MSAA should always be supported for D3D11-capable devices.
	assert(globalD3DMSAASampleQuality > 0);

	RECT clientRect;
	if (GetClientRect(globalMainWindowHandle, &clientRect) == 0) {
		DWORD winError = GetLastError();
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; could not get window client area!", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	LONG clientWidth = clientRect.right - clientRect.left;
	LONG clientHeight = clientRect.bottom - clientRect.top;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC)); // NOTE: (sonictk) Can use ``{0}`` value initialization instead, but this is MSDN style.

	swapChainDesc.BufferDesc.Width = clientWidth;
	swapChainDesc.BufferDesc.Height = clientHeight;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = (UINT)globalTargetFPS;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // NOTE: (sonictk) RGBA 8 bpp 32 bit total unsigned normalized int
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED; // NOTE: (sonictk) No preferred scanline order.
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
	swapChainDesc.SampleDesc.Count = globalD3DMSAASampleCount;
	swapChainDesc.SampleDesc.Quality = globalD3DMSAASampleCount == 1 ? 0 : globalD3DMSAASampleQuality - 1; // NOTE: (sonictk) Must set to 0 if MSAA is disabled.
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // NOTE: (sonictk) Uses the surface/resource as an output render target.
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = globalMainWindowHandle;
	swapChainDesc.Windowed = TRUE; // NOTE: (sonictk) We will maximize fullscreen later
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD; // NOTE: (sonictk) Only valid flag when MSAA is enabled.
	swapChainDesc.Flags = 0;

	// NOTE: (sonictk) As guidance from MSDN dictates: https://docs.microsoft.com/en-us/windows/desktop/api/dxgi1_2/nn-dxgi1_2-idxgifactory2
	// We do this to retrieve the factory that is used to create the device in
	// order to create a swap chain.
	status = globalD3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&globalDXGIDevice);

	assert(status != E_NOINTERFACE);

	status = globalDXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void **)&globalDXGIAdapter);

	assert(status != E_NOINTERFACE);

	status = globalDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void **)&globalDXGIFactory);

	assert(status != E_NOINTERFACE);

	// NOTE: (sonictk) We go against MSDN guidance here and use CreateSwapChain()
	// instead of CreateSwapChainForHwnd because the ``DXGI_SWAP_CHAIN_DESC1`` does
	// not allow for overriding refresh rate. This is not a UWP app where we're locked
	// to 60 FPS!
	status = globalDXGIFactory->CreateSwapChain(globalD3DDevice,
												&swapChainDesc,
												&globalD3DSwapChain);
	if (status != S_OK) {
		switch(status) {
		case E_OUTOFMEMORY:
			MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; out of memory to create swap chain!", 0, 0);

			return DX11_TUTORIAL_D3D_OUT_OF_MEMORY;
		case DXGI_ERROR_INVALID_CALL:
			MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; invalid data was specified!", 0, 0);
		default:
			MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D; an error occurred while creating the swap chain!", 0, 0);
			return DX11_TUTORIAL_D3D_SWAP_CHAIN_CREATION_FAILED;

			break;
		}
	}

	// NOTE: (sonictk) Test shaders and geo.
	TestD3DInitialize();

	OnResize();

	return DX11_TUTORIAL_SUCCESS;
}


/**
 * Cleans up all Direct3D and associated COM resources. This should be called
 * when there is no more need to render the application.
 */
void CleanupD3D()
{
	ReleaseCOM(globalD3DVertexBuffer);

	ReleaseCOM(globalD3DIndexBuffer);

	ReleaseCOM(globalD3DRenderTargetView);

	ReleaseCOM(globalD3DDepthStencilView);
	ReleaseCOM(globalD3DDepthStencilBuffer);
	ReleaseCOM(globalD3DDepthStencilState);

	ReleaseCOM(globalD3DRasterizerState);

	ReleaseCOM(globalDXGIFactory);
	ReleaseCOM(globalDXGIDevice);
	ReleaseCOM(globalDXGIAdapter);

	ReleaseCOM(globalD3DSwapChain);
	ReleaseCOM(globalD3DDeviceContext);
	ReleaseCOM(globalD3DDevice);
}


void CleanupXAudio2()
{
	HeapFree(globalProcessHeap, 0, globalAudioDataBuffer);
	HeapFree(globalProcessHeap, 0, globalX3DChannelsMatrix);
}


/**
 * This callback is executed when a mouse button is clicked.
 *
 * @param btnState
 * @param x
 * @param y
 */
void OnMouseDown(const WPARAM btnState, const int x, const int y)
{
// TODO: (sonictk) Implement
}


/**
 * This callback is executed when a mouse button is released.
 *
 * @param btnState
 * @param x
 * @param y
 */
void OnMouseUp(const WPARAM btnState, const int x, const int y)
{
	// TODO: (sonictk) Implement
}


/**
 * This callback is executed when the mouse is moved when the cursor is within
 * the window, regardless of its focus.
 *
 * @param btnState		Indicates whether various virtual keys are down.
 * @param x 			The x-coordinate of the mouse position, in client area coordinates.
 * @param y			The y-coordinate of the mouse position, in client area coordinates.
 */
void OnMouseMove(const WPARAM btnState, const int x, const int y)
{
	// NOTE: (sonictk) Don't apply any cursor restrictions if the app is not in focus
	// even though the cursor might be within it.
	if (globalApplicationState != DX11_TUTORIAL_APPLICATION_RUNNING) {
		return;
	}

	// NOTE: (sonictk) Clip to smaller area so that we don't hit window resize handles;
	// This is not placed in ``WM_INPUT`` since that is called more frequently and
	// testing this seems to work well enough for ``WM_MOUSEMOVE`` even though it's
	// messaged less frequently.
	RECT clipArea;
	GetWindowRect(globalMainWindowHandle, &clipArea);

	LONG buffer = 50;
	clipArea.top += buffer;
	clipArea.bottom -= buffer;
	clipArea.left += buffer;
	clipArea.right -= buffer;
	BOOL status = ClipCursor(&clipArea);
	if (status == 0) {
		OutputDebugString("Unable to clip the cursor to the client area!\n");
	}

	// NOTE: (sonictk) We still re-center the mouse cursor to the center of the
	// screen so that it's more predictable for the end-user what happens when they
	// alt-tab out of the application.
	// We also only re-center if the UI modifier key is not actively being held down.
	if (globalKeysDown & globalKeyMask_UIModifier) {
		return;
	}

	LONG centerX = ((clipArea.right - clipArea.left) / 2) + clipArea.left;
	LONG centerY = ((clipArea.bottom - clipArea.top) / 2) + clipArea.top;
	status = SetCursorPos(centerX, centerY);
	if (status == 0) {
		OutputDebugString("Unable to re-center the cursor to the client area!\n");
	}

	return;
}


void OnMouseWheelScroll(const short rotation)
{
	// NOTE: (sonictk) The distance is expressed in multiples of ``120`` when Windows sends the message.
	if (abs(rotation) < WHEEL_DELTA) {
		return;
	}

	if (rotation < 0) {
		globalFieldOfViewAngle += globalCameraZoomSpeed;
	} else if (rotation > 0) {
		globalFieldOfViewAngle -= globalCameraZoomSpeed;
	} else {
		return;
	}

	return;
}


/**
 * This callback is executed when a key is pressed.
 *
 * @param btnState	Information about the buttons that were pressed.
 */
void OnKeyPress(const WPARAM btnState)
{
	switch (btnState) {
	case VK_ESCAPE:
		// NOTE: (sonictk) This sends ``WM_DESTROY`` and ``WM_NCDESTORY`` messages
		// to the window, which gets handled later on in this switch statement.
		DestroyWindow(globalMainWindowHandle);

		return;

	case VK_A:
	case VK_LEFT:
		globalKeysDown |= globalKeyMask_Lf;

		return;

	case VK_D:
	case VK_RIGHT:
		globalKeysDown |= globalKeyMask_Rt;

		return;

	case VK_W:
	case VK_UP:
		globalKeysDown |= globalKeyMask_Fw;

		return;

	case VK_S:
	case VK_DOWN:
		globalKeysDown |= globalKeyMask_Bk;

		return;

	case VK_E:
		globalKeysDown |= globalKeyMask_Up;

		return;

	case VK_Q:
		globalKeysDown |= globalKeyMask_Dn;

		return;

	case VK_Z:
		globalKeysDown |= globalKeyMask_RollLf;

		return;

	case VK_C:
		globalKeysDown |= globalKeyMask_RollRt;

		return;

	case VK_V:
		globalKeysDown |= globalKeyMask_UIModifier;

		return;

	case VK_SHIFT:
		globalModifiersDown |= globalModifiersMask_Shift;

		return;

	case VK_CONTROL:
		globalModifiersDown |= globalModifiersMask_Control;

		return;

	default:
		break;
	}

	return;
}


void OnKeyRelease(const WPARAM btnState)
{
	switch (btnState) {
	case VK_A:
	case VK_LEFT:
		globalKeysDown &= ~globalKeyMask_Lf;

		return;

	case VK_D:
	case VK_RIGHT:
		globalKeysDown &= ~globalKeyMask_Rt;

		return;

	case VK_W:
	case VK_UP:
		globalKeysDown &= ~globalKeyMask_Fw;

		return;

	case VK_S:
	case VK_DOWN:
		globalKeysDown &= ~globalKeyMask_Bk;

		return;

	case VK_E:
		globalKeysDown &= ~globalKeyMask_Up;

		return;

	case VK_Q:
		globalKeysDown &= ~globalKeyMask_Dn;

		return;

	case VK_Z:
		globalKeysDown &= ~globalKeyMask_RollLf;

		return;

	case VK_C:
		globalKeysDown &= ~globalKeyMask_RollRt;

		return;

	case VK_V:
		globalKeysDown &= ~globalKeyMask_UIModifier;

		return;

	case VK_SHIFT:
		globalModifiersDown &= ~globalModifiersMask_Shift;

		return;

	case VK_CONTROL:
		globalModifiersDown &= ~globalModifiersMask_Control;

		return;

	default:
		break;
	}

	return;
}


/**
 * Callback for processing the messages that are sent to the window. This is executed
 * each time the window receives a message from Windows.
 *
 * @param hwnd		A handle to the window.
 * @param uMsg		The message.
 * @param wParam	Additional message information.
 * @param lParam	Additional message information.
 *
 * @return			The result of the message processing.
 */
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// NOTE: (sonictk) This is for ImGui's own input handling to be enabled.
	if (ImGui_ImplWin32_WndProcHandler(hwnd, uMsg, wParam, lParam)) {
		return true;
	}

	switch(uMsg) {
	// NOTE: (sonictk) Sent whenever the window is activated/deactivated.
	case WM_ACTIVATE:
	{
		// NOTE: (sonictk) For a fun release/debug bug, shadow wparam itself here
		// and the app will start paused always.
		WORD lwParam = LOWORD(wParam);
		switch (lwParam) {
		case WA_INACTIVE:
		{
			globalApplicationState = DX11_TUTORIAL_APPLICATION_PAUSED;
			BOOL status = ClipCursor(NULL);
			ShowCursor(TRUE);
			if (status == 0) {
				OutputDebugString("Unable to release the cursor!\n");
			}
			break;
		}
		case WA_CLICKACTIVE:
			ShowCursor(FALSE);
		case WA_ACTIVE:
			globalApplicationState = DX11_TUTORIAL_APPLICATION_RUNNING;
			break;
		}

		return 0;
	}
	case WM_SETCURSOR:
		// NOTE: (sonictk) Checking against ``HTCLIENT`` ensures cursor is only
		// applied when we are actually within the client area to avoid overriding built-in cursors.
		if (LOWORD(lParam) == HTCLIENT) {
			SetCursor(globalMouseCursorHandle);

			return 0;
		}
		break;
	// NOTE: (sonictk) Sent when the user is resizing the window.
	case WM_SIZE:
		// NOTE: (sonictk) Save the new client dimensions.
		globalWindowWidth = (int)LOWORD(lParam);
		globalWindowHeight = (int)HIWORD(lParam);

		if (globalD3DDevice) {
			switch (wParam) {
			case SIZE_MINIMIZED:
				globalApplicationState = DX11_TUTORIAL_APPLICATION_PAUSED;
				globalWindowState |= DX11_TUTORIAL_WINDOW_STATE_MINIMIZED;
				globalWindowState &= DX11_TUTORIAL_WINDOW_STATE_MAXIMIZED;

				break;

			case SIZE_MAXIMIZED:
				globalApplicationState = DX11_TUTORIAL_APPLICATION_RUNNING;
				globalWindowState |= DX11_TUTORIAL_WINDOW_STATE_MAXIMIZED;
				globalWindowState &= DX11_TUTORIAL_WINDOW_STATE_MINIMIZED;

				break;

			case SIZE_RESTORED:
				if (globalWindowState & DX11_TUTORIAL_WINDOW_STATE_MINIMIZED) {
					globalApplicationState = DX11_TUTORIAL_APPLICATION_RUNNING;
					globalWindowState |= DX11_TUTORIAL_WINDOW_STATE_MAXIMIZED;
					globalWindowState &= DX11_TUTORIAL_WINDOW_STATE_MINIMIZED;

					OnResize();

				} else if (globalWindowState & DX11_TUTORIAL_WINDOW_STATE_MAXIMIZED) {
					globalApplicationState = DX11_TUTORIAL_APPLICATION_PAUSED;
					globalWindowState |= DX11_TUTORIAL_WINDOW_STATE_MINIMIZED;
					globalWindowState &= DX11_TUTORIAL_WINDOW_STATE_MAXIMIZED;

					OnResize();

				} else if (globalWindowState & DX11_TUTORIAL_WINDOW_STATE_RESIZING) {
					// NOTE: (sonictk) If a user is dragging the resize bars,
					// do not resize the buffers since ``WM_SIZE`` messages are
					// being sent continuously to the window. Instead, only resize
					// the buffers once the user releases the bars, which sends a
					// ``WM_EXITSIZEMOVE`` message.
					break;

				} else {
					OnResize();
				}

				break;
			default:
				break;
			}
		}

		return 0;

	// NOTE: (sonictk) Sent when the user is grabbing the resize handles.
	case WM_ENTERSIZEMOVE:
		globalApplicationState = DX11_TUTORIAL_APPLICATION_PAUSED;
		globalWindowState |= DX11_TUTORIAL_WINDOW_STATE_RESIZING;

		return 0;

	// NOTE: (sonictk) Sent when the user releases the resize handles.
	case WM_EXITSIZEMOVE:
		globalApplicationState = DX11_TUTORIAL_APPLICATION_RUNNING;
		globalWindowState &= DX11_TUTORIAL_WINDOW_STATE_RESIZING;

		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);

		return 0;

	// NOTE: (sonictk) This is sent when a menu is active and the user presses a
	// key that does not correspond to any mnemonic or accelerator.
	case WM_MENUCHAR:
		// NOTE: (sonictk) We do this to avoid the beep when hitting alt-enter.
		return MAKELRESULT(0, MNC_CLOSE);

	// NOTE: (sonictk) Gets sent when the size/pos of the window is about to change.
	case WM_GETMINMAXINFO:
		// NOTE: (sonictk) We intercept this to prevent the window from becoming too small.
		((MINMAXINFO *)lParam)->ptMinTrackSize.x = globalMinimumWindowWidth;
		((MINMAXINFO *)lParam)->ptMinTrackSize.y = globalMinimumWindowHeight;

		return 0;

	case WM_MOUSEMOVE:
		OnMouseMove(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

		return 0;

	case WM_INPUT:
	{
		// NOTE: (sonictk) Only move the camera if the UI modifier key is not being
		// held down actively.
		if (globalKeysDown & globalKeyMask_UIModifier) {
			return 0;
		}

		UINT dwSize;
		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));

		globalVar LPBYTE lpb = (LPBYTE)HeapAlloc(globalProcessHeap, 0, (SIZE_T)dwSize);
		if (lpb == NULL) {
			return 0;
		}

		if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize) {
			OutputDebugString("GetRawInputData returns mismatched sizes!\n");

			return 0;
		};

		RAWINPUT* raw = (RAWINPUT*)lpb;
		if (raw->header.dwType == RIM_TYPEMOUSE) {
			globalMouseDeltaX = raw->data.mouse.lLastX;
			globalMouseDeltaY = raw->data.mouse.lLastY;
		}

		return 0;
	}

	case WM_LBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_RBUTTONDOWN:
		OnMouseDown(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

		return 0;

	case WM_LBUTTONUP:
	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
		OnMouseUp(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));

		return 0;

	case WM_MOUSEWHEEL:
	{
		short wheelRotation = GET_WHEEL_DELTA_WPARAM(wParam);
		OnMouseWheelScroll(wheelRotation);

		return 0;
	}
	case WM_KEYDOWN:
		OnKeyPress(wParam);

		return 0;

	case WM_KEYUP:
		OnKeyRelease(wParam);

		return 0;

	default:
		break;
	}

	// NOTE: (sonictk) As per MSDN guidance, forward all other unhandled messages
	// to the default window procedure to let Windows take care of it.
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}


/**
 * Entry point for the application.
 *
 * @param hInstance 		The handle to the current instance of the application.
 * @param hPrevInstance 	Unused. Always ``NULL``. Signature required for Windows.
 * @param lpCmdLine 		The command line for the application.
 * @param nCmdShow 		Controls how the window is meant to be shown.
 *
 * @return					Exit code.
 */
int CALLBACK WinMain(HINSTANCE hInstance,
					 HINSTANCE hPrevInstance,
					 LPSTR lpCmdLine,
					 int nCmdShow)
{
	globalProcessHeap = GetProcessHeap();
	if (globalProcessHeap == NULL) {
		DWORD winError = GetLastError();
		MessageBox(0, "Unable to accquire handle to the process heap! Is the system out of memory?.", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	BOOL status;
	status = QueryPerformanceFrequency(&globalCPUTimerFreq);
	if (status == 0) {
		DWORD winError = GetLastError();
		MessageBox(0, "The system does not support a high-resolution timer! This hardware is not suppoorted.", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	if (!dx::XMVerifyCPUSupport()) {
		MessageBox(0, "The system does not support DirectXMath libraries! This hardware is not suppoorted.", 0, 0);

		return DX11_TUTORIAL_NO_DXMATH_SUPPORT;
	}

	// NOTE: (sonictk) Load the custom cursor resource.
	static char appDir[MAX_PATH];
	getAppPath(appDir, MAX_PATH);

	// NOTE: (sonictk) Fudgery here to convert to wide char for Windows path functions.
	wchar_t appDirW[MAX_PATH];
	mbstowcs(appDirW, appDir, MAX_PATH);
	PathCchRemoveFileSpec((PWSTR)appDirW, MAX_PATH);

	memset(appDir, 0, sizeof(appDir));
	wcstombs(appDir, appDirW, MAX_PATH);

	globalVar char globalCursorImagePath[MAX_PATH];

	sprintf(globalCursorImagePath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, APPLICATION_IMAGES_DIRECTORY_NAME, WIN32_OS_PATH_SEPARATOR, globalCursorImageFilename);
	DWORD cursorImageFileAttrs = GetFileAttributes((LPCTSTR)globalCursorImagePath);
	if (cursorImageFileAttrs & FILE_ATTRIBUTE_DIRECTORY || cursorImageFileAttrs == INVALID_FILE_ATTRIBUTES)
	{
		MessageBox(0, "Failed to load cursor file resource!", "Error!", MB_ICONERROR);

		return -5;
	}

	int cursorWidth;
	int cursorHeight;
	int cursorChannels;
	uint8_t *imgData = (uint8_t *)stbi_load(globalCursorImagePath,
											   &cursorWidth,
											   &cursorHeight,
											   &cursorChannels,
											   STBI_rgb_alpha);
	if (!imgData) {
		return DX11_TUTORIAL_FAILED_TO_LOAD_IMAGE_DATA_FROM_FILE;
	}

	globalMouseCursorHandle = CreateCursorFromImage(globalMainWindowHandle, cursorWidth, cursorHeight, (uint32_t *)imgData, cursorWidth / 2, cursorHeight / 2);
	if (globalMouseCursorHandle == NULL) {
		MessageBox(0, "Failed to create cursor using custom sprite!", "Error!", MB_ICONERROR);
	} else {
		SetCursor(globalMouseCursorHandle);

		stbi_image_free(imgData);
	}

	// NOTE: (sonictk) the ``windowClass`` struct describes the properties of the
	// window that we are about to create.
	WNDCLASSEX windowClass;
	windowClass.cbSize = sizeof(WNDCLASSEX); // NOTE: (sonictk) This is what MSDN says to do
	windowClass.style = CS_HREDRAW|CS_VREDRAW; // NOTE: (sonictk) Redraws the entire window if the width/height of the client area is changed (where we are drawing d3d to.)
	windowClass.lpfnWndProc = WindowProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(0, IDI_APPLICATION); // NOTE: (sonictk) Default application icon
	windowClass.hCursor = LoadCursor(0, IDC_CROSS); // NOTE: (sonictk) Crosshair icon
	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH); // NOTE: (sonictk) Brush used for painting the background
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = D3D11_TUTORIAL_WINDOW_CLASS_NAME;
	windowClass.hIconSm = NULL; // NOTE: (sonictk) Small icon for the window class.

	ATOM result = RegisterClassEx(&windowClass);
	if (result == 0) {
		DWORD winError = GetLastError();
		switch(winError) {
		case ERROR_CLASS_ALREADY_EXISTS:
			MessageBox(0, "Failed to register window class; it already exists!", 0, 0);
			break;
		default:
			MessageBox(0, "Failed to register window class; an unknown error occurred!", 0, 0);
			break;
		}

		return (int)HRESULT_FROM_WIN32(winError);
	}

	// NOTE: (sonictk) Because CreateWindow() also includes the menu bar and the
	// border in the width/height passed in, we get the _real_ dimensions that
	// should be passed in here by letting Windows calculate what they should be
	// so that we can have proper width/height for the client area we're drawing in.
	RECT clientRect = {0, 0, globalWindowWidth, globalWindowHeight};
	status = AdjustWindowRectEx(&clientRect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_OVERLAPPEDWINDOW); // NOTE: (sonictk) Use same style that we pass in later to CreateWindowEx().
	if (status == 0) {
		DWORD winError = GetLastError();
		MessageBox(0, "Failed to calculate client dimensions; an unknown error occurred!", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	globalMainWindowHandle = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
											D3D11_TUTORIAL_WINDOW_CLASS_NAME,
											D3D11_TUTORIAL_WINDOW_TITLE,
											WS_OVERLAPPEDWINDOW, // NOTE: (sonictk) includes title bar, window menu, sizing border, minimize/maximize buttons.
											CW_USEDEFAULT, // NOTE: (sonictk) initial x pos
											CW_USEDEFAULT, // NOTE: (sonictk) initial y pos
											clientRect.right - clientRect.left,
											clientRect.bottom - clientRect.top,
											NULL, // NOTE: (sonictk) No parent window
											NULL, // NOTE: (sonictk) No menu or child window
											hInstance, // NOTE: (sonictk) Module associated with this window
											NULL); // NOTE: (sonictk) No additional data needed

	if (globalMainWindowHandle == NULL) {
		DWORD winError = GetLastError();
		MessageBox(0, "Failed to create window!", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	// NOTE: (sonictk) Show the window using the flag specified by the program that
	// started the application, and send the app a WM_PAINT message.
	ShowWindow(globalMainWindowHandle, nCmdShow);
	status = UpdateWindow(globalMainWindowHandle);
	if (status == 0) {
		MessageBox(0, "Failed to update window!", 0, 0);

		return DX11_TUTORIAL_WINDOW_UPDATE_FAILED;
	}

	status = InitializeD3D(hInstance, globalVSyncEnabled);
	if (status != DX11_TUTORIAL_SUCCESS) {
		MessageBox(globalMainWindowHandle, "Failed to initialize Direct3D swap chain!", 0, 0);

		return DX11_TUTORIAL_D3D_SWAP_CHAIN_INITIALIZATION_FAILED;
	}

	// NOTE: (sonictk) Setup Dear ImGui context.
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& imGuiIO = ImGui::GetIO();
	imGuiIO.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	ImGui::StyleColorsClassic();

	// NOTE: (sonictk) Setup platform/renderer bindings for ImGui.
	ImGui_ImplWin32_Init(globalMainWindowHandle);
	ImGui_ImplDX11_Init(globalD3DDevice, globalD3DDeviceContext);

	status = RegisterRawInputForMouse(globalMainWindowHandle);
	if (status != TRUE) {
		DWORD winError = GetLastError();
		MessageBox(0, "Failed to register raw input; an unhandled error occurred!", 0, 0);

		return (int)HRESULT_FROM_WIN32(winError);
	}

	// NOTE: (sonictk) Initialize audio.
	XAudio2Status audioStatus = InitializeXAudio2(&globalXAudio2Engine, &globalXAudio2MasteringVoice);
	if (audioStatus != XAudio2Status_Success) {
		MessageBox(0, "Could not initialize audio engine!", "Error!", MB_ICONERROR);

		return -3;
	}

#if _DEBUG
	XAUDIO2_DEBUG_CONFIGURATION xAudio2DebugCfg;
	ZeroMemory(&xAudio2DebugCfg, sizeof(XAUDIO2_DEBUG_CONFIGURATION));

	xAudio2DebugCfg.TraceMask = XAUDIO2_LOG_ERRORS|XAUDIO2_LOG_WARNINGS;
	xAudio2DebugCfg.BreakMask = XAUDIO2_LOG_ERRORS|XAUDIO2_LOG_WARNINGS;
	xAudio2DebugCfg.LogThreadID = TRUE;
	xAudio2DebugCfg.LogFileline = TRUE;
	xAudio2DebugCfg.LogFunctionName = TRUE;
	xAudio2DebugCfg.LogTiming = TRUE;

	globalXAudio2Engine->SetDebugConfiguration(&xAudio2DebugCfg);
#endif

	globalVar char globalTestBGMusicWAVFilePath[MAX_PATH];

	sprintf(globalTestBGMusicWAVFilePath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, APPLICATION_AUDIO_DIRECTORY_NAME, WIN32_OS_PATH_SEPARATOR, globalTestBGMusicWAVFilename);
	DWORD testBGMusicWAVFileAttrs = GetFileAttributes((LPCTSTR)globalTestBGMusicWAVFilePath);
	if (testBGMusicWAVFileAttrs & FILE_ATTRIBUTE_DIRECTORY || testBGMusicWAVFileAttrs == INVALID_FILE_ATTRIBUTES)
	{
		MessageBox(0, "Audio test file was not found on disk!", "Error reading file!", MB_ICONERROR);

		return -2;
	}

	globalVar char globalTestSFXWAVFilePath[MAX_PATH];

	sprintf(globalTestSFXWAVFilePath, "%s%c%s%c%s", appDir, WIN32_OS_PATH_SEPARATOR, APPLICATION_AUDIO_DIRECTORY_NAME, WIN32_OS_PATH_SEPARATOR, globalTestSFXFilename);
	DWORD testSFXWAVFileAttrs = GetFileAttributes((LPCTSTR)globalTestSFXWAVFilePath);
	if (testSFXWAVFileAttrs & FILE_ATTRIBUTE_DIRECTORY || testSFXWAVFileAttrs == INVALID_FILE_ATTRIBUTES)
	{
		MessageBox(0, "Audio test file was not found on disk!", "Error reading file!", MB_ICONERROR);

		return -2;
	}

	// NOTE: (sonictk) XAudio2 voice processing chain:
	// source voice -> submix voice (fx processing and mixing for performance) -> mastering voice -> writes audio data to audio device.
	// Not necessary to use a submix voice, but I'm just experimenting with the API.
	HRESULT hr = globalXAudio2Engine->CreateSubmixVoice(&globalSubmixVoice, 2, 48000, 0, 0, NULL, NULL);
	if (FAILED(hr)) {
		MessageBox(0, "Failed to create audio submix voice!", "Xaudio2 API error!", MB_ICONERROR);

		return (int)HRESULT_FROM_WIN32(GetLastError());
	}

	XAUDIO2_SEND_DESCRIPTOR SFXSend;
	ZeroMemory(&SFXSend, sizeof(XAUDIO2_SEND_DESCRIPTOR));
	SFXSend.Flags = 0;
	SFXSend.pOutputVoice = globalSubmixVoice;

	XAUDIO2_VOICE_SENDS SFXSendList;
	ZeroMemory(&SFXSendList, sizeof(XAUDIO2_VOICE_SENDS));
	SFXSendList.SendCount = 1;
	SFXSendList.pSends = &SFXSend;

	XAUDIO2FX_REVERB_PARAMETERS reverbParams;
	SetXAudio2FXDefaultReverbParams(reverbParams);

	IUnknown *XAPOReverb = NULL;
	CreateXAPOReverbForVoice(globalSubmixVoice, true, reverbParams, &XAPOReverb);

	// NOTE: (sonictk) This decrements the refcount and allows XAudio2 to take
	// ownership of the APO (Audio Processing Object) and manage its
	// lifetime. Can maintain reference and not call this if this APO is meant
	// to be re-used.
	ReleaseCOM(XAPOReverb);

	InitializeX3DAudio(globalXAudio2MasteringVoice, globalX3DInstance);
	TestInitializeX3DAudio();

	TestPlayAudio(globalTestBGMusicWAVFilePath, &SFXSendList, XAUDIO2_MAX_LOOP_COUNT, &globalBGSourceVoice, 0, 1.0f);
	TestPlayAudio(globalTestSFXWAVFilePath, &SFXSendList, XAUDIO2_MAX_LOOP_COUNT, &globalSFXSourceVoice, 0, 1.0f);

	// NOTE: (sonictk) Once the app has been created and initialized, enter the
	// message loop. Stay in this loop until a ``WM_QUIT`` message is received,
	// indicating that the app should be terminated.
	int exitCode = Run();

	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	// NOTE: (sonictk) Make sure we release all COM objects so that Windows can
	// re-accquire the memory.
	CleanupD3D();

	DestroyWindow(globalMainWindowHandle);
	UnregisterClass(D3D11_TUTORIAL_WINDOW_CLASS_NAME, windowClass.hInstance);

	CleanupXAudio2();

	return exitCode;
}
