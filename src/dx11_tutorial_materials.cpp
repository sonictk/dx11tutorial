#include "dx11_tutorial_materials.h"


PhongMaterial createPhongMaterial()
{
	PhongMaterial result;

	result.emissive = {0.0f, 0.0f, 0.0f, 1.0f};
	result.ambient =  {0.0f, 0.0f, 0.0f, 1.0f};
	result.diffuse =  {0.0f, 0.0f, 0.0f, 1.0f};
	result.specular =  {1.0f, 1.0f, 1.0f, 1.0f};
	result.specularExponent = 128.0f;
	result.useTexture = false;

	return result;
}


PhongMaterialProperties createPhongMaterialProperties()
{
	PhongMaterialProperties result;

	PhongMaterial mat = createPhongMaterial();

	result.material = mat;

	return result;
}
