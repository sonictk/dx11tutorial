#include "dx11_tutorial_lights.h"
#include <math.h>

namespace dx = DirectX;


globalVar LightProperties globalLightsProperties;

globalVar dx::XMVECTORF32 globalLightsColors[MAX_LIGHTS] = {DXColorWhite, DXColorWhite, DXColorWhite, DXColorWhite};
globalVar int globalLightsTypes[MAX_LIGHTS] = {POINT_LIGHT, POINT_LIGHT, DIRECTIONAL_LIGHT, SPOT_LIGHT};
globalVar int globalLightsEnabled[MAX_LIGHTS] = {true, true, true, true};


void CreateTestLights()
{
	for (int i=0; i < MAX_LIGHTS; ++i) {
		Light testLight;

		testLight.isEnabled = (int)globalLightsEnabled[i];
		testLight.lightType = globalLightsTypes[i];
		testLight.color = dx::XMFLOAT4(globalLightsColors[i]);
		testLight.spotAngle = dx::XMConvertToRadians(45.0f);
		testLight.constantAttenuation = 1.0f;
		testLight.linearAttenuation = 0.08f;
		testLight.quadraticAttenuation = 0.0f;
		testLight.position = dx::XMFLOAT4(0, 0, 0, 0);

		dx::XMVECTOR curDirection = dx::XMVectorSet(-testLight.position.x, -testLight.position.y, -testLight.position.z, 0.0f);
		curDirection = dx::XMVector3Normalize(curDirection);
		dx::XMStoreFloat4(&testLight.direction, curDirection);

		globalLightsProperties.sceneLights[i] = testLight;
	}
}


void UpdateLights(const double timeElapsed)
{
	static const float radius = 8.0;
	static const double offset = 2.0 * (double)dx::XM_PI / (double)MAX_LIGHTS;

	for (int i=0; i < MAX_LIGHTS; ++i) {
		globalLightsProperties.sceneLights[i].isEnabled = (int)globalLightsEnabled[i];
		globalLightsProperties.sceneLights[i].lightType = globalLightsTypes[i];
		globalLightsProperties.sceneLights[i].color = dx::XMFLOAT4(globalLightsColors[i]);
		globalLightsProperties.sceneLights[i].spotAngle = dx::XMConvertToRadians(45.0f);
		globalLightsProperties.sceneLights[i].constantAttenuation = 1.0f;
		globalLightsProperties.sceneLights[i].linearAttenuation = 0.08f;
		globalLightsProperties.sceneLights[i].quadraticAttenuation = 0.0f;

		// NOTE: (sonictk) Animate lights going in a circle
		dx::XMFLOAT4 curPos = dx::XMFLOAT4((float)sin((double)timeElapsed + (offset * (double)i)) * radius,
										   9.0f,
										   (float)cos((double)timeElapsed + (offset * (double)i)) * radius,
										   1.0f);

		globalLightsProperties.sceneLights[i].position = curPos;

		dx::XMVECTOR curDirection = dx::XMVectorSet(-curPos.x, -curPos.y, -curPos.z, 0.0f);
		curDirection = dx::XMVector3Normalize(curDirection);
		dx::XMStoreFloat4(&globalLightsProperties.sceneLights[i].direction, curDirection);
	}
}
