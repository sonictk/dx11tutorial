#include "dx11_tutorial_input.h"
#include <Windows.h>


BOOL RegisterRawInputForMouse(HWND hWnd)
{
	// TODO: (sonictk) Make this work for multiple mice
	RAWINPUTDEVICE Rid[1];
	Rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
	Rid[0].usUsage = HID_USAGE_GENERIC_MOUSE;
	Rid[0].dwFlags = 0; // NOTE: (sonictk) Can use ``RIDEV_INPUTSINK`` if need ``WM_INPUT`` fired even if app does not have focus
	Rid[0].hwndTarget = hWnd;

	return RegisterRawInputDevices(Rid, 1, sizeof(Rid[0]));
}
