#ifndef DX11_TUTORIAL_WIN32_AUDIO_H
#define DX11_TUTORIAL_WIN32_AUDIO_H

#include <Windows.h>
#include <Xaudio2.h>
#include <ks.h>
#include <ksmedia.h>
#include <x3daudio.h>


// NOTE: (sonictk) Taken from MSDN: https://docs.microsoft.com/en-us/windows/desktop/xaudio2/how-to--load-audio-data-files-in-xaudio2
// Each chunk's data type is indicated by a 32-bit uint of 4 ASCII chars concatenated.
#ifdef _XBOX //Big-Endian
#define fourccRIFF 'RIFF'
#define fourccDATA 'data'
#define fourccFMT 'fmt '
#define fourccWAVE 'WAVE'
#define fourccXWMA 'XWMA'
#define fourccDPDS 'dpds'
#endif

#ifndef _XBOX //Little-Endian
#define fourccRIFF 'FFIR'
#define fourccDATA 'atad'
#define fourccFMT ' tmf'
#define fourccWAVE 'EVAW'
#define fourccXWMA 'AMWX'
#define fourccDPDS 'sdpd'
#endif


/// Various status codes for operations performed in this audio API.
enum XAudio2Status
{
	XAudio2Status_Success,
	XAudio2Status_InterfaceCreationFailure,
	XAudio2Status_NoDefaultAudioDevice,
	XAudio2Status_MasteringVoiceCreationFailure,
	XAudio2Status_FileOpenFailed,
	XAudio2Status_SetFilePointerFailed,
	XAudio2Status_FileReadFailed,
	XAudio2Status_InvalidRIFFFileFormat,
	XAudio2Status_ChunkNotFound,
	XAudio2Status_XAPOCreationFailed,
	XAudio2Status_ApplyXAPOFXChainFailed,
	XAudio2Status_SetFXParamsFailed,

	XAudio2Status_Count
};


/**
 * Initializes XAudio2 objects for use.
 *
 * @param engine 		Pointer to pointer to the instance of the engine to initialize.
 * @param masterVoice 	Pointer to pointer to the instance of the main voice to initialize.
 *
 * @return 			``XAudio2Status_Success`` if the operation succeeded, or
 * 					one of the status codes otherwise.
 */
XAudio2Status InitializeXAudio2(IXAudio2 **engine, IXAudio2MasteringVoice **masterVoice);


/**
 * Finds a data chunk in the given file.
 *
 * @param hFile				The file handle to the file to find the chunk in.
 * @param fourcc				The FOURCC code of the chunk type to find.
 * @param chunkSize			Storage for the size of the chunk found.
 * @param chunkDataPosition	Storage for where the chunk is located in the file.
 *
 * @return						``XAudio2Status_Success`` if the operation succeeded, or
 * 							one of the status codes otherwise.
 */
XAudio2Status FindChunk(HANDLE hFile, DWORD fourcc, DWORD &chunkSize, DWORD &chunkDataPosition);


/**
 * Reads a specified amount of ``bufferSize`` bytes from the given file handle.
 *
 * @param hFile			The file handle to the file to read the bytes from.
 * @param buffer			Storage for the bytes read.
 * @param bufferSize		The number of bytes to read into the ``buffer``. The
 * 						size of the ``buffer`` should be equal to or larger
 * 						than this value.
 * @param bufferOffset		The offset to apply before reading the bytes from the file.
 *
 * @return					``XAudio2Status_Success`` if the operation succeeded, or
 *		 					one of the status codes otherwise.
 */
XAudio2Status ReadChunkData(HANDLE hFile, void *buffer, DWORD bufferSize, DWORD bufferOffset);


/**
 * Loads RIFF WAV audio data into a given buffer ``data`` from a file on disk.
 *
 * @param hFile	 	The handle to the audio file on disk. This must be a WAV file in RIFF format.
 * @param waveData 	The buffer for the wave format data storage.
 * @param buffer 		The buffer for the audio properties of how the WAV file should be utilized.
 * @param data 		The buffer for the audio data being read in. Is ignored if
 * 					``size`` is set to ``0``.
 * @param size 		The size of the ``data`` buffer. If ``0`` is passed, will
 * 					be set to the number of bytes that need to be allocated
 * 					in order to accomodate the ``data`` buffer. This call can
 * 					then be made again in order to actually store the data.
 */
XAudio2Status LoadWAVAudio(HANDLE hFile,
						   WAVEFORMATEXTENSIBLE &waveData,
						   XAUDIO2_BUFFER &buffer,
						   BYTE *data,
						   DWORD &size);


/**
 * Sets the default XAudio2 parameters for the given reverb settings.
 *
 * @param reverbParams 	The settings to reset to default values.
 */
void SetXAudio2FXDefaultReverbParams(XAUDIO2FX_REVERB_PARAMETERS &reverbParams);


/**
 * Creates and applies a reverb effect to the ``voice`` specified.
 *
 * @param voice			The XAudio2 source voice/submix voice to create and
 * 						apply the reverb effect for.
 * @param initialState		Whether the effect should start enabled or disabled.
 * @param reverbParams		The settings for the reverb effect.
 * @param XAPOReverb		A pointer to a pointer to the COM object to store the reverb APO.
 * 						This can be used to re-use the effect applied.
 *
 * @return					``XAudio2Status_Success`` if the operation succeeded, or
 *		 					one of the status codes otherwise.
 */
XAudio2Status CreateXAPOReverbForVoice(IXAudio2Voice *voice, bool initialState, XAUDIO2FX_REVERB_PARAMETERS reverbParams, IUnknown **XAPOReverb);


/**
 * Initializes the X3DAudio interface against the given XAudio2 Master voice.
 *
 * @param masterVoice	The master voice to get the audio channel format from.
 * @param X3DInstance	The handle to the X3D audio instance.

 */
void InitializeX3DAudio(IXAudio2MasteringVoice *masterVoice, X3DAUDIO_HANDLE &X3DInstance);


#endif /* DX11_TUTORIAL_WIN32_AUDIO_H */
