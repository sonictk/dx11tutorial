#ifndef DX11_TUTORIAL_MESH_H
#define DX11_TUTORIAL_MESH_H

#include "dx11_tutorial_vec.h"

// TODO: (sonictk) Custom allocator to handle memory mgmt for meshes from an arena pool.
#include <vector>


struct MeshData
{
	std::vector<VertexTexture> vertices;
	std::vector<unsigned int> indices;
};


// TODO: (sonictk) Convert this API to use MeshData instead to simplify usage.

/**
 * Calculates the midpoint for two vertices.
 *
 * @param v1	The first vertex.
 * @param v2	The second vertex.
 *
 * @return 	The mid-point between the two vertices.
 */
VertexTexture midPoint(const VertexTexture &v1, const VertexTexture &v2);


/**
 * Subdivides a given triangulated mesh.
 * This can be called with ``outVertices`` set to ``NULL`` to determine the size
 * required to be allocated for ``outVertices``. The same goes for ``outIndicesCount``.
 *
 * @param vertices				The vertices of the mesh triangles to subdivide.
 * 							These vertices should describe triangles in continguous order.
 *
 * @param numVertices			The number of vertices to read from the ``vertices`` buffer.
 * 							This should be a multiple of 3.
 *
 * @param outVertices			The storage for the vertices of the subdivided mesh.
 * 							May be set to ``NULL`` in order to determine the
 * 							size that should be allocated for this buffer.
 *
 * @param numSubdivs			The number of times to subdivide the mesh.
 *
 * @param outVerticesCount		The storage for the number of vertices of the subdivided mesh.
 * 							If this is not needed, ``NULL`` may be passed.
 *
 * @param outIndices			The storage for the vertex indices of the subdivided mesh.
 * 							If indices are not required to be generated, or if
 * 							trying to determine the size of the buffer, ``NULL`` may
 * 							be passed.
 *
 * @param outIndicesCount		The storage for the number of indices in the indices buffer
 * 							for the subdivided mesh. If indices are not required
 * 							to be generated, ``NULL`` may be passed.
 */
void subdivideMesh(VertexTexture *vertices,
				   const int numVertices,
				   VertexTexture *outVertices,
				   const int numSubdivs,
				   int *outVerticesCount,
				   short *outIndices,
				   int *outIndicesCount);


/**
 * Creates a mesh consisting of a single quad.
 *
 * @param width		The width of the quad.
 * @param height		The height of the quad.
 * @param x			The x-coordinate where the quad should be placed.
 * @param y			The y-coordinate where the quad should be placed.
 * @param z			The -coordinate where the quad should be placed.
 *
 * @param vertices		The storage for the vertices generated. Should be of size 4.
 *
 * @param indices		The storage for the indices generated. Should be of size 6.
 * 					If there is no need to generate indices, may be set to ``NULL``.
 */
void createQuad(const float width,
				const float height,
				const float x,
				const float y,
				const float z,
				VertexTexture *vertices,
				short *indices);


/**
 * Creates a grid plane.
 *
 * @param width 	The width of the plane to create.
 * @param depth 	The depth of the plane to create.
 * @param rows 	The number of subdivisions.along one axis. This cannot be lower than 2.
 * @param columns 	The number of subvidisions along the other axis. This cannot be lower than 2.
 *
 * @param vertices  The storage for the grid vertices generated. This must have a size
 * 				of at least ``rows x columns``.
 *
 * @param indices	The storage for the indices of the grid generated. This must
 * 				a size of at least ``(rows - 1) x (columns - 1) x 6``. May be
 * 				set to ``NULL``, in which case no indices will be generated.
 */
void createGridMesh(const float width,
					const float depth,
					const int rows,
					const int columns,
					VertexTexture *vertices,
					short *indices);


/**
 * Generates vertex positions for a polygonal cylindrical mesh.
 *
 * @param radiusBottom 	The radius of the base of the cylinder.
 * @param radiusTop 		The radius of the top of the cylinder. Set ``radiusBottom`` to a positive value and this to ``0`` for a cone.
 * @param height 			The height of the cylinder.
 * @param slices 			The number of segments across the radius of the cylinder.
 * @param stacks 			The number of segments across the height of the cylinder.
 * @param vertices 		Storage for the output vertices. This _must_ have at least
 * 						size of ``numSlices * numStacks``.
 */
void createCylinderMesh(const float radiusBottom,
						const float radiusTop,
						const float height,
						const int slices,
						const int stacks,
						const bool capTop,
						const bool capBottom,
						VertexTexture *vertices,
						short *indices);


/**
 * Creates a geosphere mesh where all faces are of uniform size as far as possible.
 *
 * @param radius 			Determines the size of the geosphere mesh created.
 *
 * @param numSubdivs		The number of subdivisions that the geosphere mesh will have.
 * 						Warning: using a large number of subdivisions ( > 5) could result
 * 						in poor performance for this function.
 *
 * @param vertices 		The storage for the vertices generated. This _must_ have
 * 						at least enough memory allocated to store the vertices generated.
 * 						May be set to ``NULL`` in order to determine the number
 * 						of vertices required for creating the geosphere.
 *
 * @param outVerticesCount	The storage for the number of vertices for the new geosphere created.
 * 						If this is not needed, may be set to ``NULL``.
 *
 * @param indices 			The storage for the vertex indices generated. This _must_
 * 						have at least enough memory allocated to store the indices generated.
 * 						If indices are not required or if you are determining the
 * 						amount of memory required for allocation, this parameter
 * 						may be set to ``NULL``.
 *
 * @param outIndicesCount	The storage for the number of indices for the new geosphere created.
 * 						If this is not needed, may be set to ``NULL``.
 */
void createGeosphere(const float radius,
					 const int numSubdivs,
					 VertexTexture *vertices,
					 int *outVerticesCount,
					 short *indices,
					 int *outIndicesCount);


#endif /* DX11_TUTORIAL_MESH_H */
