#include "dx11_tutorial_win32_cursor.h"


// NOTE: (sonictk) Implementation from https://hero.handmade.network/forums/code-discussion/t/1741-how_to_implement_mouse_cursor
// Refer to https://www.codeproject.com/Articles/5220/Creating-a-color-cursor-from-a-bitmap for explanation of how Windows handles cursor display.
HCURSOR CreateCursorFromImage(HWND hwnd,
							  int width,
							  int height,
							  uint32_t *agbr,
							  int xHotspot,
							  int yHotspot)
{
	BITMAPV5HEADER bitmapHeader;
	ZeroMemory(&bitmapHeader, sizeof(BITMAPV5HEADER));
	bitmapHeader.bV5Size = sizeof(BITMAPV5HEADER);
	bitmapHeader.bV5Width = (LONG)width;
	bitmapHeader.bV5Height = -(LONG)height; // NOTE: (sonictk) Negative value indicates top-down DIB
	bitmapHeader.bV5Planes = 1;
	bitmapHeader.bV5BitCount = 32;
	bitmapHeader.bV5Compression = BI_BITFIELDS;
	bitmapHeader.bV5RedMask = 0x00ff0000;
	bitmapHeader.bV5GreenMask = 0x0000ff00;
	bitmapHeader.bV5BlueMask = 0x000000ff;
	bitmapHeader.bV5AlphaMask = 0xff000000;

	HDC handleDeviceContext = GetDC(hwnd);
	if (handleDeviceContext == NULL) {
		return NULL;
	}

	void *bits = NULL;
	HBITMAP bitmap = CreateDIBSection(handleDeviceContext,
									  (BITMAPINFO *)&bitmapHeader,
									  DIB_RGB_COLORS,
									  (void **)&bits,
									  NULL,
									  0);

	int status = ReleaseDC(NULL, handleDeviceContext);
	if (status != 1) {
		return NULL;
	}

	uint32_t *ptrBits = (uint32_t *)bits;
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < height; ++x) {
			uint32_t c = agbr[x + (y * width)];
			uint32_t a = (c & 0xff000000) >> 24;
			uint32_t b = (c & 0x00ff0000) >> 16;
			uint32_t g = (c & 0x0000ff00) >> 8;
			uint32_t r = (c & 0x000000ff);

			*ptrBits++ = (a << 24) | (r << 16) | (g << 8) | b;
		}
	}

	HBITMAP emptyMask = CreateBitmap(width, height, 1, 1, NULL);
	if (emptyMask == NULL) {
		return NULL;
	}

	ICONINFO cursorIconInfo;
	cursorIconInfo.fIcon = FALSE; // NOTE: (sonictk) This means cursor.
	cursorIconInfo.xHotspot = (DWORD)xHotspot;
	cursorIconInfo.yHotspot = (DWORD)yHotspot;
	cursorIconInfo.hbmMask = emptyMask;
	cursorIconInfo.hbmColor = bitmap;

	HCURSOR cursor = CreateIconIndirect(&cursorIconInfo);
	if (cursor == NULL) {
		return NULL;
	}

	BOOL result = DeleteObject(bitmap);
	if (result == 0) {
		return NULL;
	}

	result = DeleteObject(emptyMask);
	if (result == 0) {
		return NULL;
	}

	return cursor;
}
