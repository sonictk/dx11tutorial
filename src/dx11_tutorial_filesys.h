#ifndef DX11_TUTORIAL_FILESYS_H
#define DX11_TUTORIAL_FILESYS_H


static const char WIN32_OS_PATH_SEPARATOR = '\\';
static const char UNIX_OS_PATH_SEPARATOR = '/';


/**
 * This function retrieves the full file path to the currently running executable.
 * On Windows, this requires linking against ``Kernel32.lib``.
 *
 * @param filename		The buffer to store the full file path in. Needs to be
 * 					long enough to store the full path in, else it will be
 * 					truncated.
 * @param len			The number of characters that will be copied to the buffer.
 *
 * @return				The number of characters copied to the buffer.
 * 					On error, returns ``-1``.
 */
int getAppPath(char *filename, const unsigned int len);


#endif /* DX11_TUTORIAL_FILESYS_H */
