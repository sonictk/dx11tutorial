#include "dx11_tutorial_test_data.h"
#include "dx11_tutorial_vec.h"

#define TEST_CYLINDER_NUM_STACKS 6
#define TEST_CYLINDER_NUM_SEGMENTS 8


// NOTE: (sonictk) Test primitive shapes
globalVar const Vertex testCubeVertices[] = {
	{DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), (const DirectX::XMFLOAT4)DXColorWhite},
	{DirectX::XMFLOAT3(-1.0f,  1.0f, -1.0f), (const DirectX::XMFLOAT4)DXColorBlack},
	{DirectX::XMFLOAT3( 1.0f,  1.0f, -1.0f), (const DirectX::XMFLOAT4)DXColorRed},
	{DirectX::XMFLOAT3( 1.0f, -1.0f, -1.0f), (const DirectX::XMFLOAT4)DXColorGreen},
	{DirectX::XMFLOAT3(-1.0f, -1.0f,  1.0f), (const DirectX::XMFLOAT4)DXColorBlue},
	{DirectX::XMFLOAT3(-1.0f,  1.0f,  1.0f), (const DirectX::XMFLOAT4)DXColorYellow},
	{DirectX::XMFLOAT3( 1.0f,  1.0f,  1.0f), (const DirectX::XMFLOAT4)DXColorCyan},
	{DirectX::XMFLOAT3( 1.0f, -1.0f,  1.0f), (const DirectX::XMFLOAT4)DXColorMagenta},
};


// NOTE: (sonictk) Test example usage of index buffers.
globalVar const UINT testVtxIndices[] = {
	0, 1, 2, 0, 2, 3, // NOTE: (sonictk) 6 quads, 2 tris per quad
	4, 6, 5, 4, 7, 6,
	4, 5, 1, 4, 1, 0,
	3, 2, 6, 3, 6, 7,
	1, 5, 6, 1, 6, 2,
	4, 0, 3, 4, 3, 7
};


/// Vertices for a test unit plane with UVs.
globalVar VertexTexture testPlaneVertices[4] = {
	{DirectX::XMFLOAT3(-0.5f, 0.0f,  0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 0.0f)},
	{DirectX::XMFLOAT3( 0.5f, 0.0f,  0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 0.0f)},
	{DirectX::XMFLOAT3( 0.5f, 0.0f, -0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(1.0f, 1.0f)},
	{DirectX::XMFLOAT3(-0.5f, 0.0f, -0.5f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f), DirectX::XMFLOAT2(0.0f, 1.0f)}
};

/// Index buffer for the unit plane. Basically two tris to form the plane.
globalVar short testPlaneIndices[6] = {0, 1, 3, 1, 2, 3};


globalVar ID3D11Buffer *globalPlanesVertexBuffer = NULL;
globalVar ID3D11Buffer *globalPlanesInstanceBuffer = NULL;
globalVar ID3D11Buffer *globalPlanesIndexBuffer = NULL;


globalVar ID3D11Buffer *globalCylinderVertexBuffer = NULL;
globalVar ID3D11Buffer *globalCylinderVertIndicesBuffer = NULL;


globalVar ID3D11ShaderResourceView *globalTxCheckerShaderResourceView = NULL;


globalVar VertexTexture globalTestCylinderVertices[TEST_CYLINDER_NUM_STACKS * TEST_CYLINDER_NUM_SEGMENTS];
globalVar short globalTestCylinderVertIndices[TEST_CYLINDER_NUM_STACKS * TEST_CYLINDER_NUM_SEGMENTS];
