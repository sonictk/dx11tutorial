/**
 * @file   dx11_tutorial_d3d_colors.h
 * @brief  Constant colours.
 */
#ifndef DX11_TUTORIAL_D3D_COLORS_H
#define DX11_TUTORIAL_D3D_COLORS_H

#include <DirectXMath.h>

// #define XMGLOBALCONST extern CONST __declspec(selectany)
// 1. extern so there is only one copy of the variable, and not a
// separate private copy in each .obj.
// 2. __declspec(selectany) so that the compiler does not complain
// about multiple definitions in a .cpp file (it can pick anyone
// and discard the rest because they are constant--all the same).
XMGLOBALCONST DirectX::XMVECTORF32 DXColorWhite = {1.0f, 1.0f, 1.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorBlack = {0.0f, 0.0f, 0.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorRed = {1.0f, 0.0f, 0.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorGreen = {0.0f, 1.0f, 0.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorBlue = {0.0f, 0.0f, 1.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorYellow = {1.0f, 1.0f, 0.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorCyan = {0.0f, 1.0f, 1.0f, 1.0f};
XMGLOBALCONST DirectX::XMVECTORF32 DXColorMagenta = {1.0f, 0.0f, 1.0f, 1.0f};

#endif /* DX11_TUTORIAL_D3D_COLORS_H */
