/**
 * @file   dx11_tutorial_materials.h
 * @brief  All functions, data structures related to materials.
 */
#ifndef DX11_TUTORIAL_MATERIALS_H
#define DX11_TUTORIAL_MATERIALS_H

#include <DirectXMath.h>


/// Stores the material definitions used in the pixel shader.
struct PhongMaterial
{
	DirectX::XMFLOAT4 emissive;
	DirectX::XMFLOAT4 ambient;
	DirectX::XMFLOAT4 diffuse;
	DirectX::XMFLOAT4 specular;
	float specularExponent;
	int useTexture; // NOTE: (sonictk) HLSL bool is 4 bytes, not 2 like in C.
	int _padding[2]; // NOTE: (sonictk) Satisfy alignment requirements of HLSL; make
	                // sure that the packing is done exactly the same as how the HLSL
	                // compiler will arrange the memory.
};


/// Provides uniform access to the material properties in the pixel shader.
struct PhongMaterialProperties
{
	PhongMaterial material;
};


PhongMaterial createPhongMaterial();


PhongMaterialProperties createPhongMaterialProperties();


#endif /* DX11_TUTORIAL_MATERIALS_H */
