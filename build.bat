@echo off
REM     Build script for the Direct3D 11 tutorial project(s).
REM     Usage: build.bat [debug|release|clean] <project_name> [msvc|clang] [exe|dll] <additional_linker_arguments>

setlocal

echo Build script started executing at %time% ...
REM Process command line arguments. Default is to build in release configuration.
set BuildType=%1
if "%BuildType%"=="" (set BuildType=release)

set ProjectName=dx11_tutorial

set Compiler=%2
if "%Compiler%"=="" (set Compiler=msvc)

echo Building %ProjectName% in %BuildType% configuration using %Compiler% ...

set MSVCVersion=%3
if "%MSVCVersion%"=="" (set MSVCVersion=2019)


if "%Compiler%"=="msvc" (
echo Building %ProjectName% in %BuildType% configuration using MSVC %MSVCVersion%...

REM    Set up the Visual Studio environment variables for calling the MSVC compiler;
REM    we do this after the call to pushd so that the top directory on the stack
REM    is saved correctly; the check for DevEnvDir is to make sure the vcvarsall.bat
REM    is only called once per-session (since repeated invocations will screw up
REM    the environment)

if defined DevEnvDir goto build_setup

if %MSVCVersion%==2015 (
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
goto build_setup

)

if %MSVCVersion%==2017 (
call "%vs2017installdir%\VC\Auxiliary\Build\vcvarsall.bat" x64
goto build_setup
)

if %MSVCVersion%==2019 (
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
goto build_setup
)

echo Invalid MSVC compiler version specified.
goto error
)

:build_setup


REM     Make a build directory to store artifacts; remember, %~dp0 is just a special
REM     FOR variable reference in Windows that specifies the current directory the
REM     batch script is being run in.
set BuildDir=%~dp0msbuild

set ResourcesDir=%~dp0resource

if "%BuildType%"=="clean" (
    REM This allows execution of expressions at execution time instead of parse time, for user input
    setlocal EnableDelayedExpansion
    echo Cleaning build from directory: %BuildDir%. Files will be deleted^^!
    echo Continue ^(Y/N^)^?
    set /p ConfirmCleanBuild=
    if "!ConfirmCleanBuild!"=="Y" (
        echo Removing files in %BuildDir%...
        del /s /q %BuildDir%\*.*
    )
    goto end
)

echo Building in directory: %BuildDir% ...

if not exist %BuildDir% mkdir %BuildDir%

if %errorlevel% neq 0 goto error

pushd %BuildDir%

set EntryPoint=%~dp0src\%ProjectName%_main.cpp

set OutBin=%BuildDir%\%ProjectName%.exe

set ImGuiIncludeDir=%~dp0%thirdparty\imgui
set ImGuiExamplesDir=%ImGuiIncludeDir%\examples
set IncludePaths=%~dp0%thirdparty


REM Setup HLSL compilation settings
set ShadersDir=%~dp0resource\shaders
set PrecompiledHeadersDir=%BuildDir%\inc

echo Placing pre-compiled headers in: %PrecompiledHeadersDir% ...

if not exist "%PrecompiledHeadersDir%" mkdir "%PrecompiledHeadersDir%"

if %errorlevel% neq 0 goto error

set ShaderExt=hlsl

set SimpleVertexShaderName=SimpleVertexShader
set SimpleVertexShaderEntryPoint=%ShadersDir%\%SimpleVertexShaderName%.%ShaderExt%
set CommonSimpleVertexShaderCompilerFlags=/T:vs_5_0 /WX /E:main /Fh:"%PrecompiledHeadersDir%\%SimpleVertexShaderName%.h" /Fo:"%BuildDir%\%SimpleVertexShaderName%.fxc" /Vn:"g_SimpleVertexShader_main"
set DebugSimpleVertexShaderCompilerFlags=/Od /Zi
set ReleaseSimpleVertexShaderCompilerFlags=/O1 /Qstrip_debug

set SingleInstanceVertexShaderName=SingleInstanceVertexShader
set SingleInstanceVertexShaderEntryPoint=%ShadersDir%\%SingleInstanceVertexShaderName%.%ShaderExt%
set CommonSingleInstanceVertexShaderCompilerFlags=/T:vs_5_0 /WX /E:main /Fh:"%PrecompiledHeadersDir%\%SingleInstanceVertexShaderName%.h" /Fo:"%BuildDir%\%SingleInstanceVertexShaderName%.fxc" /Vn:"g_SingleInstanceVertexShader_main"
set DebugSingleInstanceVertexShaderCompilerFlags=/Od /Zi
set ReleaseSingleInstanceVertexShaderCompilerFlags=/O1 /Qstrip_debug

set MultipleInstanceVertexShaderName=MultipleInstanceVertexShader
set MultipleInstanceVertexShaderEntryPoint=%ShadersDir%\%MultipleInstanceVertexShaderName%.%ShaderExt%
set CommonMultipleInstanceVertexShaderCompilerFlags=/T:vs_5_0 /WX /E:main /Fh:"%PrecompiledHeadersDir%\%MultipleInstanceVertexShaderName%.h" /Fo:"%BuildDir%\%MultipleInstanceVertexShaderName%.fxc" /Vn:"g_MultipleInstanceVertexShader_main"
set DebugMultipleInstanceVertexShaderCompilerFlags=/Od /Zi
set ReleaseMultipleInstanceVertexShaderCompilerFlags=/O1 /Qstrip_debug

set SimplePixelShaderName=SimplePixelShader
set SimplePixelShaderEntryPoint=%ShadersDir%\%SimplePixelShaderName%.%ShaderExt%
set CommonSimplePixelShaderCompilerFlags=/T:ps_5_0 /WX /E:main /Fh:"%PrecompiledHeadersDir%\%SimplePixelShaderName%.h" /Fo:"%BuildDir%\%SimplePixelShaderName%.fxc" /Vn:"g_SimplePixelShader_main"
set DebugSimplePixelShaderCompilerFlags=/Od /Zi
set ReleaseSimplePixelShaderCompilerFlags=/O1 /Qstrip_debug

set TexturedLitPixelShaderName=TexturedLitPixelShader
set TexturedLitPixelShaderEntryPoint=%ShadersDir%\%TexturedLitPixelShaderName%.%ShaderExt%
set CommonTexturedLitPixelShaderCompilerFlags=/T:ps_5_0 /WX /E:main /Fh:"%PrecompiledHeadersDir%\%TexturedLitPixelShaderName%.h" /Fo:"%BuildDir%\%TexturedLitPixelShaderName%.fxc" /Vn:"g_TexturedLitPixelShader_main"
set DebugTexturedLitPixelShaderCompilerFlags=/Od /Zi
set ReleaseTexturedLitPixelShaderCompilerFlags=/O1 /Qstrip_debug


REM Setup C/C++ compilation settings
if "%Compiler%"=="msvc" (
    set CommonLinkerFlags=/subsystem:windows /incremental:no /machine:x64 /nologo /defaultlib:Kernel32.lib /defaultlib:User32.lib /defaultlib:Gdi32.lib /defaultlib:d3d11.lib /defaultlib:d3dcompiler.lib /defaultlib:dxgi.lib /defaultlib:winmm.lib /defaultlib:Pathcch.lib /defaultlib:Xaudio2.lib
    set DebugLinkerFlags=%CommonLinkerFlags% /opt:noref /debug /pdb:"%BuildDir%\%ProjectName%.pdb"
    set ReleaseLinkerFlags=%CommonLinkerFlags% /opt:ref

    set CommonCompilerFlags=/nologo /WX /W3 /I "%IncludePaths%" /I "%ImGuiIncludeDir%" /I "%ImGuiExamplesDir%" /D_CRT_SECURE_NO_WARNINGS
    set CompilerFlagsDebug=%CommonCompilerFlags%/Od /Zi /D_DEBUG
    set CompilerFlagsRelease=%CommonCompilerFlags%/Ox /arch:AVX2 /DNDEBUG /Zo /Zi

) else (
    set CommonLinkerFlags=-v -fuse-ld=lld-link -Wl,-machine:x64,-incremental:no,-subsystem:windows,-nologo
    set CommonLinkerFlags=%CommonLinkerFlags%,-defaultlib:Kernel32.lib,-defaultlib:User32.lib,-defaultlib:Gdi32.lib
    set CommonLinkerFags=%CommonLinkerFlags%,-defaultlib:d3d11.lib,-defaultlib:d3dcompiler.lib,-defaultlib:dxgi.lib,-defaultlib:dxguid.lib,-defaultlib:winmm.lib
    set CommonLinkerFags=%CommonLinkerFlags%,-defaultlib:Xaudio2.lib
    set DebugLinkerFlags=%CommonLinkerFlags%,-opt:noref,-debug,-pdb:"%BuildDir%\%ProjectName%.pdb"
    set ReleaseLinkerFlags=%CommonLinkerFlags%,-opt:ref

    set CommonCompilerFlags=-Werror -pedantic-errors -I "%IncludePaths%" -I "%ImGuiIncludeDir%" -I "%ImGuiExamplesDir%"
    set CompilerFlagsDebug=%CommonCompilerFlags% -ggdb -O0
    set CompilerFlagsRelease=%CommonCompilerFlags% -g -O3 -mavx2
)


if "%BuildType%"=="debug" (
    set BuildSimpleVertexShaderCommand=fxc %CommonSimpleVertexShaderCompilerFlags% %DebugSimpleVertexShaderCompilerFlags% %SimpleVertexShaderEntryPoint%
    set BuildSingleInstanceVertexShaderCommand=fxc %CommonSingleInstanceVertexShaderCompilerFlags% %DebugSingleInstanceVertexShaderCompilerFlags% %SingleInstanceVertexShaderEntryPoint%
    set BuildMultipleInstanceVertexShaderCommand=fxc %CommonMultipleInstanceVertexShaderCompilerFlags% %DebugMultipleInstanceVertexShaderCompilerFlags% %MultipleInstanceVertexShaderEntryPoint%

    set BuildSimplePixelShaderCommand=fxc %CommonSimplePixelShaderCompilerFlags% %DebugSimplePixelShaderCompilerFlags% %SimplePixelShaderEntryPoint%
    set BuildTexturedLitPixelShaderCommand=fxc %CommonTexturedLitPixelShaderCompilerFlags% %DebugTexturedLitPixelShaderCompilerFlags% %TexturedLitPixelShaderEntryPoint%

    if "%Compiler%"=="msvc" (
        set BuildCommand=cl %CommonCompilerFlags% %CompilerFlagsDebug% "%EntryPoint%" /Fe:"%OutBin%" /link %CommonLinkerFlags% %DebugLinkerFlags%
    ) else (
        set BuildCommand=clang %CommonCompilerFlags% %CompilerFlagsDebug% %DebugLinkerFlagsClang% %CommonLinkerFlagsClang% %EntryPoint% -o "%OutBin%"
    )
) else (
    set BuildSimpleVertexShaderCommand=fxc %CommonSimpleVertexShaderCompilerFlags% %ReleaseSimpleVertexShaderCompilerFlags% %SimpleVertexShaderEntryPoint%
    set BuildSingleInstanceVertexShaderCommand=fxc %CommonSingleInstanceVertexShaderCompilerFlags% %ReleaseSingleInstanceVertexShaderCompilerFlags% %SingleInstanceVertexShaderEntryPoint%
    set BuildMultipleInstanceVertexShaderCommand=fxc %CommonMultipleInstanceVertexShaderCompilerFlags% %ReleaseMultipleInstanceVertexShaderCompilerFlags% %MultipleInstanceVertexShaderEntryPoint%

    set BuildSimplePixelShaderCommand=fxc %CommonSimplePixelShaderCompilerFlags% %ReleaseSimplePixelShaderCompilerFlags% %SimplePixelShaderEntryPoint%
    set BuildTexturedLitPixelShaderCommand=fxc %CommonTexturedLitPixelShaderCompilerFlags% %ReleaseTexturedLitPixelShaderCompilerFlags% %TexturedLitPixelShaderEntryPoint%

    if "%Compiler%"=="msvc" (
        set BuildCommand=cl %CommonCompilerFlags% %CompilerFlagsRelease% "%EntryPoint%" /Fe:"%OutBin%" /link %CommonLinkerFlags% %ReleaseLinkerFlags%
    ) else (
        set BuildCommand=clang %CommonCompilerFlags% %CompilerFlagsRelease% %ReleaseLinkerFlagsClang% %CommonLinkerFlagsClang% %EntryPoint% -o "%OutBin%"
    )
)

echo.
echo Compiling HLSL shaders (commands follow below)...
echo %BuildSimpleVertexShaderCommand%
echo %BuildSingleInstanceVertexShaderCommand%
echo %BuildMultipleInstanceVertexShaderCommand%

echo %BuildSimplePixelShaderCommand%
echo %BuildTexturedLitPixelShaderCommand%

echo.
echo Output from compilation:
%BuildSimpleVertexShaderCommand%

if %errorlevel% neq 0 goto error

%BuildSingleInstanceVertexShaderCommand%

if %errorlevel% neq 0 goto error

%BuildMultipleInstanceVertexShaderCommand%

if %errorlevel% neq 0 goto error

%BuildSimplePixelShaderCommand%

if %errorlevel% neq 0 goto error

%BuildTexturedLitPixelShaderCommand%

if %errorlevel% neq 0 goto error

echo.
echo Compiling source files (command follows below)...
echo %BuildCommand%

echo.
echo Output from compilation:
%BuildCommand%

if %errorlevel% neq 0 goto error

REM Copy over any other resource files needed.
echo.
echo Copying resources to build directory...
xcopy /E /Y %ResourcesDir% %BuildDir%

if %errorlevel% neq 0 goto error
if %errorlevel% == 0 goto success

:error
echo.
echo ***************************************
echo *      !!! An error occurred!!!       *
echo ***************************************
goto end


:success
echo.
echo ***************************************
echo *    Build completed successfully.    *
echo ***************************************
goto end


:end
echo.
echo Build script finished execution at %time%.
popd

endlocal

exit /b %errorlevel%
