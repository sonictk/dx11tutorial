/**
 * @file   SimpleVertexShader.hlsl
 *
 * @brief  A basic vertex shader.
 */

/// Encapsulates all of the vertex attrs sent from the app.
struct AppData
{
	float3 position : POSITION; // NOTE: (sonictk) ``POSITION`` is a *semantic*.
	float3 color : COLOR;
};


/// Defines the variables that will be output from the vertex shader.
struct SimpleVertexShaderOutput
{
	float4 color : COLOR;

	// NOTE: (sonictk) At minimum, the vtx shader *must* output a float4 bound
	// to the SV_POSITION system-value semantic as this is required by the
	// rasterizer stage.
	float4 position : SV_POSITION;
};


// NOTE: (sonictk) Assign each constant buffer to constant buffer registers by
// using the ``b`` register type. This is being explicit (the shader compiler should
// do this anyway) since it provides more control over the placement of the buffers.
cbuffer PerApplication : register(b0)
{
	matrix projectionMatrix;
}


cbuffer PerFrame : register(b1)
{
	matrix viewMatrix;
}


cbuffer PerObject : register(b2)
{
	matrix worldMatrix;
}


/**
 * The entry point of the vertex shader.
 *
 * @param inData	The data being passed in to the shader from the Input Assembler.
 *
 * @return			The transformed vertex data in clip space.
 */
SimpleVertexShaderOutput main(AppData inData)
{
	SimpleVertexShaderOutput outData;

	// NOTE: (sonictk) Since DX10, the default order for matrices in HLSL is column-major/post-multiplication.
	// However, DirectXMath uses row-major matrices, row vectors and pre-multiplication.
	matrix modelViewProjectionMatrix = mul(projectionMatrix, mul(viewMatrix, worldMatrix));
	outData.position = mul(modelViewProjectionMatrix, float4(inData.position, 1.0f));

	outData.color = float4(inData.color, 1.0f);

	return outData;
}
