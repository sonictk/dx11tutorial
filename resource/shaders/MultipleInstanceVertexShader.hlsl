/**
 * @file   MultipleInstanceVertexShader.hlsl
 * @brief  Used for drawing many instances using hardware instancing, allowing multiple
 *         copies to be drawn in a single draw call.
 */

/// Encapsulates all of the vertex attrs sent from the app.
struct AppData
{
	// Per-vertex data.
	float3 position : POSITION;
	float3 normal : NORMAL;
	float2 texCoord : TEXCOORD;

	// Per-instance data.
	matrix worldMatrix : WORLDMATRIX;
	matrix inverseTransposeMatrix : INVERSETRANSPOSEWORLDMATRIX;
};


/// Defines the variables that will be output from the vertex shader.
struct MultipleInstanceVertexShaderOutput
{
	// NOTE: (sonictk) Doesn't matter which semantics are used here as long as the
	// same semantic for the input variables for the PS are used.
	float4 positionWorldSpace : TEXCOORD1;
	float3 normalWorldSpace : TEXCOORD2;
	float2 texCoord : TEXCOORD0;
	float4 position : SV_POSITION; // NOTE: (sonictk) This is used directly by the rasterizer stage and not the PS since this is a **system-value semantic**.
};


// NOTE: (sonictk) Assign each constant buffer to constant buffer registers by
// using the ``b`` register type. This is optional, but we're being explicit
// (the shader compiler should do this anyway) since it provides more control
// over the placement of the buffers.
cbuffer PerFrame : register(b0)
{
	/// Combination of the view and projection matrices. Since this matrix is
	/// _usually_ constant for all objects rendered this frame, this can be sent
	/// as a "uniform" of sorts.
	matrix viewProjectionMatrix;
}


/**
 * The entry point of the vertex shader.
 *
 * @param inData	The data being passed in to the shader from the Input Assembler.
 *
 * @return			The transformed vertex data in clip space.
 */
MultipleInstanceVertexShaderOutput main(AppData inData)
{
	MultipleInstanceVertexShaderOutput outData;

	// NOTE: (sonictk) MVP matrix is computed per-vertex instead of being passed as
	// as a uniform. Could also have passed the pre-computed MVP matrix as an
	// additional per-instance attribute.
	matrix modelViewProjectionMatrix = mul(viewProjectionMatrix, inData.worldMatrix);

	outData.positionWorldSpace = mul(inData.worldMatrix, float4(inData.position, 1.0f));
	outData.normalWorldSpace = mul((float3x3)inData.inverseTransposeMatrix, inData.normal);
	outData.texCoord = inData.texCoord;
	outData.position = mul(modelViewProjectionMatrix, float4(inData.position, 1.0f));

	return outData;
}
