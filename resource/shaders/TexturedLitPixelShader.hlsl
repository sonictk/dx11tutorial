/**
 * @file   TexturedLitPixelShader.hlsl
 * @brief  Computes the lighting information for the objects in the scene.
 */

#define MAX_LIGHTS 4

// NOTE: (sonictk) Neither HLSL nor GLSL have enum types, even though the ``enum``
// keyword is reserved. So we resort to preprocessor defines instead.
#define DIRECTIONAL_LIGHT 0
#define POINT_LIGHT 1
#define SPOT_LIGHT 2

/// Defines a texture object that returns a ``float4`` vector when sampled.
/// Each component will be normalized.
Texture2D globalTexture2D : register(t0); // NOTE: (sonictk) We will bind a texture to the first texture slot as well in the app.

sampler globalSampler : register(s0); // NOTE: (sonictk) We will assign a sampler obj. to the first sampler slot as well in the app.


/// Defines the input variables that should be passed in from the vertex shader (through the rasterizer).
/// The layout and semantics here must match that of the vertex shader's output.
/// Since in the vertex shader we had ``position`` binded to SV_POSITION semnantic,
/// it's consumed by the rasterizer and isn't needed by the pixel shader anymore.
struct PixelShaderInput
{
	float4 positionWorldSpace : TEXCOORD1;
	float3 normalWorldSpace : TEXCOORD2;
	float2 texCoord : TEXCOORD0;
};


/// Defines material properties.
struct PhongMaterial
{
	float4 emissive;
	// 16 byte boundary here
	float4 ambient;
	// 16 byte boundary here
	float4 diffuse;
	// 16 byte boundary here
	float4 specular;
	// 16 byte boundary here
	float specularExponent; // 4 bytes
	bool useTexture;  // 4 bytes in HLSL unlike C
	// int2 padding; The 8 bytes of padding will be added automatically.
	// 16 byte boundary here
};


/// Uniform access to the material properties.
cbuffer PhongMaterialProperties : register(b0)
{
	/// This will be used to access the material properties in the shader.
	PhongMaterial globalMaterial;
}


/// Define light properties.
struct Light
{
	float4 position;
	float4 direction;
	float4 color;

	float spotAngle;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;

	int lightType;

	bool isEnabled;
	// int2 padding; 8 bytes, this padding will also be implicitly added to the constant buffer as well.
};


/// Variables in this constant buffer are kind of like uniforms.
cbuffer LightProperties : register(b1)
{
	float4 eyePosition; // NOTE: (sonictk) Pos. of camera in world space.
	float4 globalAmbient;

	Light sceneLights[MAX_LIGHTS];
}


struct LightingResult
{
	float4 diffuse;
	float4 specular;
};


/**
 * Calculates the diffuse contribution of a given ``light``.
 *
 * @param light 	The light to find the diffuse contribution for.
 * @param L 		The vector that points from the pt. being shaded to the ``light`` source.
 * @param N		The surface normal at the pt. being shaded.
 *
 * @return			The diffuse contribution of the ``light`` specified.
 */
float4 findDiffuse(Light light, float3 L, float3 N)
{
	float NDotL = max(0, dot(N, L));

	// NOTE: (sonictk) Kd (material diffuse component) is not taken into account yet;
	// we will do that after the diffuse contributions of all the light sources
	// have been computed using this function first.
	float4 result = light.color * NDotL;

	return result;
}


/**
 * Calculates the specular contribution for a given ``light``.
 *
 * @param light 	The light to find the specular contribution for.
 * @param V 		The view vector. (i.e. eye pos. - pos of pt. to shade)
 * @param L 		The vector that points from the pt. being shaded to the ``light`` source.
 * @param N		The surface normal of the pt. being shaded.
 *
 * @return
 */
float4 findSpecular(Light light, float3 V, float3 L, float3 N)
{
	// NOTE: (sonictk) Phong lighting model
	float3 R = normalize(reflect(-L, N));
	float RDotV = max(0, dot(R, V));

	// NOTE: (sonictk) Blinn-Phong lighting model
	/* float3 H = normalize(L + V); */
	/* float NDotH = max(0, dot(N, H)); */

	// NOTE: (sonictk) Again, Ks (material spec. component) will be taken into
	// consideration later after all the spec. contribs. of all the light sources
	// have been computed first.
	float4 result = light.color * pow(RDotV, globalMaterial.specularExponent);

	return result;
}


/**
 * Calculates the attenuation for a given ``light``.
 *
 * @param light	The light to calculate the attenuation for.
 * @param dist		The distance from the pt. being shaded to the ``light`` source.
 *
 * @return			The attenuation value.
 */
float findAttenuation(Light light, float dist)
{
	float result = 1.0f / (light.constantAttenuation +
						   (light.linearAttenuation * dist) +
						   (light.quadraticAttenuation * dist * dist));
	return result;
}


/**
 * Computes the diffuse and specular contributions for a given point ``light``.
 *
 * @param light 	The light to find the diffuse and spec. contrib.s for .
 * @param V 		The view vector.
 * @param P 		The pt. being shaded in world space.
 * @param N 		The surface normal of the pt. being shaded.
 *
 * @return 		The diffuse and specular contributions for the ``light`` specified.
 */
LightingResult findPointLightResult(Light light, float3 V, float4 P, float3 N)
{
	LightingResult result;

	float3 L = (light.position - P).xyz;
	float dist = length(L);
	L /= dist;

	float attenuation = findAttenuation(light, dist);

	result.diffuse = findDiffuse(light, L, N) * attenuation;
	result.specular = findSpecular(light, V, L, N) * attenuation;

	return result;
}


/**
 * Computes the diffuse and specular contributions for a given directional ``light``.
 *
 * @param light 	The light to find the diffuse and spec. contrib.s for .
 * @param V 		The view vector.
 * @param P 		The pt. being shaded in world space.
 * @param N 		The surface normal of the pt. being shaded.
 *
 * @return 		The diffuse and specular contributions for the ``light`` specified.
 */
LightingResult findDirectionalLightResult(Light light, float3 V, float4 P, float3 N)
{
	LightingResult result;

	// NOTE: (sonictk) Since dlights don't have a position (they're infinite),
	// the light vector is the direction negated (L is the normalized direction
	// vector from the pt. being shaded to the light source.)
	float3 L = -light.direction.xyz;

	result.diffuse = findDiffuse(light, L, N);
	result.specular = findSpecular(light, V, L, N);

	return result;
}


/**
 * Computes the intensity of the light for a given spot ``light``.
 *
 * @param light	The light to find the intensity for.
 * @param L		The light vector (from pt. to light)
 *
 * @return			The intensity of the light based on the angle in the spotlight cone.
 */
float findSpotConeIntensity(Light light, float3 L)
{
	// NOTE: (sonictk) The spot angle is the half-angle of the spotlight cone.
	float minCos = cos(light.spotAngle); // NOTE: (sonictk) The largest angle of the spotlight cone in radians is the smallest cosine value.
	float maxCos = (minCos + 1.0f) / 2.0f;
	float cosAngle = dot(light.direction.xyz, -L);

	float result = smoothstep(minCos, maxCos, cosAngle); // NOTE: (sonictk) Hermite interp.

	return result;
}


/**
 * Calculates the diffuse and specular contributions of the spot ``light`` provided.
 *
 * @param light	The light.
 * @param V		The view vector.
 * @param P		The pos. of the pt. being shaded.
 * @param N		The surface normal of the pt. being shaded.
 *
 * @return			The diffuse and specular contributions of the spot ``light``.
 */
LightingResult findSpotLightResult(Light light, float3 V, float4 P, float3 N)
{
	LightingResult result;

	float3 L = (light.position - P).xyz;
	float dist = length(L);
	L /= dist;

	float attenuation = findAttenuation(light, dist);
	float intensity = findSpotConeIntensity(light, L);

	result.diffuse = findDiffuse(light, L, N) * attenuation * intensity;
	result.specular = findSpecular(light, V, L, N) * attenuation * intensity;

	return result;
}


/**
 * Computes the total diffuse and specular lighting contributions for all lights
 * in the scene.
 *
 * @param P 	The pt. to shade.
 * @param N 	The surface normal of the pt. being shaded.
 *
 * @return 	The diffuse and specular lighting contributions from all lights.
 */
LightingResult computeLighting(float4 P, float3 N)
{
	LightingResult result = {{0, 0, 0, 0}, {0, 0, 0, 0}};

	// NOTE: (sonictk) Normalized view vector computed from eye pos. and pt. pos. (world space)
	float3 V = normalize(eyePosition - P).xyz;

	[unroll] // NOTE: (sonictk) HLSL-specific directive, tells fxc.exe to auto-unroll this loop.
	for (int i=0; i < MAX_LIGHTS; ++i) {
		LightingResult curResult = {{0, 0, 0, 0}, {0, 0, 0, 0}};

		if (!sceneLights[i].isEnabled) continue;

		switch (sceneLights[i].lightType)
		{
		case DIRECTIONAL_LIGHT:
			 curResult = findDirectionalLightResult(sceneLights[i], V, P, N);

			break;
		case SPOT_LIGHT:
			curResult = findSpotLightResult(sceneLights[i], V, P, N);

			break;
		case POINT_LIGHT:
			curResult = findPointLightResult(sceneLights[i], V, P, N);

			break;
		default:

			break;
		}

		result.diffuse += curResult.diffuse;
		result.specular += curResult.specular;
	}

	// NOTE: (sonictk) ``saturate()`` is the equivalent of ``clamp()`` from 0 - 1 range.
	result.diffuse = saturate(result.diffuse);
	result.specular = saturate(result.specular);

	return result;
}


/**
 * The entry point for this pixel shader.
 *
 * @param inData 	The data that should have been passed in from the vertex shader
 * 				(via the rasterizer).
 *
 * @return			The color to shade the pixel.
 */
float4 main(PixelShaderInput inData) : SV_TARGET
{
	// NOTE: (sonictk) The rasterizer is responsible for interpolating the vtx. attrs.
	// before they're passed to this pixel shader, but the interpolation process
	// can cause normalized vectors to become unnormalized. We therefore re-normalize
	// it before using it for lighting calculations.
	LightingResult lightingResult = computeLighting(inData.positionWorldSpace,
													normalize(inData.normalWorldSpace));

	float4 emissive = globalMaterial.emissive;
	float4 ambient = globalMaterial.ambient * globalAmbient;
	float4 diffuse = globalMaterial.diffuse * lightingResult.diffuse;
	float4 specular = globalMaterial.specular * lightingResult.specular;

	float4 textureColor = {1, 1, 1, 1};
	if (globalMaterial.useTexture) {
		textureColor = globalTexture2D.Sample(globalSampler, inData.texCoord);
	}

	float4 result = (emissive + ambient + diffuse + specular) * textureColor;

	return result;
}
