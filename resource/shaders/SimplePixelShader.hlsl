/**
 * @file   SimplePixelShader.hlsl
 *
 * @brief  A basic pixel shader.
 */

/// Defines the input variables that should be passed in from the vertex shader.
struct SimplePixelShaderInput
{
	float4 color : COLOR;
};


/**
 * The entry point for the pixel shader.
 *
 * @param inData 	The data that should have been passed from the vertex shader.
 *
 * @return 		The color to shade the pixel bound to the ``SV_TARGET`` system semantic.
 */
float4 main(SimplePixelShaderInput inData) : SV_TARGET
{
	return inData.color;
}
