/**
 * @file   SingleInstanceVertexShader.hlsl
 * @brief  Used to render a single instance of a scene object.
 */

/// Encapsulates all of the vertex attrs sent from the app.
struct AppData
{
	float3 position : POSITION;
	float3 normal : NORMAL;
	float2 texCoord : TEXCOORD;
};


/// Defines the variables that will be output from the vertex shader.
struct SingleInstanceVertexShaderOutput
{
	// NOTE: (sonictk) Doesn't matter which semantics are used here as long as the
	// same semantic for the input variables for the PS are used.
	float4 positionWorldSpace : TEXCOORD1;
	float3 normalWorldSpace : TEXCOORD2;
	float2 texCoord : TEXCOORD0;
	float4 position : SV_POSITION; // NOTE: (sonictk) This is used directly by the rasterizer stage and not the PS since this is a **system-value semantic**.
};


// NOTE: (sonictk) ``register`` keyword is optional, allows for manually
// assigning/packing constant data. ``b`` register type refers to assigning to
// constant buffer registers.
cbuffer PerObject : register(b0)
{
	matrix worldMatrix; // NOTE: (sonictk) Transform vtx pos from obj space to world space. Required by PS
	// NOTE: (sonictk) Used to normalize the scale in a non-uniform scale transformation. See http://www.lighthouse3d.com/tutorials/glsl-tutorial/the-normal-matrix/
	matrix inverseTransposeWorldMatrix; // NOTE: (sonictk) transform vtx normal from obj space to world space Required by PS
	matrix worldViewProjectionMatrix; // NOTE: (sonictk) Transform vtx pos. from obj space to projected clip space. Required by rasterizer.
}


cbuffer PerFrame : register(b1)
{
	matrix viewProjectionMatrix;
}

/**
 * The entry point of the vertex shader.
 *
 * @param inData	The data being passed in to the shader from the Input Assembler.
 *
 * @return			The transformed vertex data in clip space.
 */
SingleInstanceVertexShaderOutput main(AppData inData)
{
	SingleInstanceVertexShaderOutput outData;

	// NOTE: (sonictk) Since DX10, the default order for matrices in HLSL is column-major/post-multiplication.
	// However, DirectXMath uses row-major matrices, row vectors and pre-multiplication.
	outData.position = mul(worldViewProjectionMatrix, float4(inData.position, 1.0f));

	outData.positionWorldSpace = mul(worldMatrix, float4(inData.position, 1.0f));

	outData.normalWorldSpace = mul((float3x3)inverseTransposeWorldMatrix, inData.normal);

	outData.texCoord = inData.texCoord;

	return outData;
}
